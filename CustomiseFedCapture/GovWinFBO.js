﻿//GOVWIN FBO Methods
function CallGovWin() {
    
    try {
        var oppguid = Xrm.Page.data.entity.getId();
        var OID = oppguid.substring(1, 37);
        Xrm.Page.ui.setFormNotification("Please wait, System is retrieving GovWin data....", "WARNING", "1");
        var currentDateTime = new Date();
        //Xrm.Page.getAttribute("govwin_getgovwindate").setValue(currentDateTime);
        //Xrm.Page.getAttribute("govwin_getgovwindate").setSubmitMode("always");
        //var save = true;
        //Xrm.Page.data.refresh(save).then(successCallbackGovWin, errorCallbackGovWin);
        var objArry = {};
        objArry.govwin_getgovwindate = currentDateTime;
        Xrm.WebApi.updateRecord("opportunity", OID, objArry).then(
                         function success(result) {
                             Xrm.WebApi.retrieveRecord("opportunity", OID, "?$select=fedcap_govwinstatus").then(
                                    function success(result) {
                                        if (result.fedcap_govwinstatus === 'Successfull') {
                                            Xrm.Utility.alertDialog("GovWin data retrieved successfully.!!");
                                            //getdetails();
                                            //getopportunity();

                                            Xrm.Page.ui.clearFormNotification("2");
                                            Xrm.Page.ui.clearFormNotification("1");
                                            var iFrameObj = $('#GovWinFBO').attr('id');
                                            var id = $('#GovWinFBO').attr('src');
                                            $('#GovWinFBO').attr('src', $('#GovWinFBO').attr('src'));

                                            $('#idGovWin').attr('src', $('#idGovWin').attr('src'));
                                        }
                                        else {
                                            Xrm.Utility.alertDialog("Data Retrive Failed, may be ID doesn't exist or number of request exceeded for given time.");
                                            Xrm.Page.ui.clearFormNotification("1");
                                            Xrm.Page.ui.clearFormNotification("2");
                                            var iFrameObj = $('#GovWinFBO').attr('id');
                                            var id = $('#GovWinFBO').attr('src');
                                            $('#GovWinFBO').attr('src', $('#GovWinFBO').attr('src'));

                                            $('#idGovWin').attr('src', $('#idGovWin').attr('src'));
                                        }
                                    },
                                    function (error) {
                                        console.log(error.message);
                                    }
                                );
                         },
                         function (error) {
                             Xrm.Utility.alertDialog("Error while retriving GovWin records.");
                             Xrm.Page.ui.clearFormNotification("1");
                             Xrm.Page.ui.clearFormNotification("2");
                             var iFrameObj = $('#GovWinFBO').attr('id');
                             var id = $('#GovWinFBO').attr('src');
                             $('#GovWinFBO').attr('src', $('#GovWinFBO').attr('src'));
                         }
                     );
    } catch (e) {
        Xrm.Utility.alertDialog("Error while retriving GovWin records.");
        Xrm.Page.ui.clearFormNotification("1");
        var iFrameObj = $('#GovWinFBO').attr('id');
        var id = $('#GovWinFBO').attr('src');
        $('#GovWinFBO').attr('src', $('#GovWinFBO').attr('src'));
    }
}
function CallGovWin1() {
    function e() {
        var e = Xrm.Page.data.entity.getId(),
         t = new XMLHttpRequest;
        t.open("GET", Xrm.Page.context.getClientUrl() + "/api/data/v8.0/incidents?$filter=description eq '" + e + "'and  title eq 'ImtiyazPlugin'", !0), t.setRequestHeader("OData-MaxVersion", "4.0"), t.setRequestHeader("OData-Version", "4.0"), t.setRequestHeader("Accept", "application/json"), t.setRequestHeader("Content-Type", "application/json; charset=utf-8"), t.setRequestHeader("Prefer", 'odata.include-annotations="OData.Community.Display.V1.FormattedValue"'), t.onreadystatechange = function () {
            if (4 === this.readyState)
                if (t.onreadystatechange === null, 200 === this.status) {
                    for (var e = JSON.parse(this.response), a = 0; a < e.value.length; a++)
                        setTimeout(function () {
                            getdetails();
                            getopportunity();
                            "fail" === e.value[0].description ? (
                            Xrm.Utility.alertDialog("Data Retrive Failed, may be ID doesn't exist or number of request exceeded for given time"),
                            Xrm.Page.ui.clearFormNotification("1")) : (Xrm.Utility.alertDialog("Data Retrieved Successfully"), Xrm.Page.ui.clearFormNotification("1"))
                        }, 4e3);
                }
                else {
                    getdetails();
                    getopportunity();
                    var fedcap_govwinstatuscheck = "";
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities(" + OID + ")",
                        beforeSend: function (XMLHttpRequest) {
                            XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                            XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                            XMLHttpRequest.setRequestHeader("Accept", "application/json");
                            XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
                        },
                        async: false,
                        success: function (data, textStatus, xhr) {
                            var result = data;
                            fedcap_govwinstatuscheck = result["fedcap_govwinstatus"];
                        },
                        error: function (data, textStatus, xhr) { }
                    });

                    if (fedcap_govwinstatuscheck === "fail") {
                        Xrm.Utility.alertDialog("Data Retrive Failed, may be ID doesn't exist or number of request exceeded for given time");
                        Xrm.Page.ui.clearFormNotification("1");
                    }
                    else if (fedcap_govwinstatuscheck === "Successfull") {
                        Xrm.Utility.alertDialog("Data Retrieved Successfully");
                        Xrm.Page.ui.clearFormNotification("1");

                    }
                    else {
                        Xrm.Utility.alertDialog("Something went wrong while data retriving.");
                        Xrm.Page.ui.clearFormNotification("1");
                    }
                }
        }, t.send()
    }
    Xrm.Page.ui.setFormNotification("Please wait, System is retrieving data...", "WARNING", "1"), {
        prefix: "billto_",
        guid: Xrm.Page.data.entity.getId(),
        entityName: Xrm.Page.data.entity.getEntityName()
    }, e()
};
//var str2DOMElement = function (html) {
//    var frame = document.createElement('iframe');
//    frame.style.display = 'none';
//    document.body.appendChild(frame);
//    frame.contentDocument.open();
//    frame.contentDocument.write(html);
//    frame.contentDocument.close();
//    var el = frame.contentDocument.body;
//    document.body.removeChild(frame);
//    return el;
//}
function getFBO() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities(" + OID + ")",
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
            XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
        },
        async: true,
        success: function (data, textStatus, xhr) {
            var result = data;
            var  fbo_fbo_solicitation_number = result["fbo_fbo_solicitation_number"];
            $("#fbo_fbo_solicitation_number").html(result["fbo_fbo_solicitation_number"]);
            $("#fbo_fbo_title").html(result["fbo_fbo_title"]);
            $("#fbo_fbo_agency").html(result["fbo_fbo_agency"]);
            $("#fbo_fbo_contact").html(result["fbo_fbo_contact"]);
            $("#fbo_fbo_class_code").html(result["fbo_fbo_class_code"]);
            $("#fbo_fbo_archive_date").html(result["fbo_fbo_archive_date"]);
            $("#fbo_fbo_id").html(result["fbo_fbo_id"]);
            $("#fbo_fbo_office").html(result["fbo_fbo_office"]);
            $("#fbo_fbo_naics_text").html(result["fbo_fbo_naics_text"]);
            $("#fbo_fbo_office_address").html(result["fbo_fbo_office_address"]);
            $("#fbo_fbo_setaside").html(result["fbo_fbo_setaside"]);
            $("#fbo_fbo_location").html(result["fbo_fbo_location"]);
            $("#fbo_fbo_listing_url").html(result["fbo_fbo_listing_url"]);
            $("#fbo_fbo_zip_code").html(result["fbo_fbo_zip_code"]);
            $("#fbo_fbo_notice_type").html(result["fbo_fbo_notice_type"]);
            $("#fbo_fbo_pop_address").html(result["fbo_fbo_pop_address"]);
            $("#fbo_fbo_pop_zip_code").html(result["fbo_fbo_pop_zip_code"]);
            $("#fbo_fbo_pop_country").html(result["fbo_fbo_pop_country"]);
            $("#fbo_fbo_description").html(result["fbo_fbo_description"]);

            //$("#fbo_fbo_posted_date").text(result["fbo_fbo_posted_date"].toString().substring(0, 10));
            //$("#fbo_get_fbo_date").text(result["fbo_get_fbo_date"].toString().substring(0, 10));
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}
function getdetails() {
    var CVGuid = null;
    var ContactGuid = null;
    var competGuid = null;
    var contractguid = null;
    var milestoneguid = null;
    var entityguid = null;
    var FBONoticeGuid = null;
    var relatedArticleGuid = null;
    var popDataGuid = null;
    if (OID !== null && OID !== "") {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities(" + OID + ")",
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: false,
            success: function (data, textStatus, xhr) {
                var result = data;
                CVGuid = result["_govwin_govwincontractvehicles_value"];
                ContactGuid = result["_govwin_govwincontacts_value"];
                competGuid = result["_govwin_govwincompanies_value"];
                contractguid = result["_govwin_govwincontracts_value"];
                milestoneguid = result["_govwin_govwinmilestones_value"];
                entityguid = result["_govwin_govwinentities_value"];
                FBONoticeGuid = result["_govwin_govwinfbonotices_value"];
                relatedArticleGuid = result["_govwin_govwinrelatedarticles_value"];
                popDataGuid = result["_govwin_govwinplaceofperformances_value"];
            },
            error: function (xhr, textStatus, errorThrown) {


            }
        });
    }
    if (OID !== null && OID !== "") {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_govwincompetitiontypes?$filter=_govwin_opportunity_govwin_govwincompetitiid_value eq " + OID,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;
                for (var i = 0; i < results.value.length; i++) {
                    var govwin_govwincompetitiontypeid = results.value[i]["govwin_govwincompetitiontypeid"];
                    var newRowContentC = '<tr><td>' + results.value[i]["govwin_id"] + '</td><td>' + results.value[i]["govwin_title"] + '</td></tr>';
                    $("#CompType tbody").append(newRowContentC);
                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    if (OID !== null && OID !== "") {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_govcontracttypeses?$filter=_govwin_opportunitycontracttypes_value eq " + OID,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;

                for (var i = 0; i < results.value.length; i++) {
                    var govwin_govcontracttypesid = results.value[i]["govwin_govcontracttypesid"];
                    var newRowContentC = '<tr><td>' + results.value[i]["govwin_govwincontractid"] + '</td><td>' + results.value[i]["govwin_govwincontracttitle"] + '</td></tr>';
                    $("#ContractType tbody").append(newRowContentC);

                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    if (popDataGuid != null) {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_subgovplaceofperformances?$filter=_govwin_govwinplaceofperformance_value eq " + popDataGuid,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;
                for (var i = 0; i < results.value.length; i++) {
                    var govwin_subgovwinplaceofperformancesid = results.value[i]["govwin_subgovwinplaceofperformancesid"];
                    var prim = "";
                    if (results.value[i]["govwin_isprimary"] != null) {
                        if (results.value[i]["govwin_isprimary"] == "true") {
                            prim = "Yes"
                        } else {
                            prim = "No";
                        }
                    }
                    var newRowContentC = '<tr><td>' + results.value[i]["govwin_country"] + '</td><td>' + results.value[i]["govwin_location"] + '</td><td>' + results.value[i]["govwin_state"] + '</td><td>' + prim + '</td></tr>';
                    //var datacomp = $("#WebResource_html").contents().find("#thirteen table");
                    $("#POP tbody").append(newRowContentC);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                //alert(textStatus + " govwin_subgovplaceofperformances " + errorThrown);
            }
        });

    }
    if (CVGuid !== null) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_subgovcontractvehicleses?$filter=_govwin_contractvehiclelookup_value eq " + CVGuid,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;
                for (var i = 0; i < results.value.length; i++) {
                    var govwin_subgovcontractvehiclesid = results.value[i]["govwin_subgovcontractvehiclesid"];
                    var ID = "";
                    if (results.value[i]["govwin_govid2"] !== 0) {
                        ID = results.value[i]["govwin_govid2"];
                    }
                    var type = "";
                    if (results.value[i]["govwin_type"] !== null) {
                        type = results.value[i]["govwin_type"];
                    }
                    var SCID = "";
                    if (results.value[i]["govwin_subclassid2"] !== null) {
                        SCID = results.value[i]["govwin_subclassid2"];
                    }
                    var SCTitle = "";
                    if (results.value[i]["govwin_subclassestitle"] !== null) {
                        SCTitle = results.value[i]["govwin_subclassestitle"];
                    }
                    var SCNUM = "";
                    if (results.value[i]["govwin_subclassesnumber"] !== null) {
                        SCNUM = results.value[i]["govwin_subclassesnumber"];
                    }
                    var newRowContentC = '<tr><td>' + ID + '</td><td>' + results.value[i]["govwin_title"] + '</td><td>' + type + '</td><td>' + SCID + '</td><td>' + SCTitle + '</td><td>' + SCNUM + '</td></tr>';
                    $("#ContractVahicles tbody").append(newRowContentC);
                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });

    }
    if (ContactGuid !== null) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_subgovcontactses?$filter=_govwin_contactlookup_value eq  " + ContactGuid,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data.value;
                for (var i = 0; i < results.length; i++) {
                    var mfdt = "";
                    if (results[i]["govwin_modifieddate"] !== null) {
                        mfdt = results[i]["govwin_modifieddate@OData.Community.Display.V1.FormattedValue"];
                    }
                    var newRowContent = '<tr><td>' + results[i].govwin_contactname + '</td><td>' + results[i].govwin_firstname + '</td><td>' + results[i].govwin_lastname + '</td><td>' + results[i].govwin_title + '</td><td>' + results[i].govwin_email + '</td><td>' + results[i].govwin_phone + '</td><td>' + results[i].govwin_contactid + '</td><td>' + mfdt + '</td></tr>';
                    $("#contTable tbody").append(newRowContent);
                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    if (competGuid !== null) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_subgovcompanieses?$filter=_govwin_companieslookup_value eq " + competGuid,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;
                for (var i = 0; i < results.value.length; i++) {
                    var govwin_subgovcompaniesid = results.value[i]["govwin_subgovcompaniesid"];

                    var newRowContentC = '<tr><td>' + results.value[i]["govwin_name"] + '</td><td>' + results.value[i]["govwin_govid"] + '</td></tr>';
                    $("#Competitor tbody").append(newRowContentC);

                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    if (contractguid !== null) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_subgovcontractses?$filter=_govwin_contractslist_value eq " + contractguid,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;
                for (var i = 0; i < results.value.length; i++) {
                    var incumbentbox = '';
                    var govwin_incumbent = results.value[i]["govwin_incumbent"];
                    if (govwin_incumbent === "true") {
                        incumbentbox = '<input name="" type="checkbox" value="" checked disabled>';
                    } else { incumbentbox = '<input name="" type="checkbox" value="" disabled>'; }

                    var awrdt = "";
                    if (results.value[i]["govwin_awarddate"] !== null) {
                        awrdt = results.value[i]["govwin_awarddate@OData.Community.Display.V1.FormattedValue"];
                    }
                    var exrdt = "";
                    if (results.value[i]["govwin_expirationdate"] !== null) {
                        exrdt = results.value[i]["govwin_expirationdate@OData.Community.Display.V1.FormattedValue"];
                    }
                    var crname = "";
                    if (results.value[i]["govwin_contactname"] !== null) {
                        crname = results.value[i]["govwin_contactname"];
                    }
                    var cenum = "";
                    if (results.value[i]["govwin_contractnumber"] !== null) {
                        cenum = results.value[i]["govwin_contractnumber"];
                    }
                    var tonum = "";
                    if (results.value[i]["govwin_taskordernumber"] !== null) {
                        tonum = results.value[i]["govwin_taskordernumber"];
                    }
                    var newRowContent = '<tr><td>' + crname + '</td><td>' + results.value[i]["govwin_id"] + '</td><td>' + cenum + '</td><td>' + results.value[i]["govwin_company"] + '</td><td>' + awrdt + '</td><td>' + exrdt + '</td><td>' + results.value[i]["govwin_estimatedvalue@OData.Community.Display.V1.FormattedValue"] + '</td><td>' + incumbentbox + '</td><td>' + tonum + '</td><td>' + results.value[i]["govwin_title"] + '</td></tr>';
                    $("#contracttable tbody").append(newRowContent);


                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    if (milestoneguid !== null) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_subgovmilestoneses?$filter=_govwin_milestonelistid_value eq " + milestoneguid,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;
                for (var i = 0; i < results.value.length; i++) {
                    var govwin_subgovmilestonesid = results.value[i]["govwin_subgovmilestonesid"];

                    var deltekestimate = '';
                    var govwin_deltekestimate = results.value[i]["govwin_deltekestimatebool"];
                    if (govwin_deltekestimate === "true") {
                        deltekestimate = '<input name="" type="checkbox" value="" checked disabled>';
                    } else { deltekestimate = '<input name="" type="checkbox" value="" disabled>'; }

                    var govestimate = '';
                    var govwin_govtestimatebool = results.value[i]["govwin_govtestimatebool"];
                    if (govwin_govtestimatebool === "true") {
                        govestimate = '<input name="" type="checkbox" value="" checked disabled>';
                    } else { govestimate = '<input name="" type="checkbox" value="" disabled>'; }

                    var msdt = "";
                    if (results.value[i]["govwin_milestonedate"] !== null) {
                        msdt = results.value[i]["govwin_milestonedate@OData.Community.Display.V1.FormattedValue"];
                    }
                    var newRowContent = '<tr><td>' + results.value[i]["govwin_milestones"] + '</td><td>' + deltekestimate + '</td><td>' + govestimate + '</td><td>' + msdt + '</td></tr>';
                    $("#milestonetab tbody").append(newRowContent);
                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    if (entityguid !== null) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_subgoventiteses?$filter=_govwin_goventitylistid_value eq " + entityguid,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;
                for (var i = 0; i < results.value.length; i++) {
                    var newRowContent = '<tr><td>' + results.value[i]["govwin_name"] + '</td><td>' + results.value[i]["govwin_id"] + '</td><td>' + results.value[i]["govwin_market"] + '</td><td><a href="#">' + results.value[i]["govwin_spendingprofile"] + '</a></td><td>' + results.value[i]["govwin_title"] + '</td><td><a href="#">' + results.value[i]["govwin_children"] + '<a></td></tr>';
                    $("#entitytab tbody").append(newRowContent);

                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    if (FBONoticeGuid !== null) {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_subgovfbonoticeses?$filter=_govwin_fbonoticelistid_value eq " + FBONoticeGuid,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;
                for (var i = 0; i < results.value.length; i++) {
                    var govwin_subgovfbonoticesid = results.value[i]["govwin_subgovfbonoticesid"];
                    var pbdt = "";
                    if (results.value[i]["govwin_publicationdate"] !== null) {
                        pbdt = results.value[i]["govwin_publicationdate@OData.Community.Display.V1.FormattedValue"];
                    }
                    var newRowContent = '<tr><td>' + results.value[i]["govwin_title"] + '</td><td><a href="#">' + results.value[i]["govwin_documentlink"] + '</a></td><td>' + results.value[i]["govwin_id"] + '</td><td>' + pbdt + '</td></tr>';
                    $("#FBOtab tbody").append(newRowContent);

                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    //var RAData = Xrm.Page.getAttribute("govwin_govwinrelatedarticles").getValue();
    if (relatedArticleGuid !== null) {
        //var relatedArticleGuid = RAData[0].id.substring(1, 37);
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/govwin_subgovrelatedarticleses?$filter=_govwin_relatedarticallistid_value eq " + relatedArticleGuid,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var results = data;
                for (var i = 0; i < results.value.length; i++) {
                    var pbdate = "";
                    if (results.value[i]["govwin_publicationdate"] !== null) {
                        var pd = (results.value[i]["govwin_publicationdate"]).split("T");
                        pbdate = pd[0];
                    }
                    var govwin_subgovrelatedarticlesid = results.value[i]["govwin_subgovrelatedarticlesid"];
                    var newRowContent = '<tr><td>' + results.value[i]["govwin_title"] + '</td><td><a href="#">' + results.value[i]["govwin_documentlink"] + '</a></td><td>' + results.value[i]["govwin_id"] + '</td><td>' + pbdate + '</td></tr>';
                    //var datafbn = $("#WebResource_html").contents().find("#five table");
                    $("#RAtab tbody").append(newRowContent);
                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
}
function getopportunity() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities(" + OID + ")",
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
            XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
        },
        async: true,
        success: function (data, textStatus, xhr) {
            var result = data;
            var opportunityid = result["opportunityid"];
            var govwinOid = result["govwin_govwin_opportunity_id"];
            $("#oppid").text(govwinOid);
            $("#IQid").text(result["govwin_iqopportunity_id"]);
            $("#oppvalue").text(result["govwin_opportunity_value"]);
            $("#PrimReq").text(result["govwin_primary_requirement"]);
            $("#duration").text(result["govwin_duration"]);
            $("#sourceurl").text(result["govwin_source_url"]);
            $("#status").text(result["govwin_status"]);
            $("#opptitle").text(result["govwin_title"]);
            $("#optype").text(result["govwin_type"]);
            $("#typeofAward").text(result["govwin_type_of_award"]);
            var ud = '';
            if (result["govwin_update_date"] !== null) {
                var str = result["govwin_update_date"];
                ud = str.replace("T", " ");
            }
            $("#updatedate").text(ud);
            var SD = '';
            if (result["govwin_solicitation_date_value"] !== null) {
                var str = result["govwin_solicitation_date_value"];
                SD = str.replace("T", " ");
            }
            $("#solicitationdate").text(SD);
            var AD = '';
            if (result["govwin_award_date_value"] !== null) {
                var str = result["govwin_award_date_value"];
                AD = str.replace("T", " ");
            }
            $("#awarddate").text(AD);
            $("#solicitationnumber").text(result["govwin_solicitation_number"]);
            $("#PriNaicsId").text(result["govwin_primary_naics_id"]);
            $("#PriNaicsTitle").text(result["govwin_primary_naics_title"]);
            $("#PriNaicsSize").text(result["govwin_primary_naics_size_standard"]);
            var intstat = "";
            if (result["govwin_internalstatus"] !== null) {
                intstat = result["govwin_internalstatus"];
            }
            $("#internalstatus").text(intstat);
            var prior = "";
            if (result["govwin_priority"] !== null) {
                prior = result["govwin_priority"];
            }
            $("#priority").text(prior);
            if (result["govwin_description"] !== null) {
                //var descrp = str2DOMElement(result["govwin_description"])
                //$("#description").append(descrp.innerHTML);
                $("#description").append(result["govwin_description"]);
            }
            if (result["govwin_procurement"] !== null) {
                //var procu = str2DOMElement(result["govwin_procurement"]);
                //$("#procurement").append(procu.innerHTML);
                $("#procurement").append(result["govwin_procurement"]);
            }
            //var descrp = str2DOMElement(result["govwin_description"])
            //$("#description").append(descrp.innerHTML);
            //var procu = str2DOMElement(result["govwin_procurement"]);
            //$("#procurement").append(procu.innerHTML);

        },
        error: function (xhr, textStatus, errorThrown) {


        }
    });
}
function setFBODetailsDate() {
    //var currentDateTime = new Date();
    //Xrm.Page.getAttribute("fbo_get_fbo_date").setValue(currentDateTime);
    //Xrm.Page.getAttribute("fbo_get_fbo_date").setSubmitMode("always");
    //Xrm.Page.data.entity.save();

    try {
        var oppguid = Xrm.Page.data.entity.getId();
        var OID = oppguid.substring(1, 37);
        Xrm.Page.ui.setFormNotification("Please wait, System is retrieving GovWin data....", "WARNING", "1");
        var currentDateTime = new Date();
        var objArry = {};
        objArry.fbo_get_fbo_date = currentDateTime;
        Xrm.WebApi.updateRecord("opportunity", OID, objArry).then(
                         function success(result) {
                             debugger;
                             getFBO();
                         },
                         function (error) {
                         }
                     );
    } catch (e) {
    }
}
getFBO();
getdetails();
getopportunity();
//END
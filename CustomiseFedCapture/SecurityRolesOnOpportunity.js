﻿function SecurityRole() {
    //$("#fedcap_reasonlost_awrd option[value='number:1']").prop('disabled',true);
    //StarRatingCalculation();
    GreenRedYellowOption();
    getUerRoles();
}
function GreenRedYellowOption() {
    try {
        if ($('#fedcap_programunderstanding').length > 0) {
            $("#fedcap_programunderstanding option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "We have agreement on key capture staff and their continued commitment to work on this capture.  Specifically, at PNP you must have a committed capture manager and solution architect/technical lead, identified program manager and proposal manager and committed by Pricing Strategy 2.  By BNB, these roles need to be unchanged through Pre-Proposal to be green." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "Key roles identified but no line management commitment on some/all.  Any current key roles have changed in staffing.  The program manager, or possible PMs are not identified by PNP.   If any of these are true, then this section is yellow." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "If we have no CM during Qualification post CS1, we are red.  Our solution architect/technical lead must be identified no later than CS2. The proposal manager must be identified and involved in Section-Level Strategy Workshops in the Pre- Proposal phase.  If the program manager is not committed by BNB or changed after BNB, it is red.  If CM staffing has changed after PNP, it is red." });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_pastperformance').length > 0) {
            $("#fedcap_pastperformance option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "We identified and vetted all past performance references.  We identified and vetted at least one additional reference, to use if necessary.  We continue to monitor these references with existing PM and senior leadership to ensure relevance and continued successful program performance." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "We identified some relevant programs and are working on the rest.  We successfully addressed past performance issues, if any, and can count on customers’ positive feedback on most references.  If either is true, then this section is yellow." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "We have not identified enough relevant programs.  We have unaddressed past performance issues on current relevant programs.  If any of these are true, then this section is red." });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_customerhotbutton').length > 0) {
            $("#fedcap_customerhotbutton option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "We know our Customer’s wants, concerns and priorities (e.g., budget, requirements, technologies) and are confident these will be defined in the RFP and scored in the evaluation instructions (Section M)." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "We know some of our Customer’s wants, concerns and priorities (e.g., budget, requirements, technologies) and are unsure if these will be defined in the RFP and scored in the evaluation instructions (Section M)." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "We are still investigating the customer wants.  Customer has not clearly communicated (or may not know yet) their wants. (Note: This may be a great shaping opportunity for us, if it is early in the capture!)" });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_shaping').length > 0) {
            $("#fedcap_shaping option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "We have a clear shaping strategy and have implemented it.  At least in one case, we have seen success in the Customer changing their requirements to match our shaping strategy (e.g., prior to draft RFP the customer stated directly to us they would include the specific requirement; the draft RFP includes the requirement; final RFP includes the requirement and we will be evaluated on it (Section M))." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "We have a clear shaping strategy and are implementing it.  We have had some success in shaping Customer’s requirements, but are unsure whether these will translate to RFP requirements." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "We do not have a clear shaping strategy but we are working to develop one.  We have a shaping strategy, but it did not resonate with our customer.  We are working with senior leadership to revise our strategy.  If any of these are true, then this section is red." });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_customerrelationship').length > 0) {
            $("#fedcap_customerrelationship option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "We are in continuous open communication with our primary customer contact and other key stakeholders in the Customer’s organization. We are maintaining these relationships at multiple levels using senior account executives, our leadership, and capture team.  We have a Champion within the Customer’s organization.  Customer initiates conversations and engagements with us." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "We have limited contacts in the Customer’s organization.  We get most of our information (during qualification phase) through second-hand sources (e.g., consultants, industry sources).   We are working to identify a Champion." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "We have no or minimal working relationships with this Customer (e.g. new Customer).  We have worked with this Customer before but have difficulties forming a solid working relationship and we do not have a Champion.  If either is true, then this section is red." });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_ciptw').length > 0) {
            $("#fedcap_ciptw option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "We know our key competitors and have identified potential “dark horses” that may emerge during the competition.  We know their likely solution approaches and price strategies.  Based on our CI, we know how we and our competition will likely be scored.  We have aligned our solution and a price strategy with the PTW.  We can successfully execute within those parameters." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "We know most likely competitors but are working to understand their strategy.  We have just begun to engage appropriate CI/PTW resources.  We cannot estimate our ability to successfully execute the program.  If any of these are true, then this section is yellow." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "We are still working to understand the competitive field.  For a large $ value capture, we have not initiated formal CI or PTW support at this time.  If either is true, then this section is red." });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_discriminatorsdifferentiators').length > 0) {
            $("#fedcap_discriminatorsdifferentiators option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "A discriminator is unique to us, and no other competitor can offer the same or similar capability.  Leidos has at least one capability that is a discriminator.  Our customer specifically wants this discriminator as evidenced by discussions, DRFP or RFP." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "At this time, we have one or more capabilities we believe are discriminators but have not yet validated them." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "At this time, our main competitor has at least one discriminator of interest to our customer that we cannot offer.  We have not identified any discriminators that are specific to Leidos that could resonate with our customer.  If either is true, then this section is red." });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_coststrategy').length > 0) {
            $("#fedcap_coststrategy option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "Our technical, management and transition approaches align with the developed price strategy.  Our proposed program manager (or line management, prior to PM assignment) believes the program is executable." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "Our technical, management and/or transition approaches do not yet align with the developed price strategy." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "We have not held pricing strategy sessions.  After holding a pricing strategy session, we did not have an adequate understanding of the current program, customer or competition to devise a winning strategy. If either is true, then this section is red.  (Note:  If Program Understanding, Customer Relationships or CI/PTW are red, price strategy is also likely to be red.)" });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_technicalsolution').length > 0) {
            $("#fedcap_technicalsolution option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "Our technical solution has at least one discriminator and aligns with the CI/PTW position and price strategy.  We vetted key components of our technical offering with Customer stakeholders and Champion(s) and received positive responses.  We mitigated all perceived technical risks on this program.  We conducted a TRR (or plan to have one) and have a satisfactory approach." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "Our technical solution does not include discriminator(s) but aligns with the CI/PTW position and/or price strategy.  Our technical solution includes discriminator(s) but does not align with the CI/PTW position and/or price strategy.  Our technical solution is not resonating with the customer.  We are still identifying risks. If any of these are true, then this section is yellow." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "Our technical solution does not have any discriminator(s) and does not align with the CI/PTW position and/or price strategy. If is true, then this section is red.   (NOTE: If CI/PTW or price strategy are red, then the technical solution is also likely to be red.)" });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_managementstaffingstrategy').length > 0) {
            $("#fedcap_managementstaffingstrategy option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "We secured all key personnel (and they align with our price strategy), vetted each with our customer(s) and received positive responses.  We have an executable strategy in place to staff remaining positions, including special requirements (e.g., clearances, incumbent capture, etc).  We mitigated all perceived management risks on this program.  We conducted an XPR (or plan to have one, as required) and have a satisfactory approach. We have worked with BRR on all risk areas." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "We identified some key personnel and began the vetting process with our customer.  Our key personnel partially align with our price strategy and we have a partial approach for meeting the remaining staffing requirements.  We are still identifying risks.  If any of these are true, then this section is yellow." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "We have not identified key personnel yet (at PNP) or we have not secured key personnel (at BNB).  We do not have a strategy for addressing special requirements (e.g., clearances, incumbent capture, etc).  We do not have a staffing strategy that aligns with the pricing strategy yet.  We do not have a pricing strategy for personnel.  If any of these are true, then this section is red." });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_transitionstrategy').length > 0) {
            $("#fedcap_transitionstrategy option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "We mitigated all perceived transition risks on this program.  We identified staff, have hiring processes in place (as necessary), and have a viable implementation schedule that meets all customer milestones.  We have an agreed approach (and line management commitment) to perform any pre-award tasks.  We worked with BRR and have a satisfactory approach." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "We identified perceived transition risks on this program and documented them in the risk register and discussed with BRR staff.  We are developing mitigation strategies for remaining risks.  We have identified need of pre-award tasks, but do not have commitment to perform. If any of these are true, then this section is yellow." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "We have not identified transition risks on this program yet.  We do not have a pricing strategy for transition.  We have not developed an implementation schedule yet.  There are customer milestones that we still do not have an approach to meet.   If any of these are true, then this section is red." });
                    $(this).attr({ "class": "red" });
                }
            });
        }
        if ($('#fedcap_captureproposalresources').length > 0) {
            $("#fedcap_captureproposalresources option").each(function (name, val) {
                if (val.text === "Green") {
                    $(this).attr({ "title": "We have agreement on key capture staff and their continued commitment to work on this capture.  Specifically, at PNP you must have a committed capture manager and solution architect/technical lead, identified program manager and proposal manager and committed by Pricing Strategy 2.  By BNB, these roles need to be unchanged through Pre-Proposal to be green." });
                    $(this).attr({ "class": "green" });
                }
                if (val.text === "Yellow") {
                    $(this).attr({ "title": "Key roles identified but no line management commitment on some/all.  Any current key roles have changed in staffing.  The program manager, or possible PMs are not identified by PNP.   If any of these are true, then this section is yellow." });
                    $(this).attr({ "class": "yellow" });
                }
                if (val.text === "Red") {
                    $(this).attr({ "title": "If we have no CM during Qualification post CS1, we are red.  Our solution architect/technical lead must be identified no later than CS2. The proposal manager must be identified and involved in Section-Level Strategy Workshops in the Pre- Proposal phase.  If the program manager is not committed by BNB or changed after BNB, it is red.  If CM staffing has changed after PNP, it is red." });
                    $(this).attr({ "class": "red" });
                }
            });
        }
    } catch (e) {

    }
}
function StarRatingCalculation() {
    debugger;
    try {
        //$('#img_calculated_fedcap_opportunityhealthrating').attr('src', "/WebResources/fedcap_NoStarIndicator.png");
        var returnURL = "/WebResources/fedcap_NoStarIndicator.png";

        var sumOfStarRating = 0;
        var sumOfFieldsCount = 0;
        if ($('#fedcap_knowledgeofcustomer').length > 0) {
            var fedcap_knowledgeofcustomer = $('#fedcap_knowledgeofcustomer').val();
            if (fedcap_knowledgeofcustomer !== "") {
                var fedcap_knowledgeofcustomerId = fedcap_knowledgeofcustomer.replace('number:', '').replace('string:', '').replace(/,/g, '');
                var knowledgeofcustomerScore = parseInt(fedcap_knowledgeofcustomerId);
                sumOfStarRating = sumOfStarRating + knowledgeofcustomerScore;
            }
            sumOfFieldsCount = sumOfFieldsCount + 1;
        }
        if ($('#fedcap_managementsolutionmatchtoreq').length > 0) {
            var fedcap_managementsolutionmatchtoreq = $('#fedcap_managementsolutionmatchtoreq').val();
            if (fedcap_managementsolutionmatchtoreq !== "") {
                var fedcap_managementsolutionmatchtoreqId = fedcap_managementsolutionmatchtoreq.replace('number:', '').replace('string:', '').replace(/,/g, '');
                var managementsolutionmatchtoreqScore = parseInt(fedcap_managementsolutionmatchtoreqId);
                sumOfStarRating = sumOfStarRating + managementsolutionmatchtoreqScore;
            }
            sumOfFieldsCount = sumOfFieldsCount + 1;
        }
        if ($('#fedcap_availabilityofkeypersonnel').length > 0) {
            var fedcap_availabilityofkeypersonnel = $('#fedcap_availabilityofkeypersonnel').val();
            if (fedcap_availabilityofkeypersonnel !== "") {
                var fedcap_availabilityofkeypersonnelId = fedcap_availabilityofkeypersonnel.replace('number:', '').replace('string:', '').replace(/,/g, '');
                var availabilityofkeypersonnelScore = parseInt(fedcap_availabilityofkeypersonnelId);
                sumOfStarRating = sumOfStarRating + availabilityofkeypersonnelScore;
            }
            sumOfFieldsCount = sumOfFieldsCount + 1;
        }
        if ($('#fedcap_matchtostrategicfocus').length > 0) {
            var fedcap_matchtostrategicfocus = $('#fedcap_matchtostrategicfocus').val();
            if (fedcap_matchtostrategicfocus !== "") {
                var fedcap_matchtostrategicfocusId = fedcap_matchtostrategicfocus.replace('number:', '').replace('string:', '').replace(/,/g, '');
                var matchtostrategicfocusScore = parseInt(fedcap_matchtostrategicfocusId);
                sumOfStarRating = sumOfStarRating + matchtostrategicfocusScore;
            }
            sumOfFieldsCount = sumOfFieldsCount + 1;
        }
        if ($('#fedcap_technicalsolutionmatchtoreq').length > 0) {
            var fedcap_technicalsolutionmatchtoreq = $('#fedcap_technicalsolutionmatchtoreq').val();
            if (fedcap_technicalsolutionmatchtoreq !== "") {
                var fedcap_technicalsolutionmatchtoreqId = fedcap_technicalsolutionmatchtoreq.replace('number:', '').replace('string:', '').replace(/,/g, '');
                var technicalsolutionmatchtoreqScore = parseInt(fedcap_technicalsolutionmatchtoreqId);
                sumOfStarRating = sumOfStarRating + technicalsolutionmatchtoreqScore;
            }
            
            sumOfFieldsCount = sumOfFieldsCount + 1;
        }
        if ($('#fedcap_relatedpastperformance').length > 0) {
            var fedcap_relatedpastperformance = $('#fedcap_relatedpastperformance').val();
            if (fedcap_relatedpastperformance !== "") {
                var fedcap_relatedpastperformanceId = fedcap_relatedpastperformance.replace('number:', '').replace('string:', '').replace(/,/g, '');
                var relatedpastperformanceScore = parseInt(fedcap_relatedpastperformanceId);
                sumOfStarRating = sumOfStarRating + relatedpastperformanceScore;
            }
            sumOfFieldsCount = sumOfFieldsCount + 1;
        }
        if ($('#fedcap_teaming').length > 0) {
            var fedcap_teaming = $('#fedcap_teaming').val();
            if (fedcap_teaming !== "") {
                var fedcap_teamingId = fedcap_teaming.replace('number:', '').replace('string:', '').replace(/,/g, '');
                var teamingScore = parseInt(fedcap_teamingId);
                sumOfStarRating = sumOfStarRating + teamingScore;
            }
            sumOfFieldsCount = sumOfFieldsCount + 1;
        }
        var ImageBindValue = sumOfStarRating / sumOfFieldsCount;
        debugger;
        ImageBindValue = Math.round(ImageBindValue);
        if (ImageBindValue === 1) {
            //$('#img_calculated_fedcap_opportunityhealthrating').attr('src', "/WebResources/fedcap_OneStarIndicator.png");
            returnURL = "/WebResources/fedcap_OneStarIndicator.png";
        }
        else if (ImageBindValue === 2) {
            //$('#img_calculated_fedcap_opportunityhealthrating').attr('src', "/WebResources/fedcap_TwoStarIndicator.png");
            returnURL = "/WebResources/fedcap_TwoStarIndicator.png";
        }
        else if (ImageBindValue === 3) {
            //$('#img_calculated_fedcap_opportunityhealthrating').attr('src', "/WebResources/fedcap_ThreeStarIndicator.png");
            returnURL = "/WebResources/fedcap_ThreeStarIndicator.png";
        }
        else if (ImageBindValue === 4) {
            //$('#img_calculated_fedcap_opportunityhealthrating').attr('src', "/WebResources/fedcap_FourStarIndicator.png");
            returnURL = "/WebResources/fedcap_FourStarIndicator.png";

        }
        else if (ImageBindValue === 5) {
            //$('#img_calculated_fedcap_opportunityhealthrating').attr('src', "/WebResources/fedcap_FiveStarIndicator.png");
            returnURL = "/WebResources/fedcap_FiveStarIndicator.png";
        }
        else {
            //$('#img_calculated_fedcap_opportunityhealthrating').attr('src', "/WebResources/fedcap_NoStarIndicator.png");
            returnURL = "/WebResources/fedcap_NoStarIndicator.png";
        }
    } catch (e) {

    }
    return returnURL;
}
var isGetUerRoles = false;//Avoid multiple calling on tab click again again
var ifAdmin = false;
var ifBDSuperUser = false;
function getUerRoles() {
    var roleid = Xrm.Page.context.getUserRoles();
    var name;
    if (!isGetUerRoles) {
        isGetUerRoles = true;
        for (var i = 0; i < roleid.length; i++) {
            var roleID = roleid[i];
            var RoleName = getRoleName(roleID);
            if (RoleName == 'System Administrator') {
                ifAdmin = true;
            }
        }
    }
    if (ifAdmin == true) {
        $("#btnEditWinPlan").css("display", "inline-block");

    }
    else {
        //If user is not admin then Disable it
        $("#btnEditWinPlan").css("display", "none");
    }

}
function getRoleName(roleID) {
    var roleName = null;
    try {
        var serverUrl = Xrm.Page.context.getClientUrl();
        var OdataURL = serverUrl + "/XRMServices/2011/OrganizationData.svc" + "/" + "RoleSet?$filter=RoleId eq guid'" + roleID + "'";
        $.ajax({
            type: "GET", async: false, contentType: "application/json; charset=utf-8", datatype: "json", url: OdataURL,
            beforeSend:
            function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
            },
            success:
            function (data, textStatus, XmlHttpRequest) {
                var result = data.d;
                roleName = result.results[0].Name;
            },
            error:
            function (XmlHttpRequest, textStatus, errorThrown) {
            }
        });
    } catch (e) {

    }
    return roleName;
}

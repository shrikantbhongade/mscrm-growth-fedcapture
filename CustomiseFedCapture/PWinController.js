﻿(function () {
    "use strict";
    var app = angular.module('MyApp', []);
    app.controller('PWinController', PWinController);
    function PWinController($scope) {
        $scope.pwintabs = [];
        GetOptionSetLabel("fedcap_pwinquestion", "fedcap_stage");
        for (var i = 0; i < $scope.pwintabs.length; i++) {
            GetQuestion(i);
        }
        function GetOptionSetLabel(EntityLogicalName, AttributeLogicalName) {
            try {
                SDK.Metadata.RetrieveAttribute(EntityLogicalName, AttributeLogicalName, "00000000-0000-0000-0000-000000000000", true,
                    function (result) {
                        for (var i = 0; i < result.OptionSet.Options.length; i++) {
                            var value = result.OptionSet.Options[i].Label.LocalizedLabels[0].Label;
                            $scope.pwintabs.push({ "id": result.OptionSet.Options[i].Value, "value": result.OptionSet.Options[i].Label.LocalizedLabels[0].Label })
                            GetQuestion(i);
                        }
                        $scope.$apply();
                    },
                    function (error) {
                    }
                );

            } catch (e) {

            }
        }
        function GetQuestion(StageIndex) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_pwinquestions?$filter=fedcap_stage eq " + StageIndex + "&$orderby=fedcap_sequence asc", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            var data = JSON.parse(this.response);
                            $scope.pwintabs[StageIndex].questions = data.value;
                            for (var i = 0; i < data.value.length; i++) {
                                GetAnswer(StageIndex, i, data.value[i]["fedcap_pwinquestionid"]);
                            }
                        }
                        else {
                            var error = JSON.parse(this.response).error;
                            Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
        function GetAnswer(StageIndex,QueIndex, QuestionId) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_pwinanswers?$filter=_fedcap_question_value eq " + QuestionId + "", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            var data = JSON.parse(this.response);
                            $scope.pwintabs[StageIndex].questions[QueIndex].answers = data.value;
                        }
                        else {
                            var error = JSON.parse(this.response).error;
                            Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
    }
})();
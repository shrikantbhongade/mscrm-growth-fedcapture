﻿var Xrm = parent.Xrm;
function getDaysBetweenDates(start, end, dayName) {
    var result = [];
    var days = { sun: 0, mon: 1, tue: 2, wed: 3, thu: 4, fri: 5, sat: 6 };
    var day = days[dayName.toLowerCase().substr(0, 3)];
    // Copy start date
    var current = new Date(start);
    // Shift to next of required days
    current.setDate(current.getDate() + (day - current.getDay() + 7) % 7);
    // While less than end date, add dates to result array
    while (current < end) {
        result.push(new Date(+current));
        current.setDate(current.getDate() + 7);
    }
    return result;
}
function GetViewID() {
    try {
        var selectedViewId = window.parent.document.getElementsByTagName("span");
        
        var view = document.getElementById('crmGrid_SavedNewQuerySelector');
        var firstChild = view.firstChild;
        //var currentview = firstChild.currentview;
        var currentview = firstChild.getAttribute("currentview")
        if (currentview) {
            var viewId = currentview.valueOf();
            return viewId;
        }
    } catch (e) {
        debugger;
        //var x = window.parent.document.getElementsByTagName("span")[0].getAttribute("title");
        //var titleName = window.parent.document.getElementsByAttribute("Select a view");
        //var className = window.parent.document.getElementsByClassName("ak a fc w fv   flexbox");
        //var viewId = className[0].id.split('_')[2];
        //return viewId;
        //var head = parent.window.document.getElementsByTagName('head')[0];
        //var link = parent.window.document.createElement('script');
        //link.type = 'text/javascript';
        //link.src = "https://fedcap.crm.dynamics.com/webresources/fedcap_JavaScript1.4.js";
        //head.appendChild(link);

        //var head1 = parent.window.document.getElementsByTagName('head')[0];
        //var link1 = parent.window.document.createElement('script');
        //link1.type = 'text/javascript';
        //link1.src = "https://fedcap.crm.dynamics.com/webresources/fedcap_JavaScript1.8.js";
        //head1.appendChild(link1);

        //var viewId = "{58FDDA93-3DC6-E811-A965-000D3A30D0CA}"; //All Opportunities - Gannt Chart (View ID)
        var viewId = "{11ea25c0-3ec6-e811-a965-000d3a30d0ca}"; //All Opportunities - Gannt Chart (View ID)
        return viewId;
    }
    
}
function GetSelectedViewRecords(viewId, viewType) {
    debugger;
    var fetchXml = "";
    var xmlArray = [];
    if (viewType == true) {
        //var getURL = Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities?savedQuery=" + viewId;// system View
        var getURL = "?savedQuery=" + viewId;// system View
    }
    else {
        //var getURL = Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities?userQuery=" + viewId;// personal View
        var getURL = "?userQuery=" + viewId;// personal View
    }
    try {
        Xrm.WebApi.retrieveMultipleRecords("opportunity", getURL)
           .then(function (response) {
               debugger;
               var data = response;
               var result = data;
               var checkCount = 0;
               var view = 0;
               //if (result.value.length > 100) {
               //    checkCount = 100;
               //}
               //else {
               //    checkCount = result.value.length
               //}
               // Creating Batch because if the records gets more than 100 then records is skipping while XLS generating
               var batchCount = parseInt(result.entities.length / 75);
               var reminder = result.entities.length % 75;
               var batchCountAdditional = 0;
               if (batchCount > 0) {
                   for (var count = 1; count <= batchCount; count++) {
                       batchCountAdditional = count;
                       view = count * 75 - 75;
                       checkCount = count * 75;
                       fetchXml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">';
                       fetchXml += '<entity name="opportunity">'
                       fetchXml += '<attribute name="name" />'
                       fetchXml += '<attribute name="opportunityid" />'
                       fetchXml += '<attribute name="fedcap_rfiduedate" />'
                       fetchXml += '<attribute name="fedcap_draftrfpdate" />'
                       fetchXml += '<attribute name="fedcap_proposalduedate" />'
                       fetchXml += '<attribute name="fedcap_ensduedate" />'
                       fetchXml += '<attribute name="fedcap_awarddate" />'
                       fetchXml += '<attribute name="actualclosedate" />'
                       fetchXml += '<attribute name="fedcap_rfpdate" />'
                       fetchXml += '<attribute name="fedcap_primesub" />'
                       fetchXml += '<attribute name="fedcap_bul_business_unit_lead" />'
                       fetchXml += '<attribute name="fedcap_capturemanager" />'
                       fetchXml += '<attribute name="fedcap_solutioning" />'
                       fetchXml += '<attribute name="fedcap_proposal_manager" />'
                       fetchXml += '<filter type="and">'
                       fetchXml += '<condition attribute="opportunityid" operator="in">'
                       for (view1 = view; view1 < checkCount; view1++) {
                           fetchXml += '<value>' + result.entities[view1]["opportunityid"] + '</value>';
                       }
                       fetchXml += '</condition>'
                       fetchXml += '</filter>'
                       fetchXml += '</entity>'
                       fetchXml += '</fetch>'
                       xmlArray.push(fetchXml);
                   }
                   if (reminder > 0) {
                       view = batchCount * 75;
                       checkCount = batchCount * 75 + reminder;
                       fetchXml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">';
                       fetchXml += '<entity name="opportunity">'
                       fetchXml += '<attribute name="name" />'
                       fetchXml += '<attribute name="opportunityid" />'
                       fetchXml += '<attribute name="fedcap_rfiduedate" />'
                       fetchXml += '<attribute name="fedcap_draftrfpdate" />'
                       fetchXml += '<attribute name="fedcap_proposalduedate" />'
                       fetchXml += '<attribute name="fedcap_ensduedate" />'
                       fetchXml += '<attribute name="fedcap_awarddate" />'
                       fetchXml += '<attribute name="actualclosedate" />'
                       fetchXml += '<attribute name="fedcap_rfpdate" />'
                       fetchXml += '<attribute name="fedcap_primesub" />'
                       fetchXml += '<attribute name="fedcap_bul_business_unit_lead" />'
                       fetchXml += '<attribute name="fedcap_capturemanager" />'
                       fetchXml += '<attribute name="fedcap_solutioning" />'
                       fetchXml += '<attribute name="fedcap_proposal_manager" />'
                       fetchXml += '<filter type="and">'
                       fetchXml += '<condition attribute="opportunityid" operator="in">'
                       for (view1 = view; view1 < checkCount; view1++) {
                           fetchXml += '<value>' + result.entities[view1]["opportunityid"] + '</value>';
                       }
                       fetchXml += '</condition>'
                       fetchXml += '</filter>'
                       fetchXml += '</entity>'
                       fetchXml += '</fetch>'
                       xmlArray.push(fetchXml);
                   }
               }
               else {
                   view = 0;
                   checkCount = reminder;
                   fetchXml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">';
                   fetchXml += '<entity name="opportunity">'
                   fetchXml += '<attribute name="name" />'
                   fetchXml += '<attribute name="opportunityid" />'
                   fetchXml += '<attribute name="fedcap_rfiduedate" />'
                   fetchXml += '<attribute name="fedcap_draftrfpdate" />'
                   fetchXml += '<attribute name="fedcap_proposalduedate" />'
                   fetchXml += '<attribute name="fedcap_ensduedate" />'
                   fetchXml += '<attribute name="fedcap_awarddate" />'
                   fetchXml += '<attribute name="actualclosedate" />'
                   fetchXml += '<attribute name="fedcap_rfpdate" />'
                   fetchXml += '<attribute name="fedcap_primesub" />'
                   fetchXml += '<attribute name="fedcap_bul_business_unit_lead" />'
                   fetchXml += '<attribute name="fedcap_capturemanager" />'
                   fetchXml += '<attribute name="fedcap_solutioning" />'
                   fetchXml += '<attribute name="fedcap_proposal_manager" />'
                   fetchXml += '<filter type="and">'
                   fetchXml += '<condition attribute="opportunityid" operator="in">'
                   for (view = 0; view < checkCount; view++) {
                       fetchXml += '<value>' + result.entities[view]["opportunityid"] + '</value>';
                   }
                   fetchXml += '</condition>'
                   fetchXml += '</filter>'
                   fetchXml += '</entity>'
                   fetchXml += '</fetch>'
                   xmlArray.push(fetchXml);
               }
               debugger;
               if (xmlArray.length == 0) {
                   alert("Records not found.");
                   return;
               }
               else {
                   //var encodedFetchXml = encodeURI(fetchXml);
                   var dt = new Date();
                   var day = dt.getDate();
                   var month = dt.getMonth() + 1;
                   var year = dt.getFullYear();
                   var hour = dt.getHours();
                   var mins = dt.getMinutes();
                   var postfix = month + "." + day + "." + year + "_" + hour + "." + mins;
                   //creating a temporary HTML link element (they support setting file names)
                   //getting data from our div that contains the HTML table
                   var data_type = 'data:application/vnd.ms-excel';
                   var table_div = document.getElementById('dvData');
                   //var table_html = table_div.outerHTML.replace(/ /g, '%20');
                   var today = new Date();
                   var table = '<div id="dvtest">';
                   table += '<table border="1" style="font-family:Segoe UI"><thead>';
                   table += '<tr><th colspan="17" style="font-size:small; text-align:left">GovCon Suite Gantt Chart as of "' + FormatDate(today) + '" - Based on GovCon Suite "' + today.getFullYear().toString() + '" Pipeline</th></tr>';
                   table += '<tr style="font-size:x-small;bold:true"><td colspan="2"><b>Legends</b></td></tr>';
                   table += '<tr style="font-size:x-small"><td>RFI Due Date to Draft RFP Date</td><td style="background-color:#d9d9d9"></td></tr>';
                   table += '<tr style="font-size:x-small"><td>Draft RFP Date to Proposal Due Date</td><td style="background-color:#ffff00"></td></tr>';
                   table += '<tr style="font-size:x-small"><td>Proposal Due Date to ENs Due Date</td><td style="background-color:#92d050"></td></tr>';
                   table += '<tr style="font-size:x-small"><td>ENs Due Date to Award Date</td><td style="background-color:#8eb4e3"></td></tr>';
                   table += '<tr style="font-size:x-small"><td>Award Date to Actual win/lost date</td><td style="background-color:#376092"></td></tr>';
                   table += '<tr style="font-size:xx-small"><td colspan="7">Monthly Pursuit Totals --></td></tr>';
                   table += '<tr style="width:100px; font-size:xx-small"><th style="width:500px;"> Opportunity Name</th><th> RFP Date</th><th> Prime/ Sub</th><th> BD Rep</th><th> Cap Mgr</th><th> SA</th><th> Prop Mgr</th>';
                   var dateResult = getDaysBetweenDates(new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7)
                       , new Date(today.getFullYear() + 1, today.getMonth(), today.getDate() - 1), 'Sun');
                   for (var i = 0; i <= dateResult.length - 1; i++) {
                       var dateForCol = dateResult[i];
                       if (dateForCol == undefined) {
                       }
                       else {
                           var dayCol = dateForCol.getDate();
                           var monthCol = dateForCol.getMonth() + 1;
                           var yearCol = dateForCol.getFullYear();
                           var concatDate = monthCol + "/" + dayCol + "/" + yearCol;
                           table += '<th style="font-size:xx-small; background:#A6A6A6"> ' + concatDate + '</th>';
                       }
                   }
                   table += "</tr>";
                   table += "<tbody>";
                   debugger;
                   var countForDownload = 0;
                   for (var xml = 0; xml < xmlArray.length; xml++) {
                       var encodedFetchXml = encodeURI(xmlArray[xml]);
                       try {
                           Xrm.WebApi.retrieveMultipleRecords("opportunity", "?fetchXml=" + encodedFetchXml)
                              .then(function (responseFetchXml) {
                                  debugger;
                                  countForDownload = countForDownload + 1;
                                  var result = responseFetchXml;
                                  debugger;
                                  for (var j = 0; j < result.entities.length; j++) {
                                      try {
                                          var fedcap_rfpdate = result.entities[j]["fedcap_rfpdate"];
                                          var fedcap_rfiduedate = result.entities[j]["fedcap_rfiduedate"];
                                          var fedcap_draftrfpdate = result.entities[j]["fedcap_draftrfpdate"];
                                          var fedcap_proposalduedate = result.entities[j]["fedcap_proposalduedate"];
                                          var fedcap_ensduedate = result.entities[j]["fedcap_ensduedate"];
                                          var fedcap_awarddate = result.entities[j]["fedcap_awarddate"];
                                          var actualclosedate = result.entities[j]["actualclosedate"];
                                          var role = result.entities[j]["fedcap_primesub@OData.Community.Display.V1.FormattedValue"];
                                          var fedcap_primesub = (result.entities[j]["fedcap_primesub@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.entities[j]["fedcap_primesub@OData.Community.Display.V1.FormattedValue"];
                                          var fedcap_bul_business_unit_lead = (result.entities[j]["fedcap_bul_business_unit_lead"] == undefined) ? "" : result.entities[j]["fedcap_bul_business_unit_lead"];
                                          var fedcap_capturemanager = (result.entities[j]["fedcap_capturemanager"] == undefined) ? "" : result.entities[j]["fedcap_capturemanager"];
                                          var fedcap_solutioning = (result.entities[j]["fedcap_solutioning"] == undefined) ? "" : result.entities[j]["fedcap_solutioning"];
                                          var fedcap_proposal_manager = (result.entities[j]["fedcap_proposal_manager"] == undefined) ? "" : result.entities[j]["fedcap_proposal_manager"];
                                          table += "<tr>";
                                          table += '<td style="font-size:xx-small;">' + result.entities[j]["name"] + '</td>';
                                          if (result.entities[j]["fedcap_rfiduedate"] == null) {
                                              table += '<td style="font-size:xx-small;"></td>';
                                          }
                                          else {
                                              //var formatedDate = FormatDate(new Date(result.entities[j]["fedcap_rfpdate"]));
                                              var formatedDate = result.entities[j]["fedcap_rfpdate@OData.Community.Display.V1.FormattedValue"];
                                              if (formatedDate === undefined) {
                                                  formatedDate = "";
                                              }
                                              table += '<td style="font-size:xx-small;">' + formatedDate + '</td>';
                                          }
                                          table += "<td style='font-size:xx-small;'>" + fedcap_primesub + "</td>";
                                          table += "<td style='font-size:xx-small;'>" + fedcap_bul_business_unit_lead + "</td>";
                                          table += "<td style='font-size:xx-small;'>" + fedcap_capturemanager + "</td>";
                                          table += "<td style='font-size:xx-small;'>" + fedcap_solutioning + "</td>";
                                          table += "<td style='font-size:xx-small;'>" + fedcap_proposal_manager + "</td>";

                                          for (var i = 0; i <= dateResult.length - 1; i++) {
                                              var dateForRow = dateResult[i];
                                              var fedcap_rfiduedateChanged = false, fedcap_draftrfpdateChanged = false, fedcap_proposalduedateChanged = false, fedcap_ensduedateChanged = false, fedcap_awarddateChanged = false, fedcap_rfpdateChanged = false, actualclosedateChanged = false;

                                              if (dateForRow == undefined) {
                                              }
                                              else {
                                                  var todayDate = new Date();
                                                  var tdInsert = true;
                                                  if (result.entities[j]["fedcap_rfiduedate"] === undefined && result.entities[j]["fedcap_draftrfpdate"] === undefined && result.entities[j]["fedcap_proposalduedate"] === undefined
                                                      && result.entities[j]["fedcap_ensduedate"] === undefined && result.entities[j]["fedcap_awarddate"] === undefined && result.entities[j]["actualclosedate"] === undefined) {
                                                      tdInsert = true;
                                                  }
                                                  else {
                                                      tdInsert = false;
                                                  }
                                                  if (result.entities[j]["fedcap_rfiduedate"] === undefined) {
                                                      //fedcap_rfiduedate = FormatDate(todayDate - 1);
                                                  }
                                                  else {
                                                      if (result.entities[j]["fedcap_draftrfpdate"] === undefined) {
                                                          tdInsert = false;
                                                          if (result.entities[j]["fedcap_rfiduedate"] === undefined) {
                                                              fedcap_rfiduedate = dateResult[i];
                                                              fedcap_rfiduedateChanged = true;
                                                          }
                                                          fedcap_draftrfpdate = dateResult[i];
                                                          fedcap_draftrfpdateChanged = true;
                                                      }
                                                      if (result.entities[j]["fedcap_proposalduedate"] === undefined) {
                                                          tdInsert = false;
                                                          fedcap_proposalduedate = dateResult[i];
                                                          fedcap_proposalduedateChanged = true;
                                                      }
                                                      if (result.entities[j]["fedcap_ensduedate"] === undefined) {
                                                          tdInsert = false;
                                                          fedcap_ensduedate = dateResult[i];
                                                          fedcap_ensduedateChanged = true;
                                                      }
                                                      if (result.entities[j]["fedcap_awarddate"] === undefined) {
                                                          tdInsert = false;
                                                          fedcap_awarddate = dateResult[i];
                                                          fedcap_awarddateChanged = true;
                                                      }
                                                      if (result.entities[j]["actualclosedate"] === undefined) {
                                                          tdInsert = false;
                                                          actualclosedate = dateResult[i];
                                                          actualclosedateChanged = true;
                                                      }
                                                  }
                                                  if (tdInsert) {
                                                      table += '<td></td>';
                                                  }
                                                  else {
                                                      var fedcap_rfiduedateDate;
                                                      var fedcap_draftrfpdateDate;
                                                      var fedcap_proposalduedateDate;
                                                      var fedcap_ensduedateDate;
                                                      var fedcap_awarddateDate;
                                                      var actualclosedateDate;
                                                      var fedcap_rfpdate_MidDate;
                                                      if (!fedcap_rfiduedateChanged) {
                                                          fedcap_rfiduedateDate = FormatDateUTC(new Date(fedcap_rfiduedate));
                                                      }
                                                      else {
                                                          fedcap_rfiduedateDate = fedcap_rfiduedate;
                                                      }
                                                      if (!fedcap_draftrfpdateChanged) {
                                                          fedcap_draftrfpdateDate = FormatDateUTC(new Date(fedcap_draftrfpdate));
                                                      }
                                                      else {
                                                          fedcap_draftrfpdateDate = fedcap_draftrfpdate;
                                                      }
                                                      if (!fedcap_proposalduedateChanged) {
                                                          fedcap_proposalduedateDate = FormatDateUTC(new Date(fedcap_proposalduedate));
                                                      }
                                                      else {
                                                          fedcap_proposalduedateDate = fedcap_proposalduedate;
                                                      }
                                                      if (!fedcap_ensduedateChanged) {
                                                          fedcap_ensduedateDate = FormatDateUTC(new Date(fedcap_ensduedate));
                                                      }
                                                      else {
                                                          fedcap_ensduedateDate = fedcap_ensduedate;
                                                      }
                                                      if (!fedcap_awarddateChanged) {
                                                          fedcap_awarddateDate = FormatDateUTC(new Date(fedcap_awarddate));
                                                      }
                                                      else {
                                                          fedcap_awarddateDate = fedcap_awarddate;
                                                      }
                                                      if (!actualclosedateChanged) {
                                                          actualclosedateDate = FormatDateUTC(new Date(actualclosedate));
                                                      }
                                                      else {
                                                          actualclosedateDate = actualclosedate;
                                                      }

                                                      //1
                                                      if (dateForRow >= fedcap_rfiduedateDate && dateForRow <= fedcap_draftrfpdateDate && dateForRow <= fedcap_proposalduedateDate && //dateForRow <= fedcap_rfpdate_MidDate &&
                                                          dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                          if (fedcap_rfiduedateDate <= fedcap_draftrfpdateDate) {
                                                              table += '<td style="background-color:#d9d9d9"></td>';
                                                          }
                                                      }
                                                          //2
                                                      else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && dateForRow <= fedcap_proposalduedateDate && //dateForRow <= fedcap_rfpdate_MidDate &&
                                                          dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                          if (fedcap_draftrfpdateDate <= fedcap_proposalduedateDate) {
                                                              table += '<td style="background-color:#ffff00"></td>';
                                                          }
                                                      }
                                                          //3
                                                      else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && //dateForRow >= fedcap_rfpdate_MidDate &&
                                                          dateForRow >= fedcap_proposalduedateDate && dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                          if (fedcap_proposalduedateDate <= fedcap_ensduedateDate) {
                                                              table += '<td style="background-color:#92d050"></td>';
                                                          }
                                                      }
                                                          //4
                                                      else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && //dateForRow >= fedcap_rfpdate_MidDate &&
                                                               dateForRow >= fedcap_proposalduedateDate && dateForRow >= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                          if (fedcap_ensduedateDate <= fedcap_awarddateDate) {
                                                              table += '<td style="background-color:#8eb4e3"></td>';
                                                          }
                                                      }
                                                          //5
                                                      else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && //dateForRow >= fedcap_rfpdate_MidDate &&
                                                               dateForRow >= fedcap_proposalduedateDate && dateForRow >= fedcap_ensduedateDate && dateForRow >= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                          if (fedcap_awarddateDate <= actualclosedateDate) {
                                                              table += '<td style="background-color:#376092"></td>';
                                                          }
                                                      }
                                                      else {
                                                          table += '<td></td>';
                                                      }
                                                  }
                                              }
                                          }
                                          table += "</tr>";

                                      } catch (e) {

                                      }
                                  }
                                  debugger;
                                  if (countForDownload === xmlArray.length) {
                                      debugger;
                                      table += "</tbody>";
                                      table += "</table>";
                                      table += "</div>";
                                      //Xrm.Page.ui.clearFormNotification();
                                      //var table_html = table_div.outerHTML.replace(/ /g, '%20');
                                      var table_html = table.replace(/ /g, '%20');
                                      if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) //IF IE > 10
                                      {
                                          var table1 = table;
                                          table1 = table1.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                                          table1 = table1.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                                          table1 = table1.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
                                          if (document.execCommand) {
                                              try {
                                                  var oWin = window.open("about:blank", "_blank");
                                                  //var oWin = window.open("", "test", "resizable=no,scrollbars=yes,width=260,height=225");
                                                  oWin.document.write(table1);
                                                  oWin.document.close();
                                                  var success = oWin.document.execCommand('SaveAs', false, 'GanttChart_' + postfix + '.xls')
                                                  oWin.close();
                                              }
                                              catch (err) {
                                                  alert(err.message());
                                              }
                                          }
                                          else {
                                              alert('Error while downloading!!!');
                                          }
                                      }
                                      else if ((navigator.userAgent.indexOf("Edge") != -1)) //IF IE > 10
                                      {
                                          debugger;
                                          var table1 = table;
                                          table1 = table1.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                                          table1 = table1.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                                          table1 = table1.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
                                          var blob = new Blob([table1], { type: 'data:application/vnd.ms-excel' });
                                          window.navigator.msSaveBlob(blob, 'GanttChart_' + postfix + '.xls');
                                      }
                                      else if (navigator.userAgent.indexOf("Firefox") != -1) {
                                          var uri = 'data:application/vnd.ms-excel;base64,'
                                              , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Opportunities</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>' + table + '</body></html>'
                                              , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                                              , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
                                          var ctx = { worksheet: "test" || 'Worksheet', "dvtest": table }
                                          //window.location.href = uri + base64(format(template, ctx))
                                          let link = document.createElement('a');
                                          link.id = "AccountOpportunity";
                                          link.style.display = "none"; // because Firefox sux
                                          document.body.appendChild(link); // because Firefox sux
                                          link.href = uri + base64(format(template, ctx))
                                          link.download = 'GanttChart_' + postfix + '.xls';
                                          link.click();
                                          document.body.removeChild(link); // because Firefox sux
                                      }
                                      else {
                                          //var a = document.createElement('a');
                                          //a.href = data_type + ', ' + table_html;
                                          ////setting the file name
                                          //a.download = 'GanttChart_' + postfix + '.xls';
                                          ////triggering the function
                                          //document.body.appendChild(a);
                                          //a.click();
                                          ////just in case, prevent default behaviour
                                          //e.preventDefault();
                                          var uri = 'data:application/vnd.ms-excel;base64,'
                                              , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Opportunities</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>' + table + '</body></html>'
                                              , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                                              , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
                                          var ctx = { worksheet: "test" || 'Worksheet', "dvtest": table }
                                          //window.location.href = uri + base64(format(template, ctx))
                                          let link = document.createElement('a');
                                          link.id = "AccountOpportunity";
                                          link.style.display = "none"; // because Firefox sux
                                          document.body.appendChild(link); // because Firefox sux
                                          link.href = uri + base64(format(template, ctx))
                                          link.download = 'GanttChart_' + postfix + '.xls';
                                          link.click();
                                          document.body.removeChild(link); // because Firefox sux
                                      }
                                  }
                              })
                              .fail(function (error) {
                                  debugger;
                                  var data = error;
                              });
                       } catch (e) {
                       }
                   }
                   
               }
           })
           .fail(function (error) {
               debugger;
               var data = error;
           });
    } catch (e) {
    }
   
    return xml;
}
function GetSelectedViewRecords_Classic(viewId, viewType) {
    var fetchXml = "";
    var xml = [];
    if (viewType == true) {
        var getURL = Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities?savedQuery=" + viewId;// system View
    }
    else {
        var getURL = Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities?userQuery=" + viewId;// personal View
    }
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: getURL,// Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities?savedQuery="+ viewId,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
            XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
        },
        async: false,
        success: function (data, textStatus, xhr) {
            var result = data;
            var checkCount = 0;
            var view = 0;

            //if (result.value.length > 100) {
            //    checkCount = 100;
            //}
            //else {
            //    checkCount = result.value.length
            //}
            // Creating Batch because if the records gets more than 100 then records is skipping while XLS generating
            var batchCount = parseInt(result.value.length / 75);
            var reminder = result.value.length % 75;
            var batchCountAdditional = 0;
            if (batchCount > 0) {
                for (var count = 1; count <= batchCount; count++) {
                    batchCountAdditional = count;
                    view = count * 75 - 75;
                    checkCount = count * 75;
                    fetchXml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">';
                    fetchXml += '<entity name="opportunity">'
                    fetchXml += '<attribute name="name" />'
                    fetchXml += '<attribute name="opportunityid" />'
                    fetchXml += '<attribute name="fedcap_rfiduedate" />'
                    fetchXml += '<attribute name="fedcap_draftrfpdate" />'
                    fetchXml += '<attribute name="fedcap_proposalduedate" />'
                    fetchXml += '<attribute name="fedcap_ensduedate" />'
                    fetchXml += '<attribute name="fedcap_awarddate" />'
                    fetchXml += '<attribute name="actualclosedate" />'
                    fetchXml += '<attribute name="fedcap_rfpdate" />'
                    fetchXml += '<attribute name="fedcap_primesub" />'
                    fetchXml += '<attribute name="fedcap_bul_business_unit_lead" />'
                    fetchXml += '<attribute name="fedcap_capturemanager" />'
                    fetchXml += '<attribute name="fedcap_solutioning" />'
                    fetchXml += '<attribute name="fedcap_proposal_manager" />'
                    fetchXml += '<filter type="and">'
                    fetchXml += '<condition attribute="opportunityid" operator="in">'
                    for (view1 = view; view1 < checkCount; view1++) {
                        fetchXml += '<value>' + result.value[view1]["opportunityid"] + '</value>';
                    }
                    fetchXml += '</condition>'
                    fetchXml += '</filter>'
                    fetchXml += '</entity>'
                    fetchXml += '</fetch>'
                    xml.push(fetchXml);
                }
                if (reminder > 0) {
                    view = batchCount * 75;
                    checkCount = batchCount * 75 + reminder;
                    fetchXml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">';
                    fetchXml += '<entity name="opportunity">'
                    fetchXml += '<attribute name="name" />'
                    fetchXml += '<attribute name="opportunityid" />'
                    fetchXml += '<attribute name="fedcap_rfiduedate" />'
                    fetchXml += '<attribute name="fedcap_draftrfpdate" />'
                    fetchXml += '<attribute name="fedcap_proposalduedate" />'
                    fetchXml += '<attribute name="fedcap_ensduedate" />'
                    fetchXml += '<attribute name="fedcap_awarddate" />'
                    fetchXml += '<attribute name="actualclosedate" />'
                    fetchXml += '<attribute name="fedcap_rfpdate" />'
                    fetchXml += '<attribute name="fedcap_primesub" />'
                    fetchXml += '<attribute name="fedcap_bul_business_unit_lead" />'
                    fetchXml += '<attribute name="fedcap_capturemanager" />'
                    fetchXml += '<attribute name="fedcap_solutioning" />'
                    fetchXml += '<attribute name="fedcap_proposal_manager" />'
                    fetchXml += '<filter type="and">'
                    fetchXml += '<condition attribute="opportunityid" operator="in">'
                    for (view1 = view; view1 < checkCount; view1++) {
                        fetchXml += '<value>' + result.value[view1]["opportunityid"] + '</value>';
                    }
                    fetchXml += '</condition>'
                    fetchXml += '</filter>'
                    fetchXml += '</entity>'
                    fetchXml += '</fetch>'
                    xml.push(fetchXml);
                }
            }
            else {
                view = 0;
                checkCount = reminder;
                fetchXml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">';
                fetchXml += '<entity name="opportunity">'
                fetchXml += '<attribute name="name" />'
                fetchXml += '<attribute name="opportunityid" />'
                fetchXml += '<attribute name="fedcap_rfiduedate" />'
                fetchXml += '<attribute name="fedcap_draftrfpdate" />'
                fetchXml += '<attribute name="fedcap_proposalduedate" />'
                fetchXml += '<attribute name="fedcap_ensduedate" />'
                fetchXml += '<attribute name="fedcap_awarddate" />'
                fetchXml += '<attribute name="actualclosedate" />'
                fetchXml += '<attribute name="fedcap_rfpdate" />'
                fetchXml += '<attribute name="fedcap_primesub" />'
                fetchXml += '<attribute name="fedcap_bul_business_unit_lead" />'
                fetchXml += '<attribute name="fedcap_capturemanager" />'
                fetchXml += '<attribute name="fedcap_solutioning" />'
                fetchXml += '<attribute name="fedcap_proposal_manager" />'
                fetchXml += '<filter type="and">'
                fetchXml += '<condition attribute="opportunityid" operator="in">'
                for (view = 0; view < checkCount; view++) {
                    fetchXml += '<value>' + result.value[view]["opportunityid"] + '</value>';
                }
                fetchXml += '</condition>'
                fetchXml += '</filter>'
                fetchXml += '</entity>'
                fetchXml += '</fetch>'
                xml.push(fetchXml);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(textStatus + " View " + errorThrown);
        }
    });
    return xml;
}
function GetFuction(viewId) {
    debugger;
    //try {
    //    Xrm.WebApi.retrieveMultipleRecords("opportunity", "?savedQuery=" + viewId)
    //       .then(function (response) {
    //           debugger;
    //           var data = response;
    //       })
    //       .fail(function (error) {
    //           debugger;
    //           var data = error;
    //       });
    //} catch (e) {
    //}

    var check = false;
    var URL = Xrm.Page.context.getClientUrl() + "/api/data/v9.0/opportunities?savedQuery=" + viewId;
    var req = new XMLHttpRequest();
    req.open("POST", Xrm.Page.context.getClientUrl() + "/api/data/v9.0/opportunities?savedQuery=" + viewId, true);
    req.setRequestHeader("OData-MaxVersion", "4.0");
    req.setRequestHeader("OData-Version", "4.0");
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json;charset-utf-8");
    req.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
    req.onreadystatechange = function () {
        if (this.readyState == 4 /* complete */) {
            req.onreadystatechange = null;
            debugger;
            if (this.status == 200) {
                //success callback this returns null since no return value available.
                debugger;
                var result = JSON.parse(this.response);
                check = true;

            } else {
                //error callback
                debugger;
                var error = JSON.parse(this.response).error;
            }
        }
    };
    req.send();
}
function CheckSystemAndPersonalView(viewId) {
    debugger;
    var check = false;
    debugger;
    try {
        Xrm.WebApi.retrieveMultipleRecords("opportunity", "?savedQuery=" + viewId)
           .then(function (response) {
               debugger;
               check = true;
               var xmlArray = GetSelectedViewRecords(viewId, check);
               debugger;
           })
           .fail(function (error) {
               debugger;
               var data = error;
           });
    } catch (e) {
    }
    return check;
}
function CheckSystemAndPersonalView_Classic(viewId) {
    debugger;
    var check = false;
    try {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities?savedQuery=" + viewId,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: false,
            success: function (data, textStatus, xhr) {
                check = true;
            },
            error: function (xhr, textStatus, errorThrown) {
                check = false;
            }
        });
    } catch (e) {

    }
    return check;
}
function DownloadWaterFallAll() {
    debugger;
    var viewId = GetViewID();
    var check = CheckSystemAndPersonalView(viewId);
    // var xmlArray = GetSelectedViewRecords(viewId, check);
    debugger;
    var test = "shri";
}
function DownloadWaterFallAll_Classic() {
    var viewId = GetViewID();
    var check = CheckSystemAndPersonalView(viewId);
    var xmlArray = GetSelectedViewRecords(viewId, check);
    if (xmlArray.length == 0) {
        alert("Records not found.");
        return;
    }
    else {
        //var encodedFetchXml = encodeURI(fetchXml);
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = month + "." + day + "." + year + "_" + hour + "." + mins;
        //creating a temporary HTML link element (they support setting file names)
        //getting data from our div that contains the HTML table
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('dvData');
        //var table_html = table_div.outerHTML.replace(/ /g, '%20');
        var today = new Date();
        var table = '<div id="dvtest">';
        table += '<table border="1" style="font-family:Segoe UI"><thead>';
        table += '<tr><th colspan="17" style="font-size:small; text-align:left">GovCon Suite Gantt Chart as of "' + FormatDate(today) + '" - Based on GovCon Suite "' + today.getFullYear().toString() + '" Pipeline</th></tr>';
        table += '<tr style="font-size:x-small;bold:true"><td colspan="2"><b>Legends</b></td></tr>';
        table += '<tr style="font-size:x-small"><td>RFI Due Date to Draft RFP Date</td><td style="background-color:#d9d9d9"></td></tr>';
        table += '<tr style="font-size:x-small"><td>Draft RFP Date to Proposal Due Date</td><td style="background-color:#ffff00"></td></tr>';
        table += '<tr style="font-size:x-small"><td>Proposal Due Date to ENs Due Date</td><td style="background-color:#92d050"></td></tr>';
        table += '<tr style="font-size:x-small"><td>ENs Due Date to Award Date</td><td style="background-color:#8eb4e3"></td></tr>';
        table += '<tr style="font-size:x-small"><td>Award Date to Actual win/lost date</td><td style="background-color:#376092"></td></tr>';
        table += '<tr style="font-size:xx-small"><td colspan="7">Monthly Pursuit Totals --></td></tr>';
        table += '<tr style="width:100px; font-size:xx-small"><th style="width:500px;"> Opportunity Name</th><th> RFP Date</th><th> Prime/ Sub</th><th> BD Rep</th><th> Cap Mgr</th><th> SA</th><th> Prop Mgr</th>';
        var dateResult = getDaysBetweenDates(new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7)
            , new Date(today.getFullYear() + 1, today.getMonth(), today.getDate() - 1), 'Sun');
        for (var i = 0; i <= dateResult.length - 1; i++) {
            var dateForCol = dateResult[i];
            if (dateForCol == undefined) {
            }
            else {
                var dayCol = dateForCol.getDate();
                var monthCol = dateForCol.getMonth() + 1;
                var yearCol = dateForCol.getFullYear();
                var concatDate = monthCol + "/" + dayCol + "/" + yearCol;
                table += '<th style="font-size:xx-small; background:#A6A6A6"> ' + concatDate + '</th>';
            }
        }
        table += "</tr>";
        table += "<tbody>";

        for (var xml = 0; xml < xmlArray.length; xml++) {
            GetOppList();
            function GetOppList() {
                var encodedFetchXml = encodeURI(xmlArray[xml]);
                var URL = Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities?fetchXml=" + encodedFetchXml;
                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    //url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities?$top=20",
                    url: Xrm.Page.context.getClientUrl() + "/api/data/v8.0/opportunities?fetchXml=" + encodedFetchXml,
                    beforeSend: function (XMLHttpRequest) {
                        XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                        XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                        XMLHttpRequest.setRequestHeader("Accept", "application/json");
                        XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
                    },
                    async: false,
                    success: function (data, textStatus, xhr) {
                        var result = data;
                        //getting values of current time for generating the file name
                        for (var j = 0; j < result.value.length; j++) {
                            try {
                                var fedcap_rfpdate = result.value[j]["fedcap_rfpdate"];
                                var fedcap_rfiduedate = result.value[j]["fedcap_rfiduedate"];
                                var fedcap_draftrfpdate = result.value[j]["fedcap_draftrfpdate"];
                                var fedcap_proposalduedate = result.value[j]["fedcap_proposalduedate"];
                                var fedcap_ensduedate = result.value[j]["fedcap_ensduedate"];
                                var fedcap_awarddate = result.value[j]["fedcap_awarddate"];
                                var actualclosedate = result.value[j]["actualclosedate"];
                                var role = result.value[j]["fedcap_primesub@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_primesub = (result.value[j]["fedcap_primesub@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_primesub@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_bul_business_unit_lead = (result.value[j]["fedcap_bul_business_unit_lead"] == undefined) ? "" : result.value[j]["fedcap_bul_business_unit_lead"];
                                var fedcap_capturemanager = (result.value[j]["fedcap_capturemanager"] == undefined) ? "" : result.value[j]["fedcap_capturemanager"];
                                var fedcap_solutioning = (result.value[j]["fedcap_solutioning"] == undefined) ? "" : result.value[j]["fedcap_solutioning"];
                                var fedcap_proposal_manager = (result.value[j]["fedcap_proposal_manager"] == undefined) ? "" : result.value[j]["fedcap_proposal_manager"];
                                table += "<tr>";
                                table += '<td style="font-size:xx-small;">' + result.value[j]["name"] + '</td>';
                                if (result.value[j]["fedcap_rfiduedate"] == null) {
                                    table += '<td style="font-size:xx-small;"></td>';
                                }
                                else {
                                    //var formatedDate = FormatDate(new Date(result.value[j]["fedcap_rfpdate"]));
                                    var formatedDate = result.value[j]["fedcap_rfpdate@OData.Community.Display.V1.FormattedValue"];
                                    if (formatedDate === undefined) {
                                        formatedDate = "";
                                    }
                                    table += '<td style="font-size:xx-small;">' + formatedDate + '</td>';
                                }
                                table += "<td style='font-size:xx-small;'>" + fedcap_primesub + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_bul_business_unit_lead + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_capturemanager + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_solutioning + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_proposal_manager + "</td>";

                                for (var i = 0; i <= dateResult.length - 1; i++) {
                                    var dateForRow = dateResult[i];
                                    var fedcap_rfiduedateChanged = false, fedcap_draftrfpdateChanged = false, fedcap_proposalduedateChanged = false, fedcap_ensduedateChanged = false, fedcap_awarddateChanged = false, fedcap_rfpdateChanged = false, actualclosedateChanged = false;
                                   
                                    if (dateForRow == undefined) {
                                    }
                                    else {
                                        var todayDate = new Date();
                                        var tdInsert = true;
                                        if (result.value[j]["fedcap_rfiduedate"] === undefined && result.value[j]["fedcap_draftrfpdate"] === undefined && result.value[j]["fedcap_proposalduedate"] === undefined
                                            && result.value[j]["fedcap_ensduedate"] === undefined && result.value[j]["fedcap_awarddate"] === undefined && result.value[j]["actualclosedate"] === undefined)
                                        {
                                            tdInsert = true;
                                        }
                                        else {
                                            tdInsert = false;
                                        }
                                        if (result.value[j]["fedcap_rfiduedate"] === undefined) {
                                            //fedcap_rfiduedate = FormatDate(todayDate - 1);
                                        }
                                        else {
                                            if (result.value[j]["fedcap_draftrfpdate"] === undefined) {
                                                tdInsert = false;
                                                if (result.value[j]["fedcap_rfiduedate"] === undefined) {
                                                    fedcap_rfiduedate = dateResult[i];
                                                    fedcap_rfiduedateChanged = true;
                                                }
                                                fedcap_draftrfpdate = dateResult[i];
                                                fedcap_draftrfpdateChanged = true;
                                            }
                                            if (result.value[j]["fedcap_proposalduedate"] === undefined) {
                                                tdInsert = false;
                                                fedcap_proposalduedate = dateResult[i];
                                                fedcap_proposalduedateChanged = true;
                                            }
                                            if (result.value[j]["fedcap_ensduedate"] === undefined) {
                                                tdInsert = false;
                                                fedcap_ensduedate = dateResult[i];
                                                fedcap_ensduedateChanged = true;
                                            }
                                            if (result.value[j]["fedcap_awarddate"] === undefined) {
                                                tdInsert = false;
                                                fedcap_awarddate = dateResult[i];
                                                fedcap_awarddateChanged = true;
                                            }
                                            if (result.value[j]["actualclosedate"] === undefined) {
                                                tdInsert = false;
                                                actualclosedate = dateResult[i];
                                                actualclosedateChanged = true;
                                            }
                                        }

                                        //if (result.value[j]["fedcap_draftrfpdate"] === undefined) {
                                        //    tdInsert = false;
                                        //    fedcap_rfiduedate = dateResult[i];
                                        //    fedcap_draftrfpdate = dateResult[i];
                                        //    fedcap_rfiduedateChanged = fedcap_draftrfpdateChanged = true;
                                        //    if (result.value[j]["fedcap_proposalduedate"] === undefined) {
                                        //        fedcap_proposalduedate = dateResult[i];
                                        //        fedcap_proposalduedateChanged = true;
                                        //        if (result.value[j]["fedcap_rfpdate"] === undefined) {
                                        //            fedcap_rfpdate = dateResult[i];
                                        //            fedcap_rfpdateChanged = true;
                                        //            if (result.value[j]["fedcap_ensduedate"] === undefined) {
                                        //                fedcap_ensduedate = dateResult[i];
                                        //                fedcap_ensduedateChanged = true;
                                        //                if (result.value[j]["fedcap_awarddate"] === undefined) {
                                        //                    fedcap_awarddate = dateResult[i];
                                        //                    fedcap_awarddateChanged = true;
                                        //                }
                                        //                else {
                                        //                    //fedcap_awarddate else part
                                        //                }
                                        //            }
                                        //            else {
                                        //                //fedcap_ensduedate else part
                                        //                if (result.value[j]["fedcap_awarddate"] === undefined) {
                                        //                    fedcap_awarddate = dateResult[i];
                                        //                    fedcap_awarddateChanged = true;
                                        //                }
                                        //            }
                                        //        }
                                        //        else {
                                        //            //fedcap_rfpdate else part
                                        //            if (result.value[j]["fedcap_ensduedate"] === undefined) {
                                        //                fedcap_ensduedate = dateResult[i];
                                        //                fedcap_ensduedateChanged = true;
                                        //            }
                                        //            if (result.value[j]["fedcap_awarddate"] === undefined) {
                                        //                fedcap_awarddate = dateResult[i];
                                        //                fedcap_awarddateChanged = true;
                                        //            }
                                        //        }
                                        //    }
                                        //    else {
                                        //        //fedcap_proposalduedate else part
                                        //        if (result.value[j]["fedcap_rfpdate"] === undefined) {
                                        //            fedcap_rfpdate = dateResult[i];
                                        //            fedcap_rfpdateChanged = true;
                                        //        }
                                        //        if (result.value[j]["fedcap_ensduedate"] === undefined) {
                                        //            fedcap_ensduedate = dateResult[i];
                                        //            fedcap_ensduedateChanged = true;
                                        //        }
                                        //        if (result.value[j]["fedcap_awarddate"] === undefined) {
                                        //            fedcap_awarddate = dateResult[i];
                                        //            fedcap_awarddateChanged = true;
                                        //        }
                                        //    }
                                        //}
                                        //else {
                                        //    //fedcap_draftrfpdate else part
                                        //    if (result.value[j]["fedcap_proposalduedate"] === undefined) {
                                        //        fedcap_proposalduedate = dateResult[i];
                                        //        fedcap_proposalduedateChanged = true;
                                        //    }
                                        //    if (result.value[j]["fedcap_rfpdate"] === undefined) {
                                        //        fedcap_rfpdate = dateResult[i];
                                        //        fedcap_rfpdateChanged = true;
                                        //    }
                                        //    if (result.value[j]["fedcap_ensduedate"] === undefined) {
                                        //        fedcap_ensduedate = dateResult[i];
                                        //        fedcap_ensduedateChanged = true;
                                        //    }
                                        //    if (result.value[j]["fedcap_awarddate"] === undefined) {
                                        //        fedcap_awarddate = dateResult[i];
                                        //        fedcap_awarddateChanged = true;
                                        //    }
                                        //}


                                        if (tdInsert) {
                                                table += '<td></td>';
                                        }
                                        else {
                                            var fedcap_rfiduedateDate;
                                            var fedcap_draftrfpdateDate;
                                            var fedcap_proposalduedateDate;
                                            var fedcap_ensduedateDate;
                                            var fedcap_awarddateDate;
                                            var actualclosedateDate;
                                            var fedcap_rfpdate_MidDate ;
                                            if (!fedcap_rfiduedateChanged) {
                                                fedcap_rfiduedateDate = FormatDateUTC(new Date(fedcap_rfiduedate));
                                            }
                                            else {
                                                fedcap_rfiduedateDate = fedcap_rfiduedate;
                                            }
                                            if (!fedcap_draftrfpdateChanged) {
                                                fedcap_draftrfpdateDate = FormatDateUTC(new Date(fedcap_draftrfpdate));
                                            }
                                            else {
                                                fedcap_draftrfpdateDate = fedcap_draftrfpdate;
                                            }
                                            if (!fedcap_proposalduedateChanged) {
                                                fedcap_proposalduedateDate = FormatDateUTC(new Date(fedcap_proposalduedate));
                                            }
                                            else {
                                                fedcap_proposalduedateDate = fedcap_proposalduedate;
                                            }
                                            if (!fedcap_ensduedateChanged) {
                                                fedcap_ensduedateDate = FormatDateUTC(new Date(fedcap_ensduedate));
                                            }
                                            else {
                                                fedcap_ensduedateDate = fedcap_ensduedate;
                                            }
                                            if (!fedcap_awarddateChanged) {
                                                fedcap_awarddateDate = FormatDateUTC(new Date(fedcap_awarddate));
                                            }
                                            else {
                                                fedcap_awarddateDate = fedcap_awarddate;
                                            }
                                            if (!actualclosedateChanged) {
                                                actualclosedateDate = FormatDateUTC(new Date(actualclosedate));
                                            }
                                            else {
                                                actualclosedateDate = actualclosedate;
                                            }

                                            //var fedcap_rfiduedateDate = FormatDateUTC(new Date(fedcap_rfiduedate));
                                            //var fedcap_draftrfpdateDate = FormatDateUTC(new Date(fedcap_draftrfpdate));
                                            //var fedcap_proposalduedateDate = FormatDateUTC(new Date(fedcap_proposalduedate));
                                            //var fedcap_ensduedateDate = FormatDateUTC(new Date(fedcap_ensduedate));
                                            //var fedcap_awarddateDate = FormatDateUTC(new Date(fedcap_awarddate));
                                            //var fedcap_rfpdate_MidDate = FormatDateUTC(new Date(fedcap_rfpdate));

                                            //1
                                            if (dateForRow >= fedcap_rfiduedateDate && dateForRow <= fedcap_draftrfpdateDate && dateForRow <= fedcap_proposalduedateDate && //dateForRow <= fedcap_rfpdate_MidDate &&
                                                dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                if (fedcap_rfiduedateDate <= fedcap_draftrfpdateDate) {
                                                    table += '<td style="background-color:#d9d9d9"></td>';
                                                }
                                            }
                                                //2
                                            else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && dateForRow <= fedcap_proposalduedateDate && //dateForRow <= fedcap_rfpdate_MidDate &&
                                                dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                if (fedcap_draftrfpdateDate <= fedcap_proposalduedateDate) {
                                                    table += '<td style="background-color:#ffff00"></td>';
                                                }
                                            }
                                                //3
                                            else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && //dateForRow >= fedcap_rfpdate_MidDate &&
                                                dateForRow >= fedcap_proposalduedateDate && dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                if (fedcap_proposalduedateDate <= fedcap_ensduedateDate) {
                                                    table += '<td style="background-color:#92d050"></td>';
                                                }
                                            }
                                                //4
                                            else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && //dateForRow >= fedcap_rfpdate_MidDate &&
                                                     dateForRow >= fedcap_proposalduedateDate && dateForRow >= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                if (fedcap_ensduedateDate <= fedcap_awarddateDate) {
                                                    table += '<td style="background-color:#8eb4e3"></td>';
                                                }
                                            }
                                                //5
                                            else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && //dateForRow >= fedcap_rfpdate_MidDate &&
                                                     dateForRow >= fedcap_proposalduedateDate && dateForRow >= fedcap_ensduedateDate && dateForRow >= fedcap_awarddateDate && dateForRow <= actualclosedateDate) {
                                                if (fedcap_awarddateDate <= actualclosedateDate) {
                                                    table += '<td style="background-color:#376092"></td>';
                                                }
                                            }
                                            else {
                                                table += '<td></td>';
                                            }
                                        }


                                        //if (result.value[j]["fedcap_rfiduedate"] == null) {
                                        //    table += '<td></td>';
                                        //}
                                        //else {
                                        //    var fedcap_rfiduedateDate = FormatDateUTC(new Date(fedcap_rfiduedate));
                                        //    var fedcap_draftrfpdateDate = FormatDateUTC(new Date(fedcap_draftrfpdate));
                                        //    var fedcap_proposalduedateDate = FormatDateUTC(new Date(fedcap_proposalduedate));
                                        //    var fedcap_ensduedateDate = FormatDateUTC(new Date(fedcap_ensduedate));
                                        //    var fedcap_awarddateDate = FormatDateUTC(new Date(fedcap_awarddate));
                                        //    var fedcap_rfpdate_MidDate = FormatDateUTC(new Date(fedcap_rfpdate));
                                        //    //1
                                        //    if (dateForRow >= fedcap_rfiduedateDate && dateForRow <= fedcap_draftrfpdateDate && dateForRow <= fedcap_rfpdate_MidDate &&
                                        //        dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate) {
                                        //        if (fedcap_rfiduedateDate <= fedcap_draftrfpdateDate) {
                                        //            table += '<td style="background-color:#d9d9d9"></td>';
                                        //        }
                                        //    }
                                        //        //2
                                        //    else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && dateForRow <= fedcap_rfpdate_MidDate &&
                                        //        dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate) {
                                        //        if (fedcap_draftrfpdateDate <= fedcap_rfpdate_MidDate) {
                                        //            table += '<td style="background-color:#ffff00"></td>';
                                        //        }
                                        //    }
                                        //        //3
                                        //    else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && dateForRow >= fedcap_rfpdate_MidDate &&
                                        //        dateForRow <= fedcap_proposalduedateDate && dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate) {
                                        //        if (fedcap_rfpdate_MidDate <= fedcap_proposalduedateDate) {
                                        //            table += '<td style="background-color:#92d050"></td>';
                                        //        }
                                        //    }
                                        //        //4
                                        //    else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && dateForRow >= fedcap_rfpdate_MidDate &&
                                        //             dateForRow >= fedcap_proposalduedateDate && dateForRow <= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate) {
                                        //        if (fedcap_proposalduedateDate <= fedcap_ensduedateDate) {
                                        //            table += '<td style="background-color:#8eb4e3"></td>';
                                        //        }
                                        //    }
                                        //        //5
                                        //    else if (dateForRow >= fedcap_rfiduedateDate && dateForRow >= fedcap_draftrfpdateDate && dateForRow >= fedcap_rfpdate_MidDate &&
                                        //             dateForRow >= fedcap_proposalduedateDate && dateForRow >= fedcap_ensduedateDate && dateForRow <= fedcap_awarddateDate) {
                                        //        if (fedcap_ensduedateDate <= fedcap_awarddateDate) {
                                        //            table += '<td style="background-color:#376092"></td>';
                                        //        }
                                        //    }
                                        //    else {
                                        //        table += '<td></td>';
                                        //    }
                                        //}
                                    }
                                }
                                table += "</tr>";

                            } catch (e) {

                            }
                            
                        }

                    },
                    error: function (xhr, textStatus, errorThrown) {
                    }
                });
            }

        }


        table += "</tbody>";
        table += "</table>";
        table += "</div>";
        //Xrm.Page.ui.clearFormNotification();
        //var table_html = table_div.outerHTML.replace(/ /g, '%20');
        var table_html = table.replace(/ /g, '%20');
        if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) //IF IE > 10
        {
            var table1 = table;
            table1 = table1.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            table1 = table1.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            table1 = table1.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            if (document.execCommand) {
                try {
                    var oWin = window.open("about:blank", "_blank");
                    //var oWin = window.open("", "test", "resizable=no,scrollbars=yes,width=260,height=225");
                    oWin.document.write(table1);
                    oWin.document.close();
                    var success = oWin.document.execCommand('SaveAs', false, 'GanttChart_' + postfix + '.xls')
                    oWin.close();
                }
                catch (err) {
                    alert(err.message());
                }
            }
            else {
                alert('Error while downloading!!!');
            }
        }
        else if ((navigator.userAgent.indexOf("Edge") != -1)) //IF IE > 10
        {
            debugger;
            var table1 = table;
            table1 = table1.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            table1 = table1.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            table1 = table1.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            var blob = new Blob([table1], { type: 'data:application/vnd.ms-excel' });
            window.navigator.msSaveBlob(blob, 'GanttChart_' + postfix + '.xls');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Opportunities</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>' + table + '</body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            var ctx = { worksheet: "test" || 'Worksheet', "dvtest": table }
            //window.location.href = uri + base64(format(template, ctx))
            let link = document.createElement('a');
            link.id = "AccountOpportunity";
            link.style.display = "none"; // because Firefox sux
            document.body.appendChild(link); // because Firefox sux
            link.href = uri + base64(format(template, ctx))
            link.download = 'GanttChart_' + postfix + '.xls';
            link.click();
            document.body.removeChild(link); // because Firefox sux
        }
        else {
            //var a = document.createElement('a');
            //a.href = data_type + ', ' + table_html;
            ////setting the file name
            //a.download = 'GanttChart_' + postfix + '.xls';
            ////triggering the function
            //document.body.appendChild(a);
            //a.click();
            ////just in case, prevent default behaviour
            //e.preventDefault();

            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Opportunities</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>' + table + '</body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            var ctx = { worksheet: "test" || 'Worksheet', "dvtest": table }
            //window.location.href = uri + base64(format(template, ctx))
            let link = document.createElement('a');
            link.id = "AccountOpportunity";
            link.style.display = "none"; // because Firefox sux
            document.body.appendChild(link); // because Firefox sux
            link.href = uri + base64(format(template, ctx))
            link.download = 'GanttChart_' + postfix + '.xls';
            link.click();
            document.body.removeChild(link); // because Firefox sux
        }
    }

}

function FormatDate(passedDate) {
    var day = '';
    var month = '';
    var year = '';
    year = passedDate.getFullYear().toString();
    month = (passedDate.getMonth() + 1).toString();
    day = passedDate.getDate().toString();
    if (month.length == 1) {
        month = "0" + (passedDate.getMonth() + 1).toString();
    }
    if (day.length == 1) {
        day = "0" + passedDate.getDate().toString();
    }
    var convertedDate = year + "-" + month + "-" + day;
    return convertedDate;
}
function FormatDateUTC(passedDate) {
    var day = '';
    var month = '';
    var year = '';
    year = passedDate.getFullYear().toString();
    month = (passedDate.getMonth() + 1).toString();
    day = passedDate.getDate().toString();
    if (month.length == 1) {
        month = "0" + (passedDate.getMonth() + 1).toString();
    }
    if (day.length == 1) {
        day = "0" + passedDate.getDate().toString();
    }
    var convertedDate = new Date(passedDate.getUTCFullYear(), passedDate.getUTCMonth(), passedDate.getUTCDate());// year + "-" + month + "-" + day;
    return convertedDate;
}



﻿(function () {
    "use strict";
    var app = angular.module('MyApp', []);
    app.controller('PGoController', PGoController);
    //app.controller('homeController', [              
    //    '$scope',                              
    //    function homeController($scope) {        
    //        $scope.message = 'HOME PAGE';                  
    //    }                                                
    //]);   
    function PGoController($scope) {
        $scope.pgoquestions = [];
        GetQuestion();
        function GetQuestion() {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_pgoquestions?$orderby=fedcap_sequence asc", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            debugger;
                            var data = JSON.parse(this.response);
                            $scope.pgoquestions = data.value;
                            for (var i = 0; i < data.value.length; i++) {
                                debugger;
                                GetAnswer(i, data.value[i]["fedcap_pgoquestionid"]);
                            }
                        }
                        else {
                            var error = JSON.parse(this.response).error;
                            Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
        function GetAnswer(QueIndex, QuestionId) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_pgoanswers?$filter=_fedcap_pgo_question_value eq " + QuestionId + "", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            debugger;
                            var data = JSON.parse(this.response);
                            $scope.pgoquestions[QueIndex].answers = data.value;
                        }
                        else {
                            var error = JSON.parse(this.response).error;
                            Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
    }
})();
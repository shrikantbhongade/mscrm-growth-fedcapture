﻿var Xrm = window.parent.Xrm;
function GetMultipicklistValue(fieldName) {
    debugger;
    var selectedValues = null;
    var selectedValuesString = '';
    try {
        if (Xrm.Page.getAttribute(fieldName) !== null) {
            selectedValues = Xrm.Page.getAttribute(fieldName).getValue();
            for (var i = 0; i < selectedValues.length; i++) {
                if (i === 0) {
                    selectedValuesString = selectedValues[i].toString();
                }
                else {
                    selectedValuesString = selectedValuesString + "," + selectedValues[i].toString();
                }
            }
        }
    } catch (e) {

    }
    return selectedValuesString;
}
function CopyOpportunityWorkflow() {

    var oppguid = Xrm.Page.data.entity.getId();
    var OID = oppguid.substring(1, 37);
    var functionName = "executeWorkflow >>";
    var dt = new Date();
    var OppName = "Copy of " + Xrm.Page.getAttribute("name").getValue();
    var query = "";
    var OpportunityId = OID;
    var workflowId = "7865b78b-8c74-4de9-9ffa-53187c91b38f";
    var clientUrl = Xrm.Page.context.getClientUrl();
    var CopyOppURL = Xrm.Page.context.getClientUrl() + "/api/data/v9.0/opportunities?$select=name,opportunityid&$orderby=createdon desc&$filter=contains(name,'" + OppName + "')"
    debugger;
    Xrm.Page.ui.setFormNotification("Please wait while Clone Opportunity is creating..!!!", "WARNING", "1")
    try {
        //Define the query to execute the action
        query = "workflows(" + workflowId.replace("}", "").replace("{", "") + ")/Microsoft.Dynamics.CRM.ExecuteWorkflow";
        var data = {
            "EntityId": OpportunityId
        };
        //Create request
        // request url
        //https://org.crm.dynamics.com/api/data/v8.2/workflows(“f0ca33cc-23fd-496f-80e1-693873a951ca”)/Microsoft.Dynamics.CRM.ExecuteWorkflow
        var req = new XMLHttpRequest();
        req.open("POST", clientUrl + "/api/data/v9.0/" + query, true);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.onreadystatechange = function () {
            if (this.readyState == 4 /* complete */) {
                req.onreadystatechange = null;
                if (this.status == 200) {
                    //success callback this returns null since no return value available.
                    debugger;
                    var result = JSON.parse(this.response);
                    setTimeout(function () {
                        try {
                            $.ajax({
                                type: "GET",
                                contentType: "application/json; charset=utf-8",
                                datatype: "json",
                                url: CopyOppURL,
                                beforeSend: function (XMLHttpRequest) {
                                    XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                                    XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                                    XMLHttpRequest.setRequestHeader("Accept", "application/json");
                                    XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
                                },
                                async: false,
                                success: function (data, textStatus, xhr) {
                                    debugger;
                                    var fedcap_typeofawardString = GetMultipicklistValue("fedcap_typeofaward");
                                    var fedcap_contracttypeString = GetMultipicklistValue("fedcap_contracttype");

                                    var result = data.value;
                                    if (result.length === 0) {
                                        Xrm.Page.ui.clearFormNotification("1");
                                        return;
                                    }
                                    var recordId = result[0]["opportunityid"];
                                    if (fedcap_typeofawardString === "" && fedcap_contracttypeString === "") {
                                        Xrm.Page.ui.clearFormNotification("1");
                                        var dialogLabelAndText = { confirmButtonLabel: "Open Opportunity", text: "Clone Opportunity created successfully.", title: "Confirmation" };
                                        var dialogOptions = { height: 200, width: 450 };
                                        Xrm.Navigation.openConfirmDialog(dialogLabelAndText, dialogOptions).then(
                                          function (success) {
                                              if (success.confirmed) {
                                                  var windowOptions = {
                                                      openInNewWindow: false
                                                  };
                                                  if (Xrm.Utility != null) {
                                                      Xrm.Utility.openEntityForm("opportunity", recordId, null, windowOptions);
                                                  }
                                              }
                                              else {
                                                  return;
                                              }
                                          });
                                    }
                                    var objArry = {};
                                    if (fedcap_typeofawardString !== '') {
                                        objArry.fedcap_typeofaward = fedcap_typeofawardString;
                                    }
                                    if (fedcap_contracttypeString !== '') {
                                        objArry.fedcap_contracttype = fedcap_contracttypeString;
                                    }
                                    Xrm.WebApi.updateRecord("opportunity", recordId, objArry).then(
                                    function success(resultClone) {
                                        debugger;
                                        Xrm.Page.ui.clearFormNotification("1");
                                        var dialogLabelAndText = { confirmButtonLabel: "Open Opportunity", text: "Clone Opportunity created successfully.", title: "Confirmation" };
                                        var dialogOptions = { height: 200, width: 450 };
                                        Xrm.Navigation.openConfirmDialog(dialogLabelAndText, dialogOptions).then(
                                          function (success) {
                                              if (success.confirmed) {
                                                  var windowOptions = {
                                                      openInNewWindow: false
                                                  };
                                                  if (Xrm.Utility != null) {
                                                      Xrm.Utility.openEntityForm("opportunity", recordId, null, windowOptions);
                                                  }
                                              }
                                              else {
                                                  //var windowOptions = {
                                                  //    openInNewWindow: false
                                                  //};
                                                  //if (Xrm.Utility != null) {
                                                  //    Xrm.Utility.openEntityForm("opportunity", recordId, null, windowOptions);
                                                  //}
                                              }
                                          });
                                    },
                                    function (error) {
                                        debugger;
                                        debugger;
                                        Xrm.Page.ui.clearFormNotification("1");
                                        var dialogLabelAndText = { confirmButtonLabel: "Open Opportunity", text: "Clone Opportunity created successfully.", title: "Confirmation" };
                                        var dialogOptions = { height: 200, width: 450 };
                                        Xrm.Navigation.openConfirmDialog(dialogLabelAndText, dialogOptions).then(
                                          function (success) {
                                              if (success.confirmed) {
                                                  var windowOptions = {
                                                      openInNewWindow: false
                                                  };
                                                  if (Xrm.Utility != null) {
                                                      Xrm.Utility.openEntityForm("opportunity", recordId, null, windowOptions);
                                                  }
                                              }
                                              else {
                                                  //var windowOptions = {
                                                  //    openInNewWindow: false
                                                  //};
                                                  //if (Xrm.Utility != null) {
                                                  //    Xrm.Utility.openEntityForm("opportunity", recordId, null, windowOptions);
                                                  //}
                                              }
                                          });
                                    }
                                );
                                },
                                error: function (xhr, textStatus, errorThrown) {
                                    Xrm.Page.ui.clearFormNotification("1");
                                }
                            });  //end of ajax function
                        } catch (e) {
                            Xrm.Page.ui.clearFormNotification("1");
                        }
                    },
                    20000);
                } else {
                    //error callback
                    var error = JSON.parse(this.response).error;
                    Xrm.Page.ui.clearFormNotification("1");
                }
            }
        };
        req.send(JSON.stringify(data));
    } catch (e) {
        console.log(e.message.toString());
        Xrm.Page.ui.clearFormNotification("1");
    }
}
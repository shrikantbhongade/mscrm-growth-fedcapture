﻿"use strict";
function setFBODetailsDate() {
    var Xrm = parent.Xrm;
    var currentDateTime = new Date();
    Xrm.Page.getAttribute("fbo_get_fbo_date").setValue(currentDateTime);
    Xrm.Page.getAttribute("fbo_get_fbo_date").setSubmitMode("always");
    Xrm.Page.data.entity.save();
}
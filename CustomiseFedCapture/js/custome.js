var Xrm = parent.Xrm;
function closeModel(modelId) {
    $("#" + modelId).modal('hide');
}
function openModel(modelId) {
    submittedAlready = false;
    $("#" + modelId).modal('show');
}
function openTabModel() {
    submittedAlready = false;
    readyColorPickar();
    //$('#tab_isInsert').val('');
    $('#tab_Modal_headder').html('Add Tab');
    $('#tab_Modal_button').html('Add');
    openModel('tab_Modal');
}

var submittedAlready = false;

function saveTab() {
    if (submittedAlready === true) {
        return false;
    }
    submittedAlready = true;
    var tabLabel = $('#tab_Label_popup').val().trim();
    var bgColor = $('#tab_BG_color_popup').val().trim();
    var congaAPI = '';// $('#tab_congaAPI_popup').val().trim();
    var status = '';//$('#tab_status_popup').val().trim();
    var tabNumber = $('#tab_isInsert').val();
    var isShow = $('#tab_Hide').prop("checked");
    var tabId = $('#tab_isUpdate').val();
    var Height = $('#tab_height').val().trim();
    if (Height == '') {
        Height = "450";
    }
    var HeightConverted = parseInt(Height);
    //&& bgColor !== ''
    if (tabLabel !== '' && HeightConverted >= 250) {
        addTab(tabLabel, bgColor, congaAPI, status, tabNumber, tabId, Height, isShow);
    } else {
        submittedAlready = false;
        //if(bgColor===''){
        //	alert('Select color and press Choose Button');
        //}                
        if (tabLabel === '') {
            $('#tab_Label_popup').css({ "border": "1px solid #DC1616" });
        }
        if (HeightConverted < 250) {
            alert('Tab Height should not be less than 250px.');
        }
    }
}
function closeTabModel() {
    closeModel("tab_Modal");
    //1px solid #ccc;
    $('#tab_Label_popup').css({ "border": "1px solid #ccc" });
    $('#tab_BG_color_popup').css({ "border": "1px solid #ccc" });
    $('#tab_BG_color_popup').val('');
    $('#tab_Label_popup').val('');
    $('#tab_height').val('');
    $('#tab_Hide').prop("checked", false);
    //$('#tab_congaAPI_popup').val('');
    //$('#tab_status_popup').val('');
    $('#tab_isUpdate').val('');
    //$('#tab_isInsert').val('');

}
function openSecModel(tabSeq) {
    submittedAlready = false;
    $('#sec_tab_seq_popup').val('');
    $('#sec_sec_seq_popup').val('');
    $('#sec_Hide').prop("checked", false);
    $('#sec_LabelHide').prop("checked", false);
    $('#sec_DisplayBorder').prop("checked", false);

    $('#section_Modal_headder').html('Add Section');
    $('#section_Modal_button').html('Add');
    if (tabSeq !== '') {
        $('#sec_tab_seq_popup').val(tabSeq);
        openModel('section_Modal');
    }
}
function saveSection() {
    if (submittedAlready === true) {
        return false;
    }
    submittedAlready = true;
    var secLabel = $('#sec_Label_popup').val().trim();
    var tabSeq = $('#sec_tab_seq_popup').val();
    var secorder = $('#sec_sec_seq_popup').val();
    var noofclm = $('#sec_column_popup').val();
    var isHide = $('#sec_Hide').prop("checked");
    var isLabelHide = $('#sec_LabelHide').prop("checked");
    var isDisplayBorder = $('#sec_DisplayBorder').prop("checked");
    var controlwidth = $('#sec_controlwidth_popup').val();
    var sectionId = $('#section_isUpdate').val();
    var noofclmInt = parseInt(noofclm);
    if (secLabel !== '' && tabSeq !== '' && noofclm !== '' && noofclmInt <= 5) {
        addSection(secLabel, noofclm, tabSeq, secorder, sectionId, controlwidth, isHide, isLabelHide, isDisplayBorder);
    } else {
        submittedAlready = false;
        if (secLabel === '') {
            $('#sec_Label_popup').css({ "border": "1px solid #DC1616" });
        }
        if (noofclm === '') {
            $('#sec_column_popup').css({ "border": "1px solid #DC1616" });
        }
        if (noofclmInt >= 5) {
            alert('Column should be less 6.');

        }
        if (tabSeq === '') {
            alert('Something went wrong please refresh and try again.');
        }
    }
}
function closeSecModel() {
    closeModel('section_Modal');
    $('#sec_Label_popup').css({ "border": "1px solid #ccc" });
    $('#sec_column_popup').css({ "border": "1px solid #ccc" });
    $('#sec_Label_popup').val('');
    $('#sec_column_popup').val('');
    $('#sec_Hide').prop("checked", false);
    $('#sec_LabelHide').prop("checked", false);
    $('#sec_DisplayBorder').prop("checked", false);
    $('#sec_tab_seq_popup').val('');
    $('#sec_controlwidth_popup').val('');
    $('#sec_sec_seq_popup').val('');
    $('#section_isUpdate').val('');
}
function openVfpModel(tabSeq) {
    submittedAlready = false;
    $('#vfp_tab_seq_popup').val('');
    $('#vfp_sec_seq_popup').val('');
    $('#vfp_Modal_headder').html('Add VF Page');
    $('#vfp_Modal_button').html('Add');
    if (tabSeq != '') {
        $('#vfp_tab_seq_popup').val(tabSeq);
        openModel('vf_page_Modal');
    }
}
function savevfp() {
    if (submittedAlready === true) {
        return false;
    }
    submittedAlready = true;
    var pgLabel = $('#vfp_Label_popup').val().trim();
    var pgPage = $('#vfp_InlineApiName').val().trim();
    var pgWidth = $('#vfp_width').val();
    var pgHeight = $('#vfp_height').val();
    var pgTabSeq = $('#vfp_tab_seq_popup').val();
    var pgorder = $('#vfp_sec_seq_popup').val();
    var isComponant = false;
    if ($('#vfp_Is_Comp').prop("checked")) {
        isComponant = true;
    }
    if (pgLabel !== '' && pgTabSeq !== '') {
        addVfp(pgLabel, pgPage, pgWidth, pgHeight, pgTabSeq, pgorder, isComponant);
    } else {
        submittedAlready = false;
        if (pgLabel === '') {
            $('#vfp_Label_popup').css({ "border": "1px solid #DC1616" });
        }
        if (pgPage === '') {
            $('#vfp_InlineApiName').css({ "border": "1px solid #DC1616" });
        }
        if (pgWidth === '') {
            $('#vfp_width').css({ "border": "1px solid #DC1616" });
        }
        if (pgHeight === '') {
            $('#vfp_height').css({ "border": "1px solid #DC1616" });
        }

        if (pgTabSeq === '') {
            alert('Something went wrong please refresh and try again.');
        }
    }
}
function GetHeader() {
    parent.document.getElementById("formHeaderContainer").style.display = "block";
}
function closevfpModel() {
    closeModel('vf_page_Modal');
    $('#vfp_Label_popup').css({ "border": "1px solid #ccc" });
    $('#vfp_InlineApiName').css({ "border": "1px solid #ccc" });
    $('#vfp_width').css({ "border": "1px solid #ccc" });
    $('#vfp_height').css({ "border": "1px solid #ccc" });
    $('#vfp_Label_popup').val('');
    $('#vfp_InlineApiName').val('');
    $('#vfp_width').val('');
    $('#vfp_height').val('');
    $('#vfp_tab_seq_popup').val('');
    $('#vfp_sec_seq_popup').val('');
    $('#vfp_Is_Comp').prop("checked", false);
}
function openFieldModel(tabSeq, secSeq, PopupName) {
    submittedAlready = false;
    if (PopupName === "Add Field") {
        $('#tblAddField').css({ "display": "" });
        $('#tblAddHeader').css({ "display": "none" });
    }
    else {
        $('#tblAddField').css({ "display": "none" });
        $('#tblAddHeader').css({ "display": "" });
    }
    $("input").attr("disabled", false);
    $("select").attr("disabled", false);
    $("textarea").attr("disabled", false);

    $('#field_Label_popup').css({ "border": "1px solid #ccc" });
    $('#field_apiName_popup').css({ "border": "1px solid #ccc" });
    $('#field_Label_popup').val('');
    $('#field_Label_popup').attr('placeholder', '');
    $('#field_apiName_popup').val('');
    $('#field_helpText_popup').val('');
    $('#field_height_popup').val('');
    $('#field_checkbox_spacer').prop("checked", false);
    $('#field_checkbox_popup').prop("checked", false);
    $('#field_checkbox_hidelabel').prop("checked", false);
    $('#field_checkboxreadonly_popup').prop("checked", false);
    $('#field_tab_seq_popup').val('');
    $('#field_sec_seq_popup').val('');
    $('#field_field_seq_popup').val('');
    $('#field_IsUpdate').val('');
    $('#field_ViewFieldApi_popup').val('');
    $('#field_custlookup_popup').prop("checked", false);

    $('#header_cell1').val('');
    $('#header_cell2').val('');
    $('#header_checkbox_hidelabel').prop("checked", false);

    //***************************************************
    //$('#field_Modal_headder').html('Add Field');
    $('#field_Modal_headder').html(PopupName);
    $('#field_Modal_button').html('Add');
    if (tabSeq !== '' && secSeq !== '') {
        $('#field_tab_seq_popup').val(tabSeq);
        $('#field_sec_seq_popup').val(secSeq);
        openModel('field_Modal');
    }
}
function saveField() {
    if (submittedAlready === true) {
        return false;
    }
    submittedAlready = true;
    var fieldLabel = $('#field_Label_popup').val().trim();
    var apiName = $('#field_apiName_popup').val().trim();
    var helpText = $('#field_helpText_popup').val().trim();
    var height = $('#field_height_popup').val().trim();
    var isRequired = $('#field_checkbox_popup').prop("checked");
    var isReadonly = $('#field_checkboxreadonly_popup').prop("checked");
    var isHideLabel = $('#field_checkbox_hidelabel').prop("checked");
    var tabSeq = $('#field_tab_seq_popup').val();
    var secSeq = $('#field_sec_seq_popup').val();
    var fieldSeq = $('#field_field_seq_popup').val();
    var ViewFieldApi = $('#field_ViewFieldApi_popup').val();
    var useCustomLookup = $('#field_custlookup_popup').prop("checked");
    var field_checkbox_spacer = $('#field_checkbox_spacer').prop("checked");
    var ViewFieldApi = $('#field_ViewFieldApi_popup').val();
    var field_guid = $('#field_IsUpdate').val();

    var popupName = $('#field_Modal_headder').html();
    debugger;
    if (popupName === "Add Header" || popupName === "Update Header") {
        fieldLabel = $('#header_cell1').val().trim() + '-header';
        apiName = $('#header_cell2').val().trim();
        isHideLabel = $('#header_checkbox_hidelabel').prop("checked");
    }

    if (fieldLabel !== '' && apiName !== '' && secSeq !== '') {
        addField(apiName, fieldLabel, helpText, isRequired, tabSeq, secSeq, fieldSeq, ViewFieldApi, useCustomLookup, field_guid, isReadonly, height, isHideLabel, field_checkbox_spacer);
    } else {
        submittedAlready = false;
        if (fieldLabel === '') {
            $('#field_Label_popup').css({ "border": "1px solid #DC1616" });
            $('#header_cell1').css({ "border": "1px solid #DC1616" });
        }
        if (apiName === '') {
            $('#field_apiName_popup').css({ "border": "1px solid #DC1616" });
            $('#header_cell2').css({ "border": "1px solid #DC1616" });
        }
        if (secSeq === '') {
            alert('Something went wrong please refresh and try again.');
        }
    }
}
function poplateViewApi(apiVal) {
    if ($('#field_ViewFieldApi_popup').val().trim() == '') {
        $('#field_ViewFieldApi_popup').val(apiVal);
    }
}
function closeFieldModel() {
    closeModel('field_Modal');
    $('#field_Label_popup').css({ "border": "1px solid #ccc" });
    $('#field_apiName_popup').css({ "border": "1px solid #ccc" });
    $('#field_Label_popup').val('');
    $('#field_apiName_popup').val('');
    $('#field_helpText_popup').val('');
    $('#field_height_popup').val('');
    $('#field_checkbox_popup').prop("checked", false);
    $('#field_checkboxreadonly_popup').prop("checked", false);
    $('#field_checkbox_hidelabel').prop("checked", false);
    $('#field_tab_seq_popup').val('');
    $('#field_sec_seq_popup').val('');
    $('#field_field_seq_popup').val('');
    $('#field_IsUpdate').val('');
    $('#field_ViewFieldApi_popup').val('');
    $('#field_custlookup_popup').prop("checked", false);
}
function openGlanceModel() {
    $('#glance_field_seq_popup').val('');
    $('#glance_field_Modal_headder').html('Add Field');
    $('#glance_field_Modal_button').html('Add');
    readyColorPickar1();
    glanceFieldChange();
    openModel('glance_field_Modal');
}
function glanceFieldChange() {
    if ($('#glance_Is_Comp_popup').prop("checked")) {
        $('#glance_Label_popup').closest('tr').hide();
        $('#glance_docid_popup').closest('tr').hide();
        $('#glance_color_popup').closest('tr').hide();
    } else {
        $('#glance_Label_popup').closest('tr').show();
        $('#glance_docid_popup').closest('tr').show();
        $('#glance_color_popup').closest('tr').show();
    }
}
function saveGlance() {
    var fieldLabel = $('#glance_Label_popup').val().trim();
    var apiName = $('#glance_apiName_popup').val().trim();
    var fieldSeq = $('#glance_field_seq_popup').val();
    var docId = $('#glance_docid_popup').val();
    var color = $('#glance_color_popup').val();
    var colspan = $("#glance_colspan_popup").val();
    var isComponant = $('#glance_Is_Comp_popup').prop("checked");
    var colSpanInt = parseInt(colspan);

    if (apiName !== '' && colspan !== '' && (isComponant || (fieldLabel !== '' && color !== '')) && colSpanInt <= 12) {
        addGlance(apiName, fieldLabel, fieldSeq, color, isComponant, colspan, docId);
    } else {
        if (colspan === '') {
            $('#glance_colspan_popup').css({ "border": "1px solid #DC1616" });
        }

        if (colSpanInt > 12) {
            $('#glance_colspan_popup').css({ "border": "1px solid #DC1616" });
            alert("Column Span should not be more than 12!");
        }

        if (apiName === '') {
            $('#glance_apiName_popup').css({ "border": "1px solid #DC1616" });
        }

        if (!isComponant) {
            if (fieldLabel === '') { $('#glance_Label_popup').css({ "border": "1px solid #DC1616" }); }
            //if(docId==''){$('#glance_docid_popup').css({"border": "1px solid #DC1616"});}
            if (color == '') { alert('Please choose the color for field.'); $('#glance_color_popup').css({ "border": "1px solid #DC1616" }); }
        }
    }
}
function closeGlanceModel() {
    closeModel('glance_field_Modal');
    $('#glance_Label_popup').val('');
    $('#glance_apiName_popup').val('');
    $('#glance_field_seq_popup').val('');
    $('#glance_docid_popup').val('');
    $('#glance_color_popup').val('');
    $('#glance_colspan_popup').val('');
    $('#glance_Is_Comp_popup').prop("checked", false);

    $('#glance_Label_popup').css({ "border": "1px solid #ccc" });
    $('#glance_apiName_popup').css({ "border": "1px solid #ccc" });
    //$('#glance_docid_popup').css({"border": "1px solid #ccc"});	
    $('#glance_color_popup').css({ "border": "1px solid #ccc" });
    $('#glance_colspan_popup').css({ "border": "1px solid #ccc" });
}
function openButtonModel() {
    $('#button_field_seq_popup').val('');
    $('#custom_Buttom_Modal_headder').html('Add Button');
    $('#custom_Buttom_Modal_button').html('Add');
    openModel('custom_Buttom_Modal');
}
function saveButton() {
    var buttonLabel = $('#button_Label_popup').val().trim();
    var apiName = $('#button_apiName_popup').val().trim();
    var action = $('#button_action_popup').val().trim();
    var jsaction = $('#button_jsaction_popup').val().trim();
    var buttonSeq = $('#button_field_seq_popup').val();

    if (buttonLabel !== '') {
        var erromsg = '';
        $('#button_Label_popup').css({ "border": "1px solid #ccc" });
        if ((apiName !== '' && action !== '') || jsaction !== '') {
            $('#returnfalseId').html('');
            if (apiName === '' && action === '') {
                if (jsaction.includes('return false')) {
                    addCustomButton(buttonLabel, apiName, action, jsaction, buttonSeq);
                    $('#button_jsaction_popup').css({ "border": "1px solid #ccc" });
                    $('#returnfalseId').html('');
                } else {
                    $('#returnfalseId').html('Javascript buttons without a URL action must end with a \'return false;\' statement.');
                    $('#button_apiName_popup').css({ "border": "1px solid #ccc" });
                    $('#button_action_popup').css({ "border": "1px solid #ccc" });
                    $('#button_jsaction_popup').css({ "border": "1px solid #DC1616" });
                }

            } else if (apiName !== '' && action !== '') {
                addCustomButton(buttonLabel, apiName, action, jsaction, buttonSeq);
                $('#button_apiName_popup').css({ "border": "1px solid #ccc" });
                $('#button_action_popup').css({ "border": "1px solid #ccc" });
            } else {
                if (apiName === '' && action !== '') {
                    $('#button_apiName_popup').css({ "border": "1px solid #DC1616" });
                    erromsg = '\'Button Api Name\' is mandatory.';
                    $('#button_action_popup').css({ "border": "1px solid #ccc" });
                    $('#button_jsaction_popup').css({ "border": "1px solid #ccc" });

                } else if (apiName !== '' && action === '') {
                    $('#button_action_popup').css({ "border": "1px solid #DC1616" });
                    erromsg = '\'Action\' is mandatory.';
                    $('#button_apiName_popup').css({ "border": "1px solid #ccc" });
                    $('#button_jsaction_popup').css({ "border": "1px solid #ccc" });
                }

                if (erromsg !== '') {
                    $('#returnfalseId').html(erromsg);
                }
            }

        } else {
            //var erromsg = '';
            if (apiName === '' && action === '' && jsaction === '') {
                $('#button_apiName_popup').css({ "border": "1px solid #DC1616" });
                $('#button_action_popup').css({ "border": "1px solid #DC1616" });
                $('#button_jsaction_popup').css({ "border": "1px solid #DC1616" });
                $('#returnfalseId').html('Add either \'Button Api Name and Action or JavaScript Action\' ');

            }
            else if (apiName === '' && action !== '') {
                $('#returnfalseId').html('\'Button Api Name\' is mandatory.');
                //erromsg = '\'Button Api Name\' is mandatory.';
                $('#button_apiName_popup').css({ "border": "1px solid #DC1616" });
                $('#button_jsaction_popup').css({ "border": "1px solid #ccc" });

            } else if (apiName !== '' && action === '') {
                $('#returnfalseId').html('\'Action\' is mandatory.');
                //erromsg = '\'Action\' is mandatory.';
                $('#button_jsaction_popup').css({ "border": "1px solid #ccc" });
                $('#button_action_popup').css({ "border": "1px solid #DC1616" });
            }

        }
    } else {
        $('#button_Label_popup').css({ "border": "1px solid #DC1616" });
    }
}
function closeButtonModel() {
    closeModel('custom_Buttom_Modal');
    $('#button_Label_popup').val('');
    $('#button_apiName_popup').val('');
    $('#button_action_popup').val('');
    $('#button_jsaction_popup').val('');
    $('#button_field_seq_popup').val('');
    $('#button_Label_popup').css({ "border": "1px solid #ccc" });
    $('#button_apiName_popup').css({ "border": "1px solid #ccc" });
    $('#button_action_popup').css({ "border": "1px solid #ccc" });
}
function openRelatedLtsModel() {
    $('#relatedLst_modal_header').html('Add Releted List');
    $('#relatedLst_pop_button').html('Add');
    $('#relatedLst_seq_popup').val('');
    openModel('relatedLst_Modal');
}
function saveRelatedLst() {
    var relatedLstLabel = $('#relatedLst_Label_popup').val().trim();
    var relatedLstName = $('#relatedLst_apiName_popup').val().trim();
    var relatedLstObjApi = $('#relatedLst_obgapi_popup').val().trim();
    var relatedLstAppSubmit = $('#relatedLst_checkbox_popup').prop("checked");
    var relatedLstSeq = $('#relatedLst_seq_popup').val();

    if (relatedLstLabel !== '' && relatedLstName !== '' && relatedLstObjApi !== '') {
        addRelatedLst(relatedLstLabel, relatedLstName, relatedLstObjApi, relatedLstAppSubmit, relatedLstSeq);
    } else {
        if (relatedLstLabel === '') {
            $('#relatedLst_Label_popup').css({ "border": "1px solid #DC1616" });
        }
        if (relatedLstName === '') {
            $('#relatedLst_apiName_popup').css({ "border": "1px solid #DC1616" });
        }
        if (relatedLstObjApi === '') {
            $('#relatedLst_obgapi_popup').css({ "border": "1px solid #DC1616" });
        }
    }
}
function closeRelatedLstModel() {
    closeModel('relatedLst_Modal');
    $('#relatedLst_Label_popup').val('');
    $('#relatedLst_apiName_popup').val('');
    $('#relatedLst_obgapi_popup').val('');
    $('#relatedLst_checkbox_popup').prop("checked", false);
    $('#relatedLst_seq_popup').val('');
    $('#relatedLst_Label_popup').css({ "border": "1px solid #ccc" });
    $('#relatedLst_apiName_popup').css({ "border": "1px solid #ccc" });
    $('#relatedLst_obgapi_popup').css({ "border": "1px solid #ccc" });
}
function isDeleteTab(tabNumber) {
    /*if(confirm('You are about to delete this tab!')){
		deleteTab(tabNumber);
	}*/
    swal({
        title: "Are you sure?",
        text: "You are about to delete this tab!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
	function () {
	    deleteTab(tabNumber);
	});
}
function swalClose(type) {
    swal("Deleted!", type + " is Deleted", "success");
}
function isDeleteSec(secNumber) {
    /*if(confirm('Do you want to delete Section')){
		deleteSection(tabNumber,secNumber);
	}*/
    swal({
        title: "Are you sure?",
        text: "You are about to delete this section!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
	function () {
	    deleteSection(secNumber);
	});
}
function isDeleteVfp(tabNumber, vfpNumber) {
    /*if(confirm('Do you want to delete Visual force page')){
		deleteVFPPage(tabNumber,vfpNumber);
	}*/

    swal({
        title: "Are you sure?",
        text: "You are about to delete this inline visualforce page!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
	function () {
	    deleteVFPPage(tabNumber, vfpNumber);
	});
}
function isFieldDelete(fieldNumber) {
    /*if(confirm('Do you want to delete Field')){
		deleteField(tabNumber,secNumber,fieldNumber);
	}*/

    swal({
        title: "Are you sure?",
        text: "You are about to delete this field!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
	function () {
	    deleteField(fieldNumber);
	});
}
function isGlanceDelete(glanceSeq) {
    /*if(confirm('Do you want to delete Field')){
		deleteGlance(glanceSeq);
	}*/

    swal({
        title: "Are you sure?",
        text: "You are about to delete this field!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
	function () {
	    deleteGlance(glanceSeq);
	});
}
function isButtonDelete(buttonSeq) {
    /*if(confirm('Do you want to delete Button')){
		deleteCustButton(buttonSeq);
	}*/

    swal({
        title: "Are you sure?",
        text: "You are about to delete this Button!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
	function () {
	    deleteCustButton(buttonSeq);
	});
}
function isRelatedLstDelete(reletedLstSeq) {
    /*if(confirm('Do you want to delete Button')){
       deleteRelatedLst(reletedLstSeq);
   }*/

    swal({
        title: "Are you sure?",
        text: "You are about to delete this related list!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
	function () {
	    deleteRelatedLst(reletedLstSeq);
	});
}
function isTabUpdate(tabId) {
    var tabName = $('#' + tabId).data('tabname');
    var bgColor = $('#' + tabId).data('bgcolor');
    //var congaApiName = $('#'+tabId).data('congaapiname');
    //var tabStatus = $('#'+tabId).data('tabstatus');
    var tabOrder = $('#' + tabId).data('taborder');
    var tabHeight = $('#' + tabId).data('tabheight');
    var tabHide = $('#' + tabId).data('tabhide');

    $('#tab_Modal_headder').html('Update Tab');
    $('#tab_Modal_button').html('Update');
    $('#tab_Label_popup').css({ "border": "1px solid #ccc" });
    $('#tab_BG_color_popup').css({ "border": "1px solid #ccc" });
    $('#tab_BG_color_popup').val(bgColor);
    $('#tab_Label_popup').val(tabName);
    //$('#tab_congaAPI_popup').val(congaApiName);
    //$('#tab_status_popup').val(tabStatus);
    $('#tab_isInsert').val(tabOrder);
    $('#tab_isUpdate').val(tabId);
    $('#tab_height').val(tabHeight);
    $('#tab_Hide').prop("checked", tabHide);
    readyColorPickar();
    openModel('tab_Modal');
}
function isSectionUpdate(secId) {
    var taborder = $('#' + secId).data('taborder');
    var tabguid = $('#' + secId).data('tabguid');
    var secorder = $('#' + secId).data('secorder');
    var sectionname = $('#' + secId).data('sectionname');
    var noOfClm = $('#' + secId).data('noofclm');
    var border = $('#' + secId).data('border');
    var sec_guid = $('#' + secId).data('secguid');
    var controlwidth = $('#' + secId).data('controlwidth');
    var secHide = $('#' + secId).data('sechide');
    var secLabelHide = $('#' + secId).data('seclabelhide');

    $('#section_Modal_headder').html('Update Section');
    $('#section_Modal_button').html('Update');

    $('#sec_Label_popup').css({ "border": "1px solid #ccc" });
    $('#sec_Label_popup').val(sectionname);
    $('#sec_column_popup').val(noOfClm);

    $('#sec_controlwidth_popup').val(controlwidth);
    $('#sec_tab_seq_popup').val(tabguid);
    $('#sec_sec_seq_popup').val(secorder);
    $('#section_isUpdate').val(sec_guid);
    $('#sec_Hide').prop("checked", secHide);
    $('#sec_LabelHide').prop("checked", secLabelHide);
    if (border === 1) {
        $('#sec_DisplayBorder').prop("checked", true);
    }
    else {
        $('#sec_DisplayBorder').prop("checked", false);
    }
    openModel('section_Modal');
}
function isVfpUpdate(vfp_pannelId) {
    var taborder = $('#' + vfp_pannelId).data('taborder');
    var secorder = $('#' + vfp_pannelId).data('secorder');
    var label = $('#' + vfp_pannelId).data('sectionname');
    var temp = $('#' + vfp_pannelId).data('api');//.split('/')
    var api = temp;//temp[temp.length -1];
    var width = $('#' + vfp_pannelId).data('width');
    var height = $('#' + vfp_pannelId).data('height');
    var isComponant = '' + $('#' + vfp_pannelId).data('iscomponant');

    $('#vfp_Modal_headder').html('Update VF Page');
    $('#vfp_Modal_button').html('Update');

    $('#vfp_Label_popup').css({ "border": "1px solid #ccc" });
    $('#vfp_InlineApiName').css({ "border": "1px solid #ccc" });
    $('#vfp_width').css({ "border": "1px solid #ccc" });
    $('#vfp_height').css({ "border": "1px solid #ccc" });
    $('#vfp_Label_popup').val(label);
    $('#vfp_InlineApiName').val(api);
    $('#vfp_width').val(width);
    $('#vfp_height').val(height);
    $('#vfp_tab_seq_popup').val(taborder);
    $('#vfp_sec_seq_popup').val(secorder);
    if (isComponant.toLowerCase() === 'true') {
        $('#vfp_Is_Comp').prop("checked", true);
    } else {
        $('#vfp_Is_Comp').prop("checked", false);
    }
    openModel('vf_page_Modal');
}
function isFieldUpdate(fieldId, popupName) {
    if (popupName === "Update Field") {
        $('#tblAddField').css({ "display": "" });
        $('#tblAddHeader').css({ "display": "none" });
    }
    else {
        $('#tblAddField').css({ "display": "none" });
        $('#tblAddHeader').css({ "display": "" });
    }
    var fieldapi = $('#' + fieldId).data('fieldapi');
    var fieldlabel = $('#' + fieldId).data('fieldlabel');
    var fieldtype = $('#' + fieldId).data('fieldtype');
    var helptext = $('#' + fieldId).data('helptext');
    var height = $('#' + fieldId).data('height');
    var isrequired = $('#' + fieldId).data('isrequired');
    var isreadonly = $('#' + fieldId).data('isreadonly');
    var isHideLabel = $('#' + fieldId).data('ishidelabel');

    var sectionsequence = $('#' + fieldId).data('sectionsequence');
    var tabsequence = $('#' + fieldId).data('tabsequence');
    var fieldsequence = $('#' + fieldId).data('fieldsequence');
    var viewFieldApi = $('#' + fieldId).data('viewfieldapi');
    var useCustomLookup = $('#' + fieldId).data('usecustomlookup');
    var fieldid = $('#' + fieldId).data('fieldguid');
    var tabid = $('#' + fieldId).data('tabguid');
    var sectionguid = $('#' + fieldId).data('sectionguid');
    //$('#field_Modal_headder').html('Update Field');
    $('#field_Modal_headder').html(popupName);
    $('#field_Modal_button').html('Update');

    if (fieldlabel === "spacer") {
        $('#field_checkbox_spacer').prop("checked", true);
        //$("input").attr("disabled", true);
        //$("select").attr("disabled", true);
        //$("textarea").attr("disabled", true);
        $("#field_Label_popup").attr("disabled", true);
        $("#field_apiName_popup").attr("disabled", true);
        $("#field_ViewFieldApi_popup").attr("disabled", true);
        //$("#field_checkbox_hidelabel").attr("disabled", true);
        $("#field_helpText_popup").attr("disabled", true);
        $("#field_checkbox_popup").attr("disabled", true);
        $("#field_checkboxreadonly_popup").attr("disabled", true);
        $("#field_height_popup").attr("disabled", true);

        $("#field_checkbox_spacer").attr("disabled", false);
    }
    else {
        $('#field_checkbox_spacer').prop("checked", false);
        //$("input").attr("disabled", false);
        //$("select").attr("disabled", false);
        //$("textarea").attr("disabled", false);
        $("#field_Label_popup").attr("disabled", false);
        $("#field_apiName_popup").attr("disabled", false);
        $("#field_ViewFieldApi_popup").attr("disabled", false);
        $("#field_checkbox_hidelabel").attr("disabled", false);
        $("#field_helpText_popup").attr("disabled", false);
        $("#field_checkbox_popup").attr("disabled", false);
        $("#field_checkboxreadonly_popup").attr("disabled", false);
        $("#field_height_popup").attr("disabled", false);
        $("#field_checkbox_spacer").attr("disabled", false);
    }

    $('#field_Label_popup').css({ "border": "1px solid #ccc" });
    $('#field_apiName_popup').css({ "border": "1px solid #ccc" });
    $('#field_Label_popup').val(fieldlabel + "-" + fieldtype);
    $('#field_apiName_popup').val(fieldapi);
    $('#field_helpText_popup').val(helptext);
    $('#field_height_popup').val(height);
    $('#field_checkbox_popup').prop("checked", isrequired);
    $('#field_checkboxreadonly_popup').prop("checked", isreadonly);
    $('#field_checkbox_hidelabel').prop("checked", isHideLabel);
    $('#field_custlookup_popup').prop("checked", useCustomLookup);
    $('#field_tab_seq_popup').val(tabid);
    $('#field_sec_seq_popup').val(sectionguid);
    $('#field_field_seq_popup').val(fieldsequence);
    $('#field_IsUpdate').val(fieldid);
    $('#field_ViewFieldApi_popup').val(viewFieldApi);

    if (popupName === "Update Field") {
    }
    else {
        $('#header_cell1').val(fieldlabel);
        $('#header_cell2').val(fieldapi);
        $('#header_checkbox_hidelabel').prop("checked", isHideLabel);
    }

    openModel('field_Modal');
}
function isGlanceUpdate(glanceId) {
    var fieldapi = $('#' + glanceId).data('fieldapi');
    var fieldlabel = $('#' + glanceId).data('fieldlabel');
    var fieldsequence = $('#' + glanceId).data('fieldorder');

    var color = $('#' + glanceId).data('color');
    var colspan = $('#' + glanceId).data('colspan');
    var docid = $('#' + glanceId).data('docid');
    var iscomp = '' + $('#' + glanceId).data('iscomp');

    $('#glance_field_Modal_headder').html('Update Field');
    $('#glance_field_Modal_button').html('Update');
    $('#glance_Label_popup').css({ "border": "1px solid #ccc" });
    $('#glance_apiName_popup').css({ "border": "1px solid #ccc" });
    $('#glance_Label_popup').val(fieldlabel);
    $('#glance_apiName_popup').val(fieldapi);
    $('#glance_field_seq_popup').val(fieldsequence);

    $('#glance_color_popup').val(color);
    $('#glance_colspan_popup').val(colspan);
    $('#glance_docid_popup').val(docid);
    if (iscomp.toLowerCase() === 'true') {
        $('#glance_Is_Comp_popup').prop("checked", true);
    } else {
        $('#glance_Is_Comp_popup').prop("checked", false);
    }
    glanceFieldChange();
    //$('#glance_Is_Comp_popup').trigger('click');
    readyColorPickar1();
    openModel('glance_field_Modal');
}
function isButtonUpdate(buttonId) {
    var buttonLabel = $('#' + buttonId).data('label');
    var apiName = $('#' + buttonId).data('apiname');
    var action = $('#' + buttonId).data('action');
    var jsaction = $('#' + buttonId).data('actionjs');
    var buttonSeq = $('#' + buttonId).data('sequence');

    $('#custom_Buttom_Modal_headder').html('Update Button');
    $('#custom_Buttom_Modal_button').html('Update');
    $('#button_Label_popup').val(buttonLabel);
    $('#button_apiName_popup').val(apiName);
    $('#button_action_popup').val(action);
    $('#button_jsaction_popup').val(jsaction);
    $('#button_field_seq_popup').val(buttonSeq);
    $('#button_Label_popup').css({ "border": "1px solid #ccc" });
    $('#button_apiName_popup').css({ "border": "1px solid #ccc" });
    $('#button_action_popup').css({ "border": "1px solid #ccc" });
    openModel('custom_Buttom_Modal');
}
function isRelatedLstUpdate(rearrangeId) {

    var relatedLstLabel = $('#' + rearrangeId).data('label');
    var relatedLstName = $('#' + rearrangeId).data('listapiname');
    var relatedLstObjApi = $('#' + rearrangeId).data('objectapiname');
    var relatedLstAppSubmit = $('#' + rearrangeId).data('aprovalsubmit');
    var relatedLstSeq = $('#' + rearrangeId).data('order');

    $('#relatedLst_modal_header').html('Update Releted List');
    $('#relatedLst_pop_button').html('Update');

    $('#relatedLst_Label_popup').val(relatedLstLabel);
    $('#relatedLst_apiName_popup').val(relatedLstName);
    $('#relatedLst_obgapi_popup').val(relatedLstObjApi);
    $('#relatedLst_checkbox_popup').prop("checked", relatedLstAppSubmit);
    $('#relatedLst_seq_popup').val(relatedLstSeq);
    $('#relatedLst_Label_popup').css({ "border": "1px solid #ccc" });
    $('#relatedLst_apiName_popup').css({ "border": "1px solid #ccc" });
    $('#relatedLst_obgapi_popup').css({ "border": "1px solid #ccc" });
    openModel('relatedLst_Modal');
}
function openTabRearrangeModel() {
    $('#rearrange_tab_type').val('tab');
    $('#tab_dragable_Modal_header').html('Rearrange Tabs');
    openModel('tab_dragable_Modal');
    tabRearrange();
}
function openSecRearrangeModel(tabNo) {
    $('#rearrange_tab_type').val('sec');
    $('#rearrange_tab_sequence').val(tabNo);
    $('#tab_dragable_Modal_header').html('Rearrange Sections');
    openModel('tab_dragable_Modal');
    SecRearrange(tabNo);
}
function openVfpRearrangeModel(tabNo) {
    $('#rearrange_tab_type').val('vfp');
    $('#rearrange_tab_sequence').val(tabNo);
    $('#tab_dragable_Modal_header').html('Rearrange Sections');
    openModel('tab_dragable_Modal');
    vfpRearrange(tabNo);
}
function saveTabArrange() {
    var tabArrangeString = '';
    var tabNumber = $('#rearrange_tab_sequence').val();
    var type = $('#rearrange_tab_type').val();
    $('#rearrange_tab_type').val('');
    $('#rearrange_tab_sequence').val('');
    $('#tabrearrangeul li').each(function () {
        tabArrangeString += $(this).data('unique') + 'SPLITXCOMMA';
    });
    if (type === 'sec' && tabNumber !== '') {
        applySecRearrange(tabArrangeString, tabNumber);
    } else if (tabNumber === '' && type === 'sec') {
        alert('Something went wrong.');
    }

    if (type === 'vfp' && tabNumber !== '') {
        applyVfpRearrange(tabArrangeString, tabNumber);
    } else if (tabNumber === '' && type === 'vfp') {
        alert('Something went wrong.');
    }

    if (type === 'tab') {
        applytabRearrange(tabArrangeString);
    }
}
function openFieldRearrangeModel(tabSeq, secSeq) {
    $('#rearrange_field_tab_sequence').val(tabSeq);
    $('#rearrange_field_sec_sequence').val(secSeq);
    $('#rearrange_field_type').val('field');
    $('#field_dragable_Modal ul').removeClass('glance');
    openModel('field_dragable_Modal');
    fieldRearrange(tabSeq, secSeq);
}
function openButtonRearrangeModel() {
    $('#rearrange_button_type').val('button');
    $('#button_dragable_Modal_header').html('Rearrange Button');
    openModel('button_dragable_Modal');
    buttonRearrange();
}
function openGlanceRearrangeModel() {
    $('#rearrange_field_type').val('glance');
    $('#field_dragable_Modal ul').removeClass('glance');
    $('#field_dragable_Modal ul').addClass('glance');
    openModel('field_dragable_Modal');
    glanceRearrange();
}
function openRelatedLtsRearrangeModel() {
    $('#rearrange_button_type').val('relatedlst');
    $('#button_dragable_Modal_header').html('Rearrange Related List');
    openModel('button_dragable_Modal');
    relatedLstRearrange();
}
function saveFieldArrange() {
    var fieldRearrangeString = '';
    var tabNumber = $('#rearrange_field_tab_sequence').val();
    var secNumber = $('#rearrange_field_sec_sequence').val();
    var type = $('#rearrange_field_type').val();

    $('#rearrange_field_tab_sequence').val('');
    $('#rearrange_field_sec_sequence').val('');
    $('#rearrange_field_type').val('');

    $('#fieldrearrangeul li').each(function () {
        fieldRearrangeString += $(this).data('unique') + ',';
    });

    if (type === 'field') {
        applyFieldRearrange(fieldRearrangeString, tabNumber, secNumber);
    } else if (type === 'glance') {
        applyGlanceRearrange(fieldRearrangeString);
    }
}
function saveButtonArrange() {
    var fieldRearrangeString = '';
    var type = $('#rearrange_button_type').val();
    $('#rearrange_button_type').val('');

    $('#buttonrearrangeul li').each(function () {
        fieldRearrangeString += $(this).data('unique') + 'SPLITXCOMMA';
    });

    if (type === 'button') {
        applyButtonRearrange(fieldRearrangeString);
    } else if (type === 'relatedlst') {
        applyrelatedLstRearrange(fieldRearrangeString);
    }
}
function closeRearrange(type) {
    if (type === 'tab') {
        $('#rearrange_tab_sequence').val('');
        closeModel('tab_dragable_Modal');
    } else if (type === 'field') {
        $('#rearrange_field_tab_sequence').val('');
        $('#rearrange_field_sec_sequence').val('');
        $('#field_dragable_Modal ul').removeClass('glance');
        closeModel('field_dragable_Modal');
    } else if (type === 'button') {
        $('#rearrange_button_type').val('');
        closeModel('button_dragable_Modal');
    }
}
function openSecVfpModel(tabSeq) {
    $('#sec_vfp_tab_seq_popup').val('');
    $('#sec_vfp_sec_seq_popup').val('');
    $('#sec_vfp_Modal_headder').html('Add CRM Page');
    $('#sec_vfp_Modal_button').html('Add');
    if (tabSeq !== '') {
        $('#sec_vfp_tab_seq_popup').val(tabSeq);
        openModel('sec_vf_page_Modal');
    }
}
function saveSecVfp() {
    var pgLabel = $('#sec_vfp_Label_popup').val().trim();
    var pgPage = $('#sec_vfp_InlineApiName').val().trim();
    var pgWidth = $('#sec_vfp_width').val();
    var pgHeight = $('#sec_vfp_height').val();
    var pgTabSeq = $('#sec_vfp_tab_seq_popup').val();
    var pgorder = $('#sec_vfp_sec_seq_popup').val();
    var isComponant = true;
    if ($('#sec_vfp_Is_Comp').prop("checked")) {
        isComponant = true;
    }
    if (pgLabel !== '' && pgTabSeq !== '') {
        addSecVfp(pgLabel, pgPage, pgWidth, pgHeight, pgTabSeq, pgorder, isComponant, '');
    } else {
        if (pgLabel === '') {
            $('#sec_vfp_Label_popup').css({ "border": "1px solid #DC1616" });
        }
        if (pgPage === '') {
            $('#sec_vfp_InlineApiName').css({ "border": "1px solid #DC1616" });
        }
        if (pgWidth === '') {
            $('#sec_vfp_width').css({ "border": "1px solid #DC1616" });
        }
        if (pgHeight === '') {
            $('#sec_vfp_height').css({ "border": "1px solid #DC1616" });
        }

        if (pgTabSeq === '') {
            alert('Something went wrong please refresh and try again.');
        }
    }
}
function closeSecVfpModel() {
    closeModel('sec_vf_page_Modal');
    $('#sec_vfp_Label_popup').css({ "border": "1px solid #ccc" });
    $('#sec_vfp_InlineApiName').css({ "border": "1px solid #ccc" });
    $('#sec_vfp_width').css({ "border": "1px solid #ccc" });
    $('#sec_vfp_height').css({ "border": "1px solid #ccc" });
    $('#sec_vfp_Label_popup').val('');
    $('#sec_vfp_InlineApiName').val('');
    $('#sec_vfp_width').val('');
    $('#sec_vfp_height').val('');
    $('#sec_vfp_tab_seq_popup').val('');
    $('#sec_vfp_sec_seq_popup').val('');
    $('#sec_vfp_Is_Comp').prop("checked", false);
    $('#sec_vfp_Is_Classic').prop("checked", false);
    $('#sec_vfp_Is_Lightning').prop("checked", false);
}
function isSecVfpUpdate(Sec_vfp_pannelId) {
    var taborder = $('#' + Sec_vfp_pannelId).data('taborder');
    var secorder = $('#' + Sec_vfp_pannelId).data('secorder');
    var label = $('#' + Sec_vfp_pannelId).data('sectionname');
    var temp = $('#' + Sec_vfp_pannelId).data('pagecomponentname');//.split('/')
    var api = temp;//temp[temp.length -1];
    var width = $('#' + Sec_vfp_pannelId).data('width');
    var height = $('#' + Sec_vfp_pannelId).data('height');
    var pagecomponentname = $('#' + Sec_vfp_pannelId).data('pagecomponentname');
    var isComponant = '' + $('#' + Sec_vfp_pannelId).data('iscomponant');
    var isClassic = '' + $('#' + Sec_vfp_pannelId).data('isclassic');
    var isLightning = '' + $('#' + Sec_vfp_pannelId).data('islightning');

    $('#sec_vfp_Modal_headder').html('Update VF Page');
    $('#sec_vfp_Modal_button').html('Update');

    $('#sec_vfp_Label_popup').css({ "border": "1px solid #ccc" });
    $('#sec_vfp_InlineApiName').css({ "border": "1px solid #ccc" });
    $('#sec_vfp_width').css({ "border": "1px solid #ccc" });
    $('#sec_vfp_height').css({ "border": "1px solid #ccc" });
    $('#sec_vfp_Label_popup').val(label);
    $('#sec_vfp_InlineApiName').val(api);
    $('#sec_vfp_width').val(width);
    $('#sec_vfp_height').val(height);
    $('#sec_vfp_tab_seq_popup').val(taborder);
    $('#sec_vfp_sec_seq_popup').val(secorder);
    $('#sec_vfp_sec_seq_popup').val(secorder);
    if (isComponant.toLowerCase() === 'true') {
        $('#sec_vfp_Is_Comp').prop("checked", true);
    } else {
        $('#sec_vfp_Is_Comp').prop("checked", false);
    }

    if (isClassic.toLowerCase() === 'true') {
        $('#sec_vfp_Is_Classic').prop("checked", true);
    } else {
        $('#sec_vfp_Is_Classic').prop("checked", false);
    }

    if (isLightning.toLowerCase() === 'true') {
        $('#sec_vfp_Is_Lightning').prop("checked", true);
    } else {
        $('#sec_vfp_Is_Lightning').prop("checked", false);
    }
    openModel('sec_vf_page_Modal');
}

// Technomile Method

function applytabRearrange(stringTab) {
    var tabId = stringTab.split('SPLITXCOMMA')
    var order = 0;
    for (var i = 0; i < tabId.length ; i++) {
        //var order = (i+1).toString();
        order = (i + 1);
        UpdateRearrangeTab(tabId[i], order.toString());
    }
    setTimeout(function () { GetTabs(); }, 1000);
    closeRearrange('tab');

}
function applyFieldRearrange(stringField, TabId, SecId) {
    var fieldId = stringField.split(',')
    for (var i = 0; i < fieldId.length ; i++) {
        var order = (i + 1).toString();
        UpdateRearrangeField(fieldId[i], order, TabId, SecId);
    }
    closeRearrange('field');
    var _tabId = "pltab_" + TabId;
    var tabName = $("#" + _tabId).data('tabname').replace(/ /g, "");
    var dvSection = "dvSection_" + TabId.replace(/-/g, "");;
    GetSections(dvSection, _tabId, TabId);
}
function applySecRearrange(stringSection, TabId) {
    var sectionId = stringSection.split('SPLITXCOMMA')
    for (var i = 0; i < sectionId.length ; i++) {
        var order = (i + 1).toString();
        UpdateRearrangeSection(sectionId[i], order);

    }
    closeRearrange('tab');
}
function UpdateRearrangeField(FieldUID, order, TabId, SecId) {

    var functionName = "UpdateEntityRecord";
    try {
        //get Server url
        var serverURL = Xrm.Page.context.getClientUrl();
        var xhr = new XMLHttpRequest();
        xhr.open("PATCH", serverURL + "/api/data/v9.0/fedcap_customizefields(" + FieldUID + ")", true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xhr.setRequestHeader("OData-MaxVersion", "4.0");
        xhr.setRequestHeader("OData-Version", "4.0");
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                xhr.onreadystatechange = null;
                if (this.status === 204) {
                    //show alert
                }
                else {
                    //var error = JSON.parse(this.response).error;
                }
            }
        };
        var objCustomiseFields = {};
        objCustomiseFields.fedcap_order = order;
        var body = JSON.stringify(objCustomiseFields);
        xhr.send(body);
    }

    catch (e) {
    }
}
function confirmMoveSection(tabFrmSeq, tabDestSeq, secSeq) {

    var functionName = "UpdateEntityRecord";
    try {
        //get Server url
        var serverURL = Xrm.Page.context.getClientUrl();
        var xhr = new XMLHttpRequest();
        xhr.open("PATCH", serverURL + "/api/data/v9.0/fedcap_customizesections(" + secSeq + ")", true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xhr.setRequestHeader("OData-MaxVersion", "4.0");
        xhr.setRequestHeader("OData-Version", "4.0");
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                xhr.onreadystatechange = null;
                if (this.status === 204) {
                    //show alert
                    GetTabs();
                    closeSecMoveModel();
                }
                else {
                    //var error = JSON.parse(this.response).error;
                }
            }
        };
        var objCustomiseSections = {};
        objCustomiseSections["fedcap_Tab@odata.bind"] = "/fedcap_customizetabs(" + tabDestSeq + ")";
        var body = JSON.stringify(objCustomiseSections);
        xhr.send(body);
    }
    catch (e) {
    }
}
function UpdateRearrangeSection(SectionUID, order) {

    var functionName = "UpdateEntityRecord";
    try {
        //get Server url
        var serverURL = Xrm.Page.context.getClientUrl();
        var xhr = new XMLHttpRequest();
        xhr.open("PATCH", serverURL + "/api/data/v9.0/fedcap_customizesections(" + SectionUID + ")", true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xhr.setRequestHeader("OData-MaxVersion", "4.0");
        xhr.setRequestHeader("OData-Version", "4.0");
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                xhr.onreadystatechange = null;
                if (this.status === 204) {
                    //show alert
                    GetTabs();
                }
                else {
                    //var error = JSON.parse(this.response).error;
                }
            }
        };
        var objCustomiseTabs = {};
        objCustomiseTabs.fedcap_order = order;
        var body = JSON.stringify(objCustomiseTabs);
        xhr.send(body);
    }
    catch (e) {
    }
}
function UpdateRearrangeTab(TabUID, order) {

    var functionName = "UpdateEntityRecord";
    try {
        //get Server url
        var serverURL = Xrm.Page.context.getClientUrl();
        var xhr = new XMLHttpRequest();
        xhr.open("PATCH", serverURL + "/api/data/v9.0/fedcap_customizetabs(" + TabUID + ")", true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xhr.setRequestHeader("OData-MaxVersion", "4.0");
        xhr.setRequestHeader("OData-Version", "4.0");
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                xhr.onreadystatechange = null;
                if (this.status === 204) {
                    //show alert
                    //GetTabs();
                }
                else {
                    //var error = JSON.parse(this.response).error;
                }
            }
        };
        var objCustomiseTabs = {};
        objCustomiseTabs.fedcap_order = order;
        var body = JSON.stringify(objCustomiseTabs);
        xhr.send(body);
    }
    catch (e) {
    }
}
function deleteTab(tabNumber) {
    var functionName = "DeleteEntityRecord";
    try {
        //get Server url

        var serverURL = Xrm.Page.context.getClientUrl();
        var xhr = new XMLHttpRequest();
        xhr.open("DELETE", serverURL + "/api/data/v9.0/fedcap_customizetabs(" + tabNumber + ")", true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xhr.setRequestHeader("OData-MaxVersion", "4.0");
        xhr.setRequestHeader("OData-Version", "4.0");
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                xhr.onreadystatechange = null;
                if (this.status === 204) {
                    GetTabs();
                    swal("Deleted!", "Tab is Deleted", "success");
                }
                else {
                    var error = JSON.parse(this.response).error;
                    //show error
                }
            }
        };
        xhr.send();
    } catch (e) {
        //Xrm.Utility.alertDialog(functionName + (e.message || e.description));
    }
}
function deleteSection(sectionNumber) {
    var functionName = "DeleteEntityRecord";
    try {
        //get Server url
        var serverURL = Xrm.Page.context.getClientUrl();
        var xhr = new XMLHttpRequest();
        xhr.open("DELETE", serverURL + "/api/data/v9.0/fedcap_customizesections(" + sectionNumber + ")", true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xhr.setRequestHeader("OData-MaxVersion", "4.0");
        xhr.setRequestHeader("OData-Version", "4.0");
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                xhr.onreadystatechange = null;
                if (this.status === 204) {
                    GetTabs();
                    swal("Deleted!", "Section is Deleted", "success");
                }
                else {
                    var error = JSON.parse(this.response).error;
                    //show error
                }
            }
        };
        xhr.send();
    } catch (e) {
        //Xrm.Utility.alertDialog(functionName + (e.message || e.description));
    }
}
function deleteField(fieldNumber) {
    var functionName = "DeleteEntityRecord";
    try {
        //get Server url

        var serverURL = Xrm.Page.context.getClientUrl();
        var xhr = new XMLHttpRequest();
        xhr.open("DELETE", serverURL + "/api/data/v9.0/fedcap_customizefields(" + fieldNumber + ")", true);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xhr.setRequestHeader("OData-MaxVersion", "4.0");
        xhr.setRequestHeader("OData-Version", "4.0");
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                xhr.onreadystatechange = null;
                if (this.status === 204) {
                    GetTabs();
                    swal("Deleted!", "Field is Deleted", "success");
                }
                else {
                    var error = JSON.parse(this.response).error;
                    //show error
                }
            }
        };
        xhr.send();
    } catch (e) {
        //Xrm.Utility.alertDialog(functionName + (e.message || e.description));
    }
}
function fieldRearrange(tabNo, secNo) {
    debugger;
    $("#fieldrearrangeul").empty();
    var tabId = "pltab_" + tabNo;
    var sectionId = tabId + '_section_' + secNo;
    var noofclm = $('#' + sectionId).data('noofclm');
    var LIWidth = "";
    if (noofclm === 1) {
        LIWidth = "98%";
    }
    else if (noofclm === 2) {
        LIWidth = "48%";
    }
    else if (noofclm === 3) {
        LIWidth = "33%";
    }
    else if (noofclm === 4) {
        LIWidth = "24%";
    }
    else if (noofclm === 5) {
        LIWidth = "19%";
    }
    try {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizefields?$filter=_fedcap_section_value eq " + secNo + " &$orderby=fedcap_order asc",
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var result = data;
                try {
                    for (var i = 0; i < result.value.length; i++) {
                        //$("#fieldrearrangeul").append("<li style='width: " + LIWidth + "' id=" + result.value[i]["fedcap_customizefieldid"] + " data-unique=" + result.value[i]["fedcap_customizefieldid"] + " class='ui-state-default ui-sortable-handle'>" + result.value[i]["fedcap_name"] + "</li>");
                        if (result.value[i]["fedcap_fieldtype"] === "header") {
                            $("#fieldrearrangeul").append("<li style='width: " + LIWidth + ";text-decoration: underline;font-weight: bold' id=" + result.value[i]["fedcap_customizefieldid"] + " data-unique=" + result.value[i]["fedcap_customizefieldid"] + " class='ui-state-default ui-sortable-handle'>" + result.value[i]["fedcap_name"] + "</li>");
                        }
                        else {
                            $("#fieldrearrangeul").append("<li style='width: " + LIWidth + "' id=" + result.value[i]["fedcap_customizefieldid"] + " data-unique=" + result.value[i]["fedcap_customizefieldid"] + " class='ui-state-default ui-sortable-handle'>" + result.value[i]["fedcap_name"] + "</li>");
                        }

                    }

                } catch (e) {

                }
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    } catch (e) {

    }
}
function SecRearrange(tabNo) {
    $("#tabrearrangeul").empty();

    try {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizesections?$filter=_fedcap_tab_value eq " + tabNo + " &$orderby=fedcap_order asc",
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var result = data;
                try {

                    for (var i = 0; i < result.value.length; i++) {
                        $("#tabrearrangeul").append("<li id=" + result.value[i]["fedcap_customizesectionid"] + " data-unique=" + result.value[i]["fedcap_customizesectionid"] + " class='ui-state-default ui-sortable-handle'>" + result.value[i]["fedcap_name"] + "</li>");
                    }

                } catch (e) {

                }
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    } catch (e) {

    }
}
function tabRearrange() {
    $("#tabrearrangeul").empty();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizetabs?$orderby=fedcap_order asc",
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
            XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
        },
        async: true,
        success: function (data, textStatus, xhr) {
            var result = data;
            try {

                for (var i = 0; i < result.value.length; i++) {
                    $("#tabrearrangeul").append("<li id=" + result.value[i]["fedcap_customizetabid"] + " data-unique=" + result.value[i]["fedcap_customizetabid"] + " class='ui-state-default ui-sortable-handle'>" + result.value[i]["fedcap_name"] + "</li>");
                }

            } catch (e) {

            }
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}
function GetTabs() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizetabs?$orderby=fedcap_order asc",
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
            XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
        },
        async: true,
        success: function (data, textStatus, xhr) {
            var result = data;
            try {
                $('#tab_isInsert').val((result.value.length + 1));
                $('#pl_tab_content_container').empty();
                $('#pl_tab_container').empty();
                for (var i = 0; i < result.value.length; i++) {
                    var hrfLink = "#pltab_" + result.value[i]["fedcap_customizetabid"];
                    var LIclassName = '', TabclassName = 'tab-pane';
                    if (i === 0) {
                        LIclassName = "active";
                        TabclassName = "tab-pane active"
                    }
                    $("#pl_tab_container").append("<li class='" + LIclassName + "' role='presentation' id='id_" + result.value[i]["fedcap_customizetabid"] + "'><a aria-controls='home' aria-expanded='' data-toggle='tab' role='tabpanel' data-tabname='" + result.value[i]["fedcap_name"] + "' data-tabhide='" + result.value[i]["fedcap_ishidetab"] + "' data-tabheight='" + result.value[i]["fedcap_height"] + "' data-bgcolor='" + result.value[i]["fedcap_backgroundcolor"] + "' data-congaapiname='" + result.value[i]["fedcap_apinameforcongafield"] + "'data-tabstatus='" + result.value[i]["fedcap_tabstatus"] + "' data-taborder='" + result.value[i]["fedcap_order"] + "' id='" + result.value[i]["fedcap_customizetabid"] + "' href='" + hrfLink + "'>" + result.value[i]["fedcap_name"] + "</a></li>");
                    var ImgEdit = $('<img></img>').attr('src', '/WebResources/fedcap_/img/edit.png').attr('onclick', 'isTabUpdate("' + result.value[i]["fedcap_customizetabid"] + '")');
                    var ImgDelete = $('<img></img>').attr('src', '/WebResources/fedcap_/img/close.png').attr('onclick', 'isDeleteTab("' + result.value[i]["fedcap_customizetabid"] + '")');
                    $("#" + result.value[i]["fedcap_customizetabid"]).append(ImgEdit);
                    $("#" + result.value[i]["fedcap_customizetabid"]).append(ImgDelete);
                    $("#pl_tab_content_container").append("<div class='" + TabclassName + "' role='tabpanel' data-taborder='' data-congaapiname='' data-bgcolor='" + result.value[i]["fedcap_backgroundcolor"] + "' data-tabname='" + result.value[i]["fedcap_name"] + "' data-tabstatus='" + result.value[i]["fedcap_tabstatus"] + "' data-tabguid='" + result.value[i]["fedcap_customizetabid"] + "' id='pltab_" + result.value[i]["fedcap_customizetabid"] + "'>   </div>");
                    var btnAddSection = $('<button>Add Section</button>').addClass('button1 ButtonSpace').attr('onclick', 'openSecModel("' + result.value[i]["fedcap_customizetabid"] + '")');
                    var btnAddComponent = $('<button>Add Page/Component</button>').addClass('button1 ButtonSpace').attr('onclick', 'openSecVfpModel("' + result.value[i]["fedcap_customizetabid"] + '")');
                    var btnReArrngSection = $('<button>Re-Arrange Section</button>').addClass('button1 ButtonSpace').attr('onclick', 'openSecRearrangeModel("' + result.value[i]["fedcap_customizetabid"] + '")');
                    $("#pltab_" + result.value[i]["fedcap_customizetabid"]).append(btnAddSection);
                    $("#pltab_" + result.value[i]["fedcap_customizetabid"]).append(btnAddComponent);
                    $("#pltab_" + result.value[i]["fedcap_customizetabid"]).append(btnReArrngSection);
                    $("#pltab_" + result.value[i]["fedcap_customizetabid"]).append("<div id='dvSection_" + result.value[i]["fedcap_customizetabid"].replace(/-/g, "") + "'></div>");
                    GetSections("dvSection_" + result.value[i]["fedcap_customizetabid"].replace(/-/g, ""), "pltab_" + result.value[i]["fedcap_customizetabid"], result.value[i]["fedcap_customizetabid"]);
                }

            } catch (e) {
            }
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}
function GetSections(divSection, TabId, tabGUID) {
    try {
        var URL = Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizesections?$filter=_fedcap_tab_value eq " + tabGUID + " &$orderby=fedcap_order asc";
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizesections?$filter=_fedcap_tab_value eq " + tabGUID + " &$orderby=fedcap_order asc",
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var result = data;
                try {
                    $("#" + divSection).empty();

                    for (var i = 0; i < result.value.length; i++) {
                        var sectionId = TabId + "_section_" + result.value[i]["fedcap_customizesectionid"];
                        var isComponent = result.value[i]["fedcap_iscomponentpage"];
                        var h2 = result.value[i]["fedcap_name"];
                        var btnAddFeild, btnMoveSection, btnReArrngField, ImgEdit, ImgDelete;
                        btnMoveSection = $('<button>Move Section</button>').addClass('button1 bluebtn ButtonSpace').attr('onclick', 'openMoveSectionModel("' + tabGUID + '","' + result.value[i]["fedcap_customizesectionid"] + '",this,"sec")');
                        ImgDelete = $('<img></img>').attr('src', '/WebResources/fedcap_/img/close2.png').attr('onclick', 'isDeleteSec("' + result.value[i]["fedcap_customizesectionid"] + '")');
                        if (isComponent === true) {
                            h2 = result.value[i]["fedcap_name"] + " (CRM/Component)";
                            ImgEdit = $('<img></img>').attr('src', '/WebResources/fedcap_/img/edit2.png').attr('onclick', 'isSecVfpUpdate("' + sectionId + '")');
                        }
                        else {
                            btnAddFeild = $('<button>Add Field</button>').addClass('button1 bluebtn ButtonSpace').attr('onclick', 'openFieldModel("' + tabGUID + '","' + result.value[i]["fedcap_customizesectionid"] + '","Add Field")');
                            btnReArrngField = $('<button>Re-Arrange Field</button>').addClass('button1 bluebtn ButtonSpace').attr('onclick', 'openFieldRearrangeModel("' + tabGUID + '","' + result.value[i]["fedcap_customizesectionid"] + '")');
                            btnAddHeader = $('<button>Add Header</button>').addClass('button1 bluebtn ButtonSpace').attr('onclick', 'openFieldModel("' + tabGUID + '","' + result.value[i]["fedcap_customizesectionid"] + '","Add Header")');
                            ImgEdit = $('<img></img>').attr('src', '/WebResources/fedcap_/img/edit2.png').attr('onclick', 'isSectionUpdate("' + sectionId + '")');
                        }

                        var divRow = "<div data-sectionname='" + result.value[i]["fedcap_name"] + "' data-seclabelhide='" + result.value[i]["fedcap_ishidesectionlabel"] + "' data-sechide='" + result.value[i]["fedcap_ishidesection"] + "' data-pagecomponentname='" + result.value[i]["fedcap_pagecomponentname"] + "' data-height='" + result.value[i]["fedcap_pagecomponentheight"] + "' data-iscomponent='" + result.value[i]["fedcap_iscomponentpage"] + "' data-noofclm='" + result.value[i]["fedcap_sectioncolumn"] + "' data-border='" + result.value[i]["fedcap_displayborder"] + "'  data-controlwidth='" + result.value[i]["fedcap_controlwidth"] + "'  data-taborder='' data-secorder='" + result.value[i]["fedcap_order"] + "' data-secguid='" + result.value[i]["fedcap_customizesectionid"] + "'";
                        divRow = divRow + " data-tabguid='" + tabGUID + "' class='row' id='" + sectionId + "'>";
                        divRow = divRow + "<div class='fulldiv' id='fulldiv" + sectionId + "'><h2 class='sectionHeading'>" + h2 + "</h2></div>";
                        divRow = divRow + "</div>";
                        $("#" + divSection).append(divRow);
                        if (isComponent === true) {
                        }
                        else {
                            $("#fulldiv" + sectionId).append(btnAddFeild);
                            $("#fulldiv" + sectionId).append(btnReArrngField);
                            $("#fulldiv" + sectionId).append(btnAddHeader);
                        }
                        $("#fulldiv" + sectionId).append(btnMoveSection);
                        $("#fulldiv" + sectionId).append("<div style='position:absolute; right:15px; top:20px' id='fulldivCorner" + sectionId + "'><a class='editSec ButtonSpace' id='SecEdit" + sectionId + "'></a><a class='editSec' id='SecDel" + sectionId + "'></a></div>");
                        $("#SecEdit" + sectionId).append(ImgEdit);
                        $("#SecDel" + sectionId).append(ImgDelete);
                        $("#" + sectionId).append("<div id='dvFieldSection_" + result.value[i]["fedcap_customizesectionid"].replace(/-/g, "") + "'></div>");
                        $("#" + sectionId).append("<div class='fulldiv' id='fulldiv2" + sectionId + "'></div>");
                        $("#fulldiv2" + sectionId).append("<hr />");
                        if (isComponent === true) {
                        }
                        else {
                            GetFields("dvFieldSection_" + result.value[i]["fedcap_customizesectionid"].replace(/-/g, ""), sectionId, result.value[i]["fedcap_customizesectionid"], tabGUID, result.value[i]["fedcap_sectioncolumn"]);
                        }
                    }

                } catch (e) {
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    } catch (e) {
        console.log(e.message.toString());
    }
}
function GetFields(divFieldSection, SectionId, SectionGUID, tabGUID, noofclm) {
    try {
        debugger;
        var divClass = "halfdiv";
        var imageMargin = "";
        if (noofclm === 3) {
            divClass = "halfdiv2";
            imageMargin = "imageMargin";
        }
        else if (noofclm === 1) {
            divClass = "fulldiv";
        }
        else if (noofclm === 4) {
            divClass = "halfdiv4";
        }
        else if (noofclm === 5) {
            divClass = "halfdiv5";
        }

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizefields?$filter=_fedcap_section_value eq " + SectionGUID + " &$orderby=fedcap_order asc",
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: true,
            success: function (data, textStatus, xhr) {
                var result = data;
                try {
                    $("#" + divFieldSection).empty();
                    $("#" + divFieldSection).attr('data-fieldcount', result.value.length);
                    for (var i = 0; i < result.value.length; i++) {
                        var _varFieldId = SectionId + "_field_" + result.value[i]["fedcap_customizefieldid"];
                        var divRow = "<div class='" + divClass + "' id='" + _varFieldId + "' data-fieldapi='" + result.value[i]["fedcap_fieldapiname"] + "' data-fieldlabel='" + result.value[i]["fedcap_name"] + "' data-fieldsequence='" + result.value[i]["fedcap_order"] + "'";
                        divRow = divRow + " data-helptext='" + result.value[i]["fedcap_helptext"] + "' data-isrequired='" + result.value[i]["fedcap_isrequired"] + "' data-isreadonly='" + result.value[i]["fedcap_isreadonly"] + "' data-ishidelabel='" + result.value[i]["fedcap_ishidelabel"] + "' data-height='" + result.value[i]["fedcap_height"] + "'";
                        divRow = divRow + " data-fieldtype='" + result.value[i]["fedcap_fieldtype"] + "' data-sectionguid='" + SectionGUID + "'";
                        divRow = divRow + " data-tabguid='" + tabGUID + "' data-fieldguid='" + result.value[i]["fedcap_customizefieldid"] + "' data-viewfieldapi='" + result.value[i]["fedcap_viewfieldapi"] + "'>";
                        //divRow = divRow + " <div alt='" + result.value[i]["fedcap_name"] + "' class='col45 bold'> " + result.value[i]["fedcap_name"] + " </div>";
                        //divRow = divRow + " <div alt='" + result.value[i]["fedcap_name"] + "' class='col45'> " + result.value[i]["fedcap_fieldapiname"] + " </div>";
                        if (result.value[i]["fedcap_fieldtype"] === "header") {
                            divRow = divRow + " <div alt='" + result.value[i]["fedcap_name"] + "' class='col45 bold' style='text-decoration: underline;'> " + result.value[i]["fedcap_name"] + " </div>";
                            divRow = divRow + " <div alt='" + result.value[i]["fedcap_name"] + "' class='col45 bold' style='text-decoration: underline;'> " + result.value[i]["fedcap_fieldapiname"] + " </div>";
                        }
                        else {
                            divRow = divRow + " <div alt='" + result.value[i]["fedcap_name"] + "' class='col45 bold'> " + result.value[i]["fedcap_name"] + " </div>";
                            divRow = divRow + " <div alt='" + result.value[i]["fedcap_name"] + "' class='col45'> " + result.value[i]["fedcap_fieldapiname"] + " </div>";
                        }
                        divRow = divRow + " <div class='col10'> <a class='edit' id='FieldEdit" + _varFieldId + "'></a><a class='edit' id='FieldDel" + _varFieldId + "'></a> </div>";
                        divRow = divRow + " </div>";
                        $("#" + divFieldSection).append(divRow);
                        var popupName = "Update Field";
                        if (result.value[i]["fedcap_fieldtype"] === "header") {
                            popupName = "Update Header";
                        }
                        var ImgEdit = $('<img ></img>').attr('src', '/WebResources/fedcap_/img/edit.png').attr('onclick', 'isFieldUpdate("' + _varFieldId + '","' + popupName + '")');
                        var ImgDelete = $('<img class="' + imageMargin + '"></img>').attr('src', '/WebResources/fedcap_/img/close.png').attr('onclick', 'isFieldDelete("' + result.value[i]["fedcap_customizefieldid"] + '")');
                        $("#FieldEdit" + _varFieldId).append(ImgEdit);
                        $("#FieldDel" + _varFieldId).append(ImgDelete);
                    }

                } catch (e) {

                }
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    } catch (e) {
        console.log(e.message.toString());
    }
}
function addTab(tabLabel, bgColor, congaAPI, status, tabNumber, tabId, Height, isShow) {
    var functionName = "Update Entity Customise Tab";
    try {
        var objCustomiseTabs = {};
        //objCustomiseTabs.fedcap_apinameforcongafield = congaAPI; 
        objCustomiseTabs.fedcap_backgroundcolor = bgColor;
        //objCustomiseTabs.fedcap_tabstatus = status; 
        objCustomiseTabs.fedcap_name = tabLabel;
        objCustomiseTabs.fedcap_tabid = "tab_" + tabLabel;
        objCustomiseTabs.fedcap_order = tabNumber;
        objCustomiseTabs.fedcap_height = Height;
        objCustomiseTabs.fedcap_ishidetab = isShow;
        if (tabId == '') {
            Xrm.WebApi.createRecord("fedcap_customizetab", objCustomiseTabs).then(function (result) {
                var recordId = result.id;
                closeTabModel();
                GetTabs();
            })
        }
        else {
            Xrm.WebApi.updateRecord("fedcap_customizetab", tabId, objCustomiseTabs).then(
                      function success(result) {
                          closeTabModel();
                          GetTabs();
                      },
                      function (error) {
                      }
                  );
        }
    } catch (e) {
        Xrm.Utility.alertDialog(functionName + (e.message || e.description));
    }
}
function addSection(secLabel, noofclm, tabSeq, secorder, sectionId, controlwidth, isHide, isLabelHide, isDisplayBorder) {
    var sectionCss = "";
    if (secorder === "") {
        secorder = "1";
    }
    debugger;
    if (controlwidth === '') {
        controlwidth = null;
    }
    if (noofclm === "") { noofclm = null; sectionCss = "fulldiv"; }
    else if (noofclm === "1") { sectionCss = "fulldiv"; }
    else if (noofclm === "2") { sectionCss = "halfdiv2"; }
    else if (noofclm === "3") { sectionCss = "halfdiv3"; }


    var functionName = "Update Entity Customize Section";
    try {
        var objCustomiseSections = {};
        objCustomiseSections["fedcap_Tab@odata.bind"] = "/fedcap_customizetabs(" + tabSeq + ")";
        objCustomiseSections.fedcap_name = secLabel;
        objCustomiseSections.fedcap_order = secorder;
        objCustomiseSections.fedcap_sectioncolumn = noofclm;
        objCustomiseSections.fedcap_sectioncolumncss = sectionCss;
        objCustomiseSections.fedcap_controlwidth = controlwidth;
        objCustomiseSections.fedcap_ishidesection = isHide;
        objCustomiseSections.fedcap_ishidesectionlabel = isLabelHide;
        if (isDisplayBorder) {
            objCustomiseSections.fedcap_displayborder = 1;
        }
        else {
            objCustomiseSections.fedcap_displayborder = 0;
        }
        if (isLabelHide) {
            objCustomiseSections.fedcap_paddingtop = "30px";
        }
        else {
            objCustomiseSections.fedcap_paddingtop = null;
        }

        if (sectionId === '') {
            Xrm.WebApi.createRecord("fedcap_customizesection", objCustomiseSections).then(function (result) {
                var recordId = result.id;
                closeSecModel();
                var tabId = "pltab_" + tabSeq;
                var tabName = $("#" + tabId).data('tabname').replace(/ /g, "");
                var dvSection = "dvSection_" + tabSeq.replace(/-/g, "");
                GetSections(dvSection, tabId, tabSeq);
            })
        }
        else {
            Xrm.WebApi.updateRecord("fedcap_customizesection", sectionId, objCustomiseSections).then(
                      function success(result) {
                          closeSecModel();
                          var tabId = "pltab_" + tabSeq;
                          var tabName = $("#" + tabId).data('tabname').replace(/ /g, "");
                          var dvSection = "dvSection_" + tabSeq.replace(/-/g, "");
                          GetSections(dvSection, tabId, tabSeq);
                      },
                      function (error) {
                      }
                  );
        }
    } catch (e) {
        Xrm.Utility.alertDialog(functionName + (e.message || e.description));
    }
}
function addSecVfp(pgLabel, pgPage, pgWidth, pgHeight, tabSeq, pgorder, isComponant, sectionId) {
    if (pgorder === "") {
        pgorder = "1";
    }
    var functionName = "Update Entity Customize Section";
    try {
        //get Server url
        var serverURL = Xrm.Page.context.getClientUrl();
        var xhr = new XMLHttpRequest();
        if (sectionId == '') {
            xhr.open("POST", serverURL + "/api/data/v9.0/fedcap_customizesections", true);
        }
        else {
            xhr.open("PATCH", serverURL + "/api/data/v9.0/fedcap_customizesections(" + sectionId + ")", true);
        }
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xhr.setRequestHeader("OData-MaxVersion", "4.0");
        xhr.setRequestHeader("OData-Version", "4.0");
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                xhr.onreadystatechange = null;
                if (this.status === 204) {
                    closeSecVfpModel();
                    var tabId = "pltab_" + tabSeq;
                    var tabName = $("#" + tabId).data('tabname').replace(/ /g, "");
                    var dvSection = "dvSection_" + tabSeq.replace(/-/g, "");
                    GetSections(dvSection, tabId, tabSeq);
                }
                else {
                    var error = JSON.parse(this.response).error;
                    Xrm.Utility.alertDialog(error.message);
                }
            }
        };
        var objCustomiseSections = {};
        objCustomiseSections["fedcap_Tab@odata.bind"] = "/fedcap_customizetabs(" + tabSeq + ")";
        objCustomiseSections.fedcap_name = pgLabel;
        objCustomiseSections.fedcap_pagecomponentname = pgPage;
        objCustomiseSections.fedcap_pagecomponentheight = pgHeight;
        objCustomiseSections.fedcap_iscomponentpage = isComponant;
        objCustomiseSections.fedcap_order = pgorder;
        var body = JSON.stringify(objCustomiseSections);
        xhr.send(body);
    } catch (e) {
        Xrm.Utility.alertDialog(functionName + (e.message || e.description));
    }
}

var objectTypeCode = '', SourceType = '', SchemaName = '';
function GetObjectTypeCode(fieldName) {
    try {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v8.2/EntityDefinitions(LogicalName='opportunity')/Attributes(LogicalName='" + fieldName + "')",
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: false,
            success: function (data, textStatus, xhr) {
                var result = data.Targets;
                try {
                    if (data.SourceType !== null) {
                        SourceType = data.SourceType.toString();
                    }
                    objectTypeCode = result[0];
                    SchemaName = data.SchemaName.toString();

                } catch (e) {

                }
            },
            error: function (xhr, textStatus, errorThrown) {
                var err = textStatus;
            }
        });
    } catch (e) {

    }
}
function addField(apiName, fieldLabel, helpText, isRequired, tabSeq, secSeq, fieldSeq, ViewFieldApi, useCustomLookup, field_guid, isReadonly, height, isHideLabel, field_checkbox_spacer) {
    if (height === '') {
        height = null
    }
    var tabId = "pltab_" + tabSeq;
    var sectionId = tabId + '_section_' + secSeq;
    var dvFieldSectionId = 'dvFieldSection_' + secSeq.replace(/-/g, "");
    var fieldCount = $('#' + dvFieldSectionId).data('fieldcount');
    var fieldOrder = 0;
    try {
        if (field_guid === '') {
            fieldOrder = parseInt(fieldCount);
        }
        else {
        }

    } catch (e) {

    }
    fieldOrder++;
    var functionName = "Update Entity Customize Field";
    try {
        var type = fieldLabel.split('-');
        var fieldType = type[type.length - 1];
        var _fieldLabel = "";
        for (var label = 0; label < type.length - 1; label++) {
            _fieldLabel = _fieldLabel + type[label];
        }
        objectTypeCode = '';
        SourceType = '';
        SchemaName = '';
        if (!field_checkbox_spacer) {
            GetObjectTypeCode(apiName);
        }
        else {
        }

        var objCustomiseFields = {};
        objCustomiseFields["fedcap_Section@odata.bind"] = "/fedcap_customizesections(" + secSeq + ")";
        objCustomiseFields.fedcap_name = _fieldLabel;
        objCustomiseFields.fedcap_isrequired = isRequired;
        objCustomiseFields.fedcap_isreadonly = isReadonly;
        objCustomiseFields.fedcap_height = height;
        objCustomiseFields.fedcap_ishidelabel = isHideLabel;

        if (field_guid === '') {
            objCustomiseFields.fedcap_order = fieldOrder;
        }
        else {
        }
        objCustomiseFields.fedcap_fieldapiname = apiName;
        objCustomiseFields.fedcap_helptext = helpText;
        objCustomiseFields.fedcap_viewfieldapi = ViewFieldApi;
        objCustomiseFields.fedcap_fieldtype = fieldType;
        objCustomiseFields.fedcap_sourcetype = SourceType;
        if (fieldType === 'Lookup' || fieldType === 'Customer') {
            //objectTypeCode = '';
            //GetObjectTypeCode(apiName);
            if (apiName.includes("_value")) {

            }
            else {
                if (apiName === "parentaccountid") {
                    SchemaName = apiName;// ParentAccountId with schema name not saving
                }
                objCustomiseFields.fedcap_fieldapiname = "_" + apiName + "_value";
                objCustomiseFields.fedcap_formattedvalue = "_" + apiName + "_value@OData.Community.Display.V1.FormattedValue";
                objCustomiseFields.fedcap_lookupobjecttypecode = objectTypeCode;
                objCustomiseFields.fedcap_viewfieldapi = SchemaName;
            }
        }
        if (fieldType === 'DateTime' || fieldType === 'Money' || fieldType === 'Decimal' || fieldType === 'Integer' || fieldType === 'BigInt' || fieldType === 'Boolean') {
            objCustomiseFields.fedcap_formattedvalue = apiName + "@OData.Community.Display.V1.FormattedValue";
        }

        //**************************************************************************************************
        if (field_guid === '') {
            Xrm.WebApi.createRecord("fedcap_customizefield", objCustomiseFields).then(function (result) {
                var recordId = result.id;
                closeFieldModel();
                var secName = $("#" + sectionId).data('sectionname').replace(/ /g, "");
                var noofclm = $("#" + sectionId).data('noofclm');
                var noofclmInt = parseInt(noofclm);
                var dvFieldSection = "dvFieldSection_" + secSeq.replace(/-/g, "");;
                GetFields(dvFieldSection, sectionId, secSeq, tabSeq, noofclmInt);
            })
        }
        else {
            Xrm.WebApi.updateRecord("fedcap_customizefield", field_guid, objCustomiseFields).then(
                      function success(result) {
                          closeFieldModel();
                          var secName = $("#" + sectionId).data('sectionname').replace(/ /g, "");
                          var noofclm = $("#" + sectionId).data('noofclm');
                          var noofclmInt = parseInt(noofclm);
                          var dvFieldSection = "dvFieldSection_" + secSeq.replace(/-/g, "");;
                          GetFields(dvFieldSection, sectionId, secSeq, tabSeq, noofclmInt);
                      },
                      function (error) {
                      }
                  );
        }

        //**************************************************************************************************

    } catch (e) {
        Xrm.Utility.alertDialog(functionName + (e.message || e.description));
    }
}
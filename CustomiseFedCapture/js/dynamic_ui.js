﻿function GetTabsUI() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizetabs?$orderby=fedcap_order asc",
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
            XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
        },
        async: false,
        success: function (data, textStatus, xhr) {
            var result = data;
            try {
                var pl_tab_content_container = document.getElementById("pl_tab_content_container");
                pl_tab_content_container.innerHTML = "";
                var pl_tab_container = document.getElementById("pl_tab_container");
                pl_tab_container.innerHTML = "";
                for (var i = 0; i < result.value.length; i++) {
                    var hrfLink = "#pltab_" + result.value[i]["fedcap_customizetabid"];
                    var li = document.createElement("li");
                    if (i == 0) {
                        li.className = "active";
                    }
                    else {
                        li.className = "";
                    }
                    li.setAttribute("role", "presentation");
                    li.setAttribute("id", "id_" + result.value[i]["fedcap_customizetabid"] + "");
                    var link = document.createElement("a");
                    link.setAttribute("aria-controls", "home");
                    link.setAttribute("aria-expanded", "");
                    link.setAttribute("data-toggle", "tab");
                    link.setAttribute("data-tabname", result.value[i]["fedcap_name"]);
                    link.setAttribute("data-bgcolor", result.value[i]["fedcap_backgroundcolor"]);
                    link.setAttribute("data-congaapiname", result.value[i]["fedcap_apinameforcongafield"]);
                    link.setAttribute("data-tabstatus", result.value[i]["fedcap_tabstatus"]);
                    link.setAttribute("data-taborder", result.value[i]["fedcap_order"]);
                    link.setAttribute("href", hrfLink);
                    link.setAttribute("role", "tabpanel");
                    link.setAttribute("id", "" + result.value[i]["fedcap_customizetabid"] + "");
                    link.text = result.value[i]["fedcap_name"];
                    link.innerHTML = result.value[i]["fedcap_name"];
                    link.innerText = result.value[i]["fedcap_name"];
                    li.appendChild(link);
                    pl_tab_container.appendChild(li);
                    var div = document.createElement("div");
                    if (i == 0) {
                        div.className = "tab-pane active";
                    }
                    else {
                        div.className = "tab-pane";
                    }
                    div.setAttribute("data-bgcolor", result.value[i]["fedcap_backgroundcolor"]);
                    div.setAttribute("data-congaapiname", "");
                    div.setAttribute("data-tabname", result.value[i]["fedcap_name"]);
                    div.setAttribute("data-taborder", "");
                    div.setAttribute("data-tabstatus", result.value[i]["fedcap_tabstatus"]);
                    div.setAttribute("data-tabguid", result.value[i]["fedcap_customizetabid"]);
                    div.setAttribute("role", "tabpanel");
                    div.id = "pltab_" + result.value[i]["fedcap_customizetabid"];
                    var divSection = document.createElement("div");
                    divSection.id = "dvSection_" + result.value[i]["fedcap_name"];
                    div.appendChild(divSection);
                    pl_tab_content_container.appendChild(div);
                    GetSectionsUI(divSection.id, div.id, result.value[i]["fedcap_customizetabid"]);
                }
            } catch (e) {
            }
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}
function GetSectionsUI(divSection, TabId, tabGUID) {
    try {
        var URL = Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizesections?$filter=_fedcap_tab_value eq " + tabGUID + " &$orderby=fedcap_order asc";
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizesections?$filter=_fedcap_tab_value eq " + tabGUID + " &$orderby=fedcap_order asc",
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: false,
            success: function (data, textStatus, xhr) {
                var result = data;
                try {
                    var pl_tab_container = document.getElementById(divSection);
                    pl_tab_container.innerHTML = "";
                    var divTop = document.createElement("div");
                    divTop.setAttribute("class", "tablewrap");
                    var tableTop = document.createElement("table");
                    tableTop.setAttribute("class", "table");
                    tableTop.setAttribute("border", "0");
                    tableTop.setAttribute("cellspacing", "0");
                    tableTop.setAttribute("cellpadding", "0");
                    var tbodyTop = document.createElement("tbody");
                    var trTop = document.createElement("tr");
                    var tdTop1 = document.createElement("td");
                    tdTop1.setAttribute("align", "right");
                    tdTop1.setAttribute("valign", "middle");
                    var buttonSaveTop = document.createElement("button");
                    buttonSaveTop.setAttribute("name", "submit");
                    buttonSaveTop.setAttribute("title", "Submit");
                    buttonSaveTop.setAttribute("class", "btn btn-primary");
                    buttonSaveTop.setAttribute("id", "btnSaveTop" + TabId);
                    buttonSaveTop.setAttribute("value", "save");
                    buttonSaveTop.setAttribute("ng-click", "saveForm(OppModel)");
                    buttonSaveTop.innerHTML = "Submit";
                    tdTop1.appendChild(buttonSaveTop);
                    trTop.appendChild(tdTop1);
                    var tdTop2 = document.createElement("td");
                    tdTop2.setAttribute("align", "left");
                    tdTop2.setAttribute("valign", "middle");
                    var buttonCancelTop = document.createElement("button");
                    buttonCancelTop.setAttribute("name", "submit");
                    buttonCancelTop.setAttribute("title", "Submit");
                    buttonCancelTop.setAttribute("class", "btn btn-primary");
                    buttonCancelTop.setAttribute("id", "btnCancelTop" + TabId);
                    buttonCancelTop.setAttribute("value", "save");
                    buttonCancelTop.setAttribute("ng-click", "cancelDetails(OppModel)");
                    buttonCancelTop.innerHTML = "Cancel";
                    tdTop2.appendChild(buttonCancelTop);
                    trTop.appendChild(tdTop2);
                    tbodyTop.appendChild(trTop);
                    tableTop.appendChild(tbodyTop);
                    divTop.appendChild(tableTop);
                    pl_tab_container.appendChild(divTop);
                    for (var i = 0; i < result.value.length; i++) {
                        var divRow = document.createElement("div");
                        divRow.setAttribute("data-sectionname", result.value[i]["fedcap_name"]);
                        divRow.setAttribute("data-taborder", "");
                        divRow.setAttribute("data-secorder", result.value[i]["fedcap_order"]);
                        divRow.setAttribute("data-secguid", result.value[i]["fedcap_customizesectionid"]);
                        divRow.setAttribute("data-tabguid", result.value[i]["fedcap_customizetabid"]);
                        divRow.id = TabId + "_section_" + result.value[i]["fedcap_customizesectionid"];
                        var h4 = document.createElement("h4");
                        h4.innerHTML = result.value[i]["fedcap_name"];
                        divRow.appendChild(h4);
                        var divSectionField = document.createElement("div");
                        divSectionField.id = "dvFieldSection_" + result.value[i]["fedcap_name"].replace(' ', '');
                        divRow.appendChild(divSectionField);
                        var fulldiv2 = document.createElement("div");
                        var hr = document.createElement('hr');
                        fulldiv2.appendChild(hr);
                        divRow.appendChild(fulldiv2);
                        pl_tab_container.appendChild(divRow);
                        GetFieldsUI(divSectionField.id, divRow.id, result.value[i]["fedcap_customizesectionid"]);
                    }
                    var divBottom = document.createElement("div");
                    divBottom.setAttribute("class", "tablewrap");
                    var tableBottom = document.createElement("table");
                    tableBottom.setAttribute("class", "table");
                    tableBottom.setAttribute("border", "0");
                    tableBottom.setAttribute("cellspacing", "0");
                    tableBottom.setAttribute("cellpadding", "0");
                    var tbodyBottom = document.createElement("tbody");
                    var trBottom = document.createElement("tr");
                    var tdBottom1 = document.createElement("td");
                    tdBottom1.setAttribute("align", "right");
                    tdBottom1.setAttribute("valign", "middle");
                    var buttonSaveBottom = document.createElement("button");
                    buttonSaveBottom.setAttribute("name", "submit");
                    buttonSaveBottom.setAttribute("title", "Submit");
                    buttonSaveBottom.setAttribute("class", "btn btn-primary");
                    buttonSaveBottom.setAttribute("id", "btnTop" + TabId);
                    buttonSaveBottom.setAttribute("value", "save");
                    buttonSaveBottom.setAttribute("ng-click", "saveForm(OppModel)");
                    buttonSaveBottom.innerHTML = "Submit";
                    tdBottom1.appendChild(buttonSaveBottom);
                    trBottom.appendChild(tdBottom1);
                    var tdBottom2 = document.createElement("td");
                    tdBottom2.setAttribute("align", "left");
                    tdBottom2.setAttribute("valign", "middle");
                    var buttonCancelBottom = document.createElement("button");
                    buttonCancelBottom.setAttribute("name", "submit");
                    buttonCancelBottom.setAttribute("title", "Submit");
                    buttonCancelBottom.setAttribute("class", "btn btn-primary");
                    buttonCancelBottom.setAttribute("id", "btnTop" + TabId);
                    buttonCancelBottom.setAttribute("value", "save");
                    buttonCancelBottom.setAttribute("ng-click", "cancelDetails(OppModel)");
                    buttonCancelBottom.innerHTML = "Cancel";
                    tdBottom2.appendChild(buttonCancelBottom);
                    trBottom.appendChild(tdBottom2);
                    tbodyBottom.appendChild(trBottom);
                    tableBottom.appendChild(tbodyBottom);
                    divBottom.appendChild(tableBottom);
                    pl_tab_container.appendChild(divBottom);

                } catch (e) {
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    } catch (e) {
        console.log(e.message.toString());
    }
}
function GetFieldsUI(divFieldSection, SectionId, SectionGUID) {
    var rowCol = 2;
    var rowColCount = 2;
    try {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/fedcap_customizefields?$filter=_fedcap_section_value eq " + SectionGUID + " &$orderby=fedcap_order asc",
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
                XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            },
            async: false,
            success: function (data, textStatus, xhr) {
                var result = data;
                try {
                    var dvFieldSection = document.getElementById(divFieldSection);
                    dvFieldSection.innerHTML = "";
                    var divTop = document.createElement("div");
                    divTop.setAttribute("class", "tablewrap");
                    var divTop = document.createElement("div");
                    divTop.setAttribute("class", "tablewrap");
                    var tableTop = document.createElement("table");
                    tableTop.setAttribute("class", "table");
                    tableTop.setAttribute("border", "0");
                    tableTop.setAttribute("cellspacing", "0");
                    tableTop.setAttribute("cellpadding", "0");
                    var tbodyTop = document.createElement("tbody");
                    var trTop;
                    for (var i = 0; i < result.value.length; i++) {
                        if (rowColCount == rowCol) {
                            trTop = document.createElement("tr");
                            rowColCount = 0;
                        }
                        rowColCount++;
                        var tdTop1 = document.createElement("td");
                        tdTop1.setAttribute("align", "right");
                        tdTop1.setAttribute("valign", "middle");
                        tdTop1.innerHTML = result.value[i]["fedcap_name"];
                        trTop.appendChild(tdTop1);
                        var tdTop2 = document.createElement("td");
                        tdTop2.setAttribute("align", "left");
                        tdTop2.setAttribute("valign", "middle");
                        var type = result.value[i]["fedcap_fieldtype"];
                        var control = document.createElement("input");
                        if (type == "Memo") {
                            control = document.createElement("textarea");
                        }
                        else {
                        }
                        control.setAttribute("id", result.value[i]["fedcap_fieldapiname"]);
                        control.setAttribute("class", "form-control custom-form-control");
                        control.setAttribute("type", "text");
                        control.setAttribute("placeholder", "--");
                        control.setAttribute("ng-model", "OppModel." + result.value[i]["fedcap_fieldapiname"]);
                        tdTop2.appendChild(control);
                        trTop.appendChild(tdTop2);
                        tbodyTop.appendChild(trTop);
                    }
                    var reminder = result.value.length % rowCol;
                    if (reminder != 0) {
                    }
                    tableTop.appendChild(tbodyTop);
                    divTop.appendChild(tableTop);
                    dvFieldSection.appendChild(divTop);
                } catch (e) {

                }
               
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    } catch (e) {
        console.log(e.message.toString());
    }
}
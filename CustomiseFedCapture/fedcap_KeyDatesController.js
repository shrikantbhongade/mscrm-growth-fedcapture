(function () {
    "use strict";
    var app = angular.module('MyApp')
    app.controller('KeyDatesController', KeyDatesController);
    function KeyDatesController($scope) {
        var Xrm = parent.Xrm;
        var oppguid = Xrm.Page.data.entity.getId();
        var OID = oppguid.substring(1, 37);
        UserId = Xrm.Page.context.getUserId();
        init2();

        function formatUTC(passedDate) {
            var day = '';
            var month = '';
            var year = '';

            var UTCpassedDate = new Date(passedDate);
            var UTC_Date = new Date(UTCpassedDate.getUTCFullYear(), UTCpassedDate.getUTCMonth(), UTCpassedDate.getUTCDate(), UTCpassedDate.getUTCHours(), UTCpassedDate.getUTCMinutes(), UTCpassedDate.getUTCSeconds());
            var CRMDate = new Date(UTC_Date);
            day = CRMDate.getDate();
            month = CRMDate.getMonth() + 1;
            year = CRMDate.getFullYear();
            var convertedDate = year + "-" + month + "-" + day;
            return convertedDate;
        }

        function init2() {
            //getKeyData();

            $scope.dateformat = "MM-dd-yyyy";
            $scope.dateOptions = {
                startingDay: 0
            };

            $scope.showcalendar1 = function ($event) {
                $scope.showdp1 = true;
            };
            $scope.showcalendar2 = function ($event) {
                $scope.showdp2 = true;
            };
            $scope.showcalendar3 = function ($event) {
                $scope.showdp3 = true;
            };
            $scope.showcalendar4 = function ($event) {
                $scope.showdp4 = true;
            };
            $scope.showcalendar5 = function ($event) {
                $scope.showdp5 = true;
            };
            $scope.showcalendar6 = function ($event) {
                $scope.showdp6 = true;
            };
            $scope.showcalendar7 = function ($event) {
                $scope.showdp7 = true;
            };
            $scope.showcalendar8 = function ($event) {
                $scope.showdp8 = true;
            };
            $scope.showcalendar9 = function ($event) {
                $scope.showdp9 = true;
            };
            $scope.showcalendar10 = function ($event) {
                $scope.showdp10 = true;
            };
            $scope.showcalendar12 = function ($event) {
                $scope.showdp12 = true;
            };
            $scope.showcalendar13 = function ($event) {
                $scope.showdp13 = true;
            };
            $scope.showcalendar14 = function ($event) {
                $scope.showdp14 = true;
            };
            $scope.showcalendar15 = function ($event) {
                $scope.showdp15 = true;
            };
            $scope.showcalendar16 = function ($event) {
                $scope.showdp16 = true;
            };
            $scope.showcalendar17 = function ($event) {
                $scope.showdp17 = true;
            };

            $scope.showcalendar18 = function ($event) {
                $scope.showdp18 = true;
            };
            $scope.showcalendar19 = function ($event) {
                $scope.showdp19 = true;
            };
            $scope.showcalendar20 = function ($event) {
                $scope.showdp20 = true;
            };
            $scope.showcalendar21 = function ($event) {
                $scope.showdp21 = true;
            };




            $scope.showdp1 = false;
            $scope.showdp2 = false;
            $scope.showdp3 = false;
            $scope.showdp4 = false;
            $scope.showdp5 = false;
            $scope.showdp6 = false;
            $scope.showdp7 = false;
            $scope.showdp8 = false;
            $scope.showdp9 = false;
            $scope.showdp10 = false;
            $scope.showdp12 = false;
            $scope.showdp13 = false;
            $scope.showdp14 = false;
            $scope.showdp15 = false;
            $scope.showdp16 = false;
            $scope.showdp17 = false;

            $scope.showdp18 = false;
            $scope.showdp19 = false;
            $scope.showdp20 = false;
            $scope.showdp21 = false;


        }

        function GetRefreshDataForCancel() {
            var Refresing = "Refresing";

            if (OID !== "") {
                // Xrm.Page.ui.setFormNotification("Opportunity Details tabs is Refreshing.", "INFO", Refresing);
                var webResourceControl = Xrm.Page.getControl("WebResource_OpportunityTabs");
                var src = webResourceControl.getSrc();
                webResourceControl.setSrc(null);
                webResourceControl.setSrc(src);

            }
            else {
                var webResourceControl = Xrm.Page.getControl("WebResource_OpportunityTabs");
                var src = webResourceControl.getSrc();
                webResourceControl.setSrc(null);
                webResourceControl.setSrc(src);
            }
        }
        function FormatDate(passedDate) {
            var day = '';
            var month = '';
            var year = '';
            var newpassedDate = new Date(passedDate);
            year = passedDate.getFullYear().toString();
            month = (passedDate.getMonth() + 1).toString();
            day = passedDate.getDate().toString();
            if (month.length === 1) {
                month = "0" + (passedDate.getMonth() + 1).toString();
            }
            if (day.length === 1) {
                day = "0" + passedDate.getDate().toString();
            }
            var convertedDate = year + "-" + month + "-" + day;
            //var convertedDate = passedDate;
            return convertedDate;
        }

        $scope.cancelKeyDatesSave = function (data) {
            GetRefreshDataForCancel();
        };

        $scope.saveForm = function (obj) {
            if (Xrm.Page.getAttribute("name").getValue() !== null) {
                try {
                    //OpportunityName = Xrm.Page.getAttribute("name").getValue();
                    var serverUrl = location.protocol + "//" + location.host;
                    var oppguid = Xrm.Page.data.entity.getId();
                    var OID = oppguid.substring(1, 37);
                    var req = new XMLHttpRequest();
                    req.open("PATCH", serverUrl + "/api/data/v9.0/opportunities(" + OID + ")", true);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json;charset=utf-8");
                    req.setRequestHeader("OData-MaxVersion", "4.0");
                    req.setRequestHeader("OData-Version", "4.0");
                    req.onreadystatechange = function () {
                        if (this.readyState === 4) {
                            req.onreadystatechange = null;
                            if (this.status === 204) {
                                var infoNotify = "INFO";
                                Xrm.Page.ui.setFormNotification("Key Dates updated successfully.", "INFO", infoNotify);
                                setTimeout(function () { Xrm.Page.ui.clearFormNotification(infoNotify); }, 3000);
                                $scope.Mymodel = null;
                                getKeyData();
                            }
                            else {

                            }

                        }
                        else {
                            var err = this.response;
                            var err1 = JSON.stringify(err);
                            if (err !== null || err !== "") {
                                var error = JSON.parse(this.response).error;
                                //   console.log(error.message);
                            }

                        }
                    }

                    if (obj.fedcap_awarddate) {
                        var awarddate = formatUTC(obj.fedcap_awarddate);
                        obj.fedcap_awarddate = awarddate;
                    }
                    if (obj.fedcap_proposalduedate) {
                        var proposalduedate = formatUTC(obj.fedcap_proposalduedate);
                        obj.fedcap_proposalduedate = proposalduedate;
                    }
                    if (obj.fedcap_marketsurvey) {
                        var output = formatUTC(obj.fedcap_marketsurvey);
                        obj.fedcap_marketsurvey = output;
                    }
                    if (obj.fedcap_rfiduedate) {
                        var output1 = formatUTC(obj.fedcap_rfiduedate);
                        obj.fedcap_rfiduedate = output1;
                    }



                    if (obj.fedcap_rfpdate) {
                        var rfpdate = formatUTC(obj.fedcap_rfpdate);
                        obj.fedcap_rfpdate = rfpdate;
                    }
                    if (obj.fedcap_orals) {
                        var output2 = formatUTC(obj.fedcap_orals);
                        obj.fedcap_orals = output2;
                    }
                    //typeof value  !== "undefined" && value
                    if (obj.fedcap_biddersconferencedate) {
                        var output3 = formatUTC(obj.fedcap_biddersconferencedate);
                        obj.fedcap_biddersconferencedate = output3;
                    }
                    if (obj.fedcap_draftrfpdate) {
                        var output4 = formatUTC(obj.fedcap_draftrfpdate);
                        obj.fedcap_draftrfpdate = output4;
                    }
                    if (obj.fedcap_ensduedate) {
                        var output5 = formatUTC(obj.fedcap_ensduedate);
                        obj.fedcap_ensduedate = output5;
                    }
                    if (obj.fedcap_industryday) {
                        var output6 = formatUTC(obj.fedcap_industryday);
                        obj.fedcap_industryday = output6;
                    }

                    if (obj.fedcap_fprduedate) {
                        var output7 = formatUTC(obj.fedcap_fprduedate);
                        obj.fedcap_fprduedate = output7;
                    }


                    if (obj.fedcap_greenteamdate) {
                        var greenteamdate = formatUTC(obj.fedcap_greenteamdate);
                        obj.fedcap_greenteamdate = greenteamdate;
                    }

                    if (obj.fedcap_blackhatdate) {
                        var blackhatdate = formatUTC(obj.fedcap_blackhatdate);
                        obj.fedcap_blackhatdate = blackhatdate;
                    }

                    if (obj.fedcap_blueteamdate) {
                        var blueteamdate = formatUTC(obj.fedcap_blueteamdate);
                        obj.fedcap_blueteamdate = blueteamdate;
                    }

                    if (obj.fedcap_goldteamdate) {
                        var goldteamdate = formatUTC(obj.fedcap_goldteamdate);
                        obj.fedcap_goldteamdate = goldteamdate;
                    }

                    if (obj.fedcap_redteamdate) {
                        var redteamdate = formatUTC(obj.fedcap_redteamdate);
                        obj.fedcap_redteamdate = redteamdate;
                    }
                    if (obj.fedcap_pinkteamdate) {
                        var pinkteamdate = formatUTC(obj.fedcap_pinkteamdate);
                        obj.fedcap_pinkteamdate = pinkteamdate;
                    }
                    //---
                    if (obj.fedcap_milestonephase1) {
                        var milestonephase1 = formatUTC(obj.fedcap_milestonephase1);
                        obj.fedcap_milestonephase1 = milestonephase1;
                    }
                    if (obj.fedcap_milestonephase2) {
                        var milestonephase2 = formatUTC(obj.fedcap_milestonephase2);
                        obj.fedcap_milestonephase2 = milestonephase2;
                    }
                    if (obj.fedcap_milestonephase3) {
                        var milestonephase3 = formatUTC(obj.fedcap_milestonephase3);
                        obj.fedcap_milestonephase3 = milestonephase3;
                    }
                    if (obj.fedcap_milestonephase4) {
                        var milestonephase4 = formatUTC(obj.fedcap_milestonephase4);
                        obj.fedcap_milestonephase4 = milestonephase4;
                    }


                    var body = JSON.stringify(obj);
                    req.send(body);
                }

                catch (e) {
                    Xrm.Utility.alertDialog(e.message || e.description);
                }
            }
            else {

            }
        };

        $scope.ResetFunc = function () {
            window.location.reload();
        }

        function getKeyData() {
            if (Xrm.Page.getAttribute("name").getValue() !== null) {

                try {
                    var serverUrl = location.protocol + "//" + location.host;
                    var data;
                    var oppguid = Xrm.Page.data.entity.getId();
                    var OID = oppguid.substring(1, 37);
                    var req = new XMLHttpRequest();// the name of custom entity is = new_trialentity
                    req.open("GET", serverUrl + "/api/data/v9.0/opportunities(" + OID + ")?$select=fedcap_marketsurvey,fedcap_biddersconferencedate,fedcap_draftrfpdate,fedcap_ensduedate,fedcap_industryday,fedcap_fprduedate,fedcap_marketsurvey,fedcap_rfiduedate,fedcap_orals,fedcap_awarddate,fedcap_proposalduedate,fedcap_rfpdate,fedcap_greenteamdate,fedcap_blackhatdate,fedcap_blueteamdate,fedcap_goldteamdate,fedcap_redteamdate,fedcap_pinkteamdate,fedcap_milestonephase1,fedcap_milestonephase2,fedcap_milestonephase3,fedcap_milestonephase4", false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json;charset=utf-8");
                    req.setRequestHeader("OData-MaxVersion", "4.0");
                    req.setRequestHeader("OData-Version", "4.0");
                    req.onreadystatechange = function () {
                        if (this.readyState === 4 /* complete */) {
                            req.onreadystatechange = null;
                            if (this.status === 200) {
                                data = JSON.parse(this.response);
                                $scope.Mymodel = data;

                                if ($scope.Mymodel.fedcap_awarddate) {
                                    var awardDate = new Date($scope.Mymodel.fedcap_awarddate);
                                    $scope.Mymodel.fedcap_awarddate = awardDate;
                                }

                                if ($scope.Mymodel.fedcap_proposalduedate) {
                                    var propDate = new Date($scope.Mymodel.fedcap_proposalduedate);
                                    $scope.Mymodel.fedcap_proposalduedate = propDate;
                                }

                                if ($scope.Mymodel.fedcap_marketsurvey) {
                                    var useDate = new Date($scope.Mymodel.fedcap_marketsurvey);
                                    $scope.Mymodel.fedcap_marketsurvey = useDate;
                                }

                                if ($scope.Mymodel.fedcap_rfiduedate) {
                                    var useDate1 = new Date($scope.Mymodel.fedcap_rfiduedate);
                                    $scope.Mymodel.fedcap_rfiduedate = useDate1;
                                }
                                if ($scope.Mymodel.fedcap_rfpdate) {
                                    var rfpdate = new Date($scope.Mymodel.fedcap_rfpdate);
                                    $scope.Mymodel.fedcap_rfpdate = rfpdate;
                                }


                                if ($scope.Mymodel.fedcap_orals) {
                                    var useDate2 = new Date($scope.Mymodel.fedcap_orals);
                                    $scope.Mymodel.fedcap_orals = useDate2;
                                }

                                if ($scope.Mymodel.fedcap_biddersconferencedate) {
                                    var useDate3 = new Date($scope.Mymodel.fedcap_biddersconferencedate);
                                    $scope.Mymodel.fedcap_biddersconferencedate = useDate3;
                                }
                                if ($scope.Mymodel.fedcap_draftrfpdate) {
                                    var useDate4 = new Date($scope.Mymodel.fedcap_draftrfpdate);
                                    $scope.Mymodel.fedcap_draftrfpdate = useDate4;
                                }
                                if ($scope.Mymodel.fedcap_ensduedate) {
                                    var useDate5 = new Date($scope.Mymodel.fedcap_ensduedate);
                                    $scope.Mymodel.fedcap_ensduedate = useDate5;
                                }
                                if ($scope.Mymodel.fedcap_industryday) {
                                    var useDate6 = new Date($scope.Mymodel.fedcap_industryday);
                                    $scope.Mymodel.fedcap_industryday = useDate6;
                                }

                                if ($scope.Mymodel.fedcap_fprduedate) {
                                    var useDate7 = new Date($scope.Mymodel.fedcap_fprduedate);
                                    $scope.Mymodel.fedcap_fprduedate = useDate7;
                                }

                                if ($scope.Mymodel.fedcap_greenteamdate) {
                                    var _greenteamdate = new Date($scope.Mymodel.fedcap_greenteamdate);
                                    $scope.Mymodel.fedcap_greenteamdate = _greenteamdate;
                                }

                                if ($scope.Mymodel.fedcap_blackhatdate) {
                                    var _blackhatdate = new Date($scope.Mymodel.fedcap_blackhatdate);
                                    $scope.Mymodel.fedcap_blackhatdate = _blackhatdate;
                                }

                                if ($scope.Mymodel.fedcap_blueteamdate) {
                                    var _blueteamdate = new Date($scope.Mymodel.fedcap_blueteamdate);
                                    $scope.Mymodel.fedcap_blueteamdate = _blueteamdate;
                                }

                                if ($scope.Mymodel.fedcap_goldteamdate) {
                                    var _goldteamdate = new Date($scope.Mymodel.fedcap_goldteamdate);
                                    $scope.Mymodel.fedcap_goldteamdate = _goldteamdate;
                                }

                                if ($scope.Mymodel.fedcap_redteamdate) {
                                    var _redteamdate = new Date($scope.Mymodel.fedcap_redteamdate);
                                    $scope.Mymodel.fedcap_redteamdate = _redteamdate;
                                }

                                if ($scope.Mymodel.fedcap_pinkteamdate) {
                                    var _pinkteamdate = new Date($scope.Mymodel.fedcap_pinkteamdate);
                                    $scope.Mymodel.fedcap_pinkteamdate = _pinkteamdate;
                                }

                                if ($scope.Mymodel.fedcap_milestonephase1) {
                                    var _milestonephase1 = new Date($scope.Mymodel.fedcap_milestonephase1);
                                    $scope.Mymodel.fedcap_milestonephase1 = _milestonephase1;
                                }
                                if ($scope.Mymodel.fedcap_milestonephase2) {
                                    var _milestonephase2 = new Date($scope.Mymodel.fedcap_milestonephase2);
                                    $scope.Mymodel.fedcap_milestonephase2 = _milestonephase2;
                                }
                                if ($scope.Mymodel.fedcap_milestonephase3) {
                                    var _milestonephase3 = new Date($scope.Mymodel.fedcap_milestonephase3);
                                    $scope.Mymodel.fedcap_milestonephase3 = _milestonephase3;
                                }
                                if ($scope.Mymodel.fedcap_milestonephase4) {
                                    var _milestonephase4 = new Date($scope.Mymodel.fedcap_milestonephase4);
                                    $scope.Mymodel.fedcap_milestonephase4 = _milestonephase4;
                                }


                                $scope.$apply();

                            }
                            else {
                                var error = JSON.parse(this.response).error;

                            }
                        }
                    };
                    req.send();
                }
                catch (e) {

                }
            }
            else {

            }

        }

        debugger;
        $scope.getKeyDataTab = function () {
            debugger;
            if (Xrm.Page.getAttribute("name").getValue() !== null) {

                try {
                    var serverUrl = location.protocol + "//" + location.host;
                    var data;
                    var oppguid = Xrm.Page.data.entity.getId();
                    var OID = oppguid.substring(1, 37);
                    var req = new XMLHttpRequest();// the name of custom entity is = new_trialentity
                    req.open("GET", serverUrl + "/api/data/v9.0/opportunities(" + OID + ")?$select=fedcap_marketsurvey,fedcap_biddersconferencedate,fedcap_draftrfpdate,fedcap_ensduedate,fedcap_industryday,fedcap_fprduedate,fedcap_marketsurvey,fedcap_rfiduedate,fedcap_orals,fedcap_awarddate,fedcap_proposalduedate,fedcap_rfpdate,fedcap_greenteamdate,fedcap_blackhatdate,fedcap_blueteamdate,fedcap_goldteamdate,fedcap_redteamdate,fedcap_pinkteamdate,fedcap_milestonephase1,fedcap_milestonephase2,fedcap_milestonephase3,fedcap_milestonephase4", false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json;charset=utf-8");
                    req.setRequestHeader("OData-MaxVersion", "4.0");
                    req.setRequestHeader("OData-Version", "4.0");
                    req.onreadystatechange = function () {
                        if (this.readyState === 4 /* complete */) {
                            req.onreadystatechange = null;
                            if (this.status === 200) {
                               var dataKD = JSON.parse(this.response);
                               $scope.Mymodel = dataKD;

                                if ($scope.Mymodel.fedcap_awarddate) {
                                    var awardDate = new Date($scope.Mymodel.fedcap_awarddate);
                                    $scope.Mymodel.fedcap_awarddate = awardDate;
                                }

                                if ($scope.Mymodel.fedcap_proposalduedate) {
                                    var propDate = new Date($scope.Mymodel.fedcap_proposalduedate);
                                    $scope.Mymodel.fedcap_proposalduedate = propDate;
                                }

                                if ($scope.Mymodel.fedcap_marketsurvey) {
                                    var useDate = new Date($scope.Mymodel.fedcap_marketsurvey);
                                    $scope.Mymodel.fedcap_marketsurvey = useDate;
                                }

                                if ($scope.Mymodel.fedcap_rfiduedate) {
                                    var useDate1 = new Date($scope.Mymodel.fedcap_rfiduedate);
                                    $scope.Mymodel.fedcap_rfiduedate = useDate1;
                                }
                                if ($scope.Mymodel.fedcap_rfpdate) {
                                    var rfpdate = new Date($scope.Mymodel.fedcap_rfpdate);
                                    $scope.Mymodel.fedcap_rfpdate = rfpdate;
                                }


                                if ($scope.Mymodel.fedcap_orals) {
                                    var useDate2 = new Date($scope.Mymodel.fedcap_orals);
                                    $scope.Mymodel.fedcap_orals = useDate2;
                                }

                                if ($scope.Mymodel.fedcap_biddersconferencedate) {
                                    var useDate3 = new Date($scope.Mymodel.fedcap_biddersconferencedate);
                                    $scope.Mymodel.fedcap_biddersconferencedate = useDate3;
                                }
                                if ($scope.Mymodel.fedcap_draftrfpdate) {
                                    var useDate4 = new Date($scope.Mymodel.fedcap_draftrfpdate);
                                    $scope.Mymodel.fedcap_draftrfpdate = useDate4;
                                }
                                if ($scope.Mymodel.fedcap_ensduedate) {
                                    var useDate5 = new Date($scope.Mymodel.fedcap_ensduedate);
                                    $scope.Mymodel.fedcap_ensduedate = useDate5;
                                }
                                if ($scope.Mymodel.fedcap_industryday) {
                                    var useDate6 = new Date($scope.Mymodel.fedcap_industryday);
                                    $scope.Mymodel.fedcap_industryday = useDate6;
                                }

                                if ($scope.Mymodel.fedcap_fprduedate) {
                                    var useDate7 = new Date($scope.Mymodel.fedcap_fprduedate);
                                    $scope.Mymodel.fedcap_fprduedate = useDate7;
                                }

                                if ($scope.Mymodel.fedcap_greenteamdate) {
                                    var _greenteamdate = new Date($scope.Mymodel.fedcap_greenteamdate);
                                    $scope.Mymodel.fedcap_greenteamdate = _greenteamdate;
                                }

                                if ($scope.Mymodel.fedcap_blackhatdate) {
                                    var _blackhatdate = new Date($scope.Mymodel.fedcap_blackhatdate);
                                    $scope.Mymodel.fedcap_blackhatdate = _blackhatdate;
                                }

                                if ($scope.Mymodel.fedcap_blueteamdate) {
                                    var _blueteamdate = new Date($scope.Mymodel.fedcap_blueteamdate);
                                    $scope.Mymodel.fedcap_blueteamdate = _blueteamdate;
                                }

                                if ($scope.Mymodel.fedcap_goldteamdate) {
                                    var _goldteamdate = new Date($scope.Mymodel.fedcap_goldteamdate);
                                    $scope.Mymodel.fedcap_goldteamdate = _goldteamdate;
                                }

                                if ($scope.Mymodel.fedcap_redteamdate) {
                                    var _redteamdate = new Date($scope.Mymodel.fedcap_redteamdate);
                                    $scope.Mymodel.fedcap_redteamdate = _redteamdate;
                                }

                                if ($scope.Mymodel.fedcap_pinkteamdate) {
                                    var _pinkteamdate = new Date($scope.Mymodel.fedcap_pinkteamdate);
                                    $scope.Mymodel.fedcap_pinkteamdate = _pinkteamdate;
                                }

                                if ($scope.Mymodel.fedcap_milestonephase1) {
                                    var _milestonephase1 = new Date($scope.Mymodel.fedcap_milestonephase1);
                                    $scope.Mymodel.fedcap_milestonephase1 = _milestonephase1;
                                }
                                if ($scope.Mymodel.fedcap_milestonephase2) {
                                    var _milestonephase2 = new Date($scope.Mymodel.fedcap_milestonephase2);
                                    $scope.Mymodel.fedcap_milestonephase2 = _milestonephase2;
                                }
                                if ($scope.Mymodel.fedcap_milestonephase3) {
                                    var _milestonephase3 = new Date($scope.Mymodel.fedcap_milestonephase3);
                                    $scope.Mymodel.fedcap_milestonephase3 = _milestonephase3;
                                }
                                if ($scope.Mymodel.fedcap_milestonephase4) {
                                    var _milestonephase4 = new Date($scope.Mymodel.fedcap_milestonephase4);
                                    $scope.Mymodel.fedcap_milestonephase4 = _milestonephase4;
                                }


                                //$scope.$apply();

                            }
                            else {
                                var error = JSON.parse(this.response).error;

                            }
                        }
                    };
                    req.send();
                }
                catch (e) {

                }
            }
            else {

            }

        }

        function isNullOrUndefined(value) {
            return (typeof (value) === "undefined"
                        || value === null);
        }
    }

})();
"use strict";
var opendata = function () {
    var Xrm = parent.Xrm;
    try {
        var oppguid = Xrm.Page.data.entity.getId();
        var OID = oppguid.substring(1, 37);
        Xrm.Page.ui.setFormNotification("Please wait, System is retrieving GovWin data...", "WARNING", "1");
        var currentDateTime = new Date();
        var objArry = {};
        objArry.govwin_getgovwindate = currentDateTime;
        Xrm.WebApi.updateRecord("opportunity", OID, objArry).then(
                         function success(result) {
                             Xrm.WebApi.retrieveRecord("opportunity", OID, "?$select=fedcap_govwinstatus").then(
                                    function success(result) {
                                        if (result.fedcap_govwinstatus === 'Successfull') {
                                            Xrm.Utility.alertDialog("GovWin data retrieved successfully!!!");
                                            Xrm.Page.ui.clearFormNotification("1");
                                            //setTimeout(function () {
                                                
                                            //}, 2000);
                                            var windowOptions = {
                                                openInNewWindow: false
                                            };
                                            if (Xrm.Utility != null) {
                                                Xrm.Utility.openEntityForm("opportunity", OID, null, windowOptions);
                                            }
                                        }
                                        else {
                                            Xrm.Utility.alertDialog("Data Retrive Failed, may be ID doesn't exist or number of request exceeded for given time.");
                                            Xrm.Page.ui.clearFormNotification("1");
                                        }
                                    },
                                    function (error) {
                                        //console.log(error.message);
                                    }
                                );
                         },
                         function (error) {
                             Xrm.Utility.alertDialog("Error while retriving GovWin records.");
                             Xrm.Page.ui.clearFormNotification("1");
                         }
                     );
    } catch (e) {
        Xrm.Utility.alertDialog("Error while retriving GovWin records.");
        Xrm.Page.ui.clearFormNotification("1");
    }
}
﻿function DownloadAccOpp() {
    var Xrm = parent.Xrm;
    var accguid = Xrm.Page.data.entity.getId();
    var AID = accguid.substring(1, 37);
    var xmlArray = [];
    debugger;
    var fetchXml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">';
    fetchXml += '<entity name="opportunity">'
    fetchXml += '<attribute name="name" />'
    fetchXml += '<attribute name="opportunityid" />'
    fetchXml += '<attribute name="parentaccountid" />'
    fetchXml += '<attribute name="statecode" />'
    fetchXml += '<attribute name="sosi_bdexecutive" />'
    fetchXml += '<attribute name="sosi_capturelead" />'
    fetchXml += '<attribute name="sosi_proposallead" />'
    fetchXml += '<attribute name="fedcap_opportunitystatus" />'
    fetchXml += '<attribute name="fedcap_contractstartdate" />'
    fetchXml += '<attribute name="fedcap_contractenddatecalculated" />'
    fetchXml += '<attribute name="fedcap_totalperiodofperformancemonths" />'
    fetchXml += '<attribute name="fedcap_rfpdate" />'
    fetchXml += '<attribute name="fedcap_proposalduedate" />'
    fetchXml += '<attribute name="fedcap_awarddate" />'
    fetchXml += '<attribute name="fedcap_pwin" />'
    fetchXml += '<attribute name="fedcap_primesub" />'
    fetchXml += '<attribute name="fedcap_opportunitytype" />'
    fetchXml += '<attribute name="sos_businessunit" />'
    fetchXml += '<attribute name="fedcap_basevalue" />'
    fetchXml += '<attribute name="fedcap_profit_new" />'
    fetchXml += '<order attribute="name" descending="false" />'
    fetchXml += '<filter type="and">'
    fetchXml += '<condition attribute="parentaccountid" operator="in">'
    fetchXml += '<value>' + AID + '</value>';
    fetchXml += '</condition>'
    fetchXml += '<condition attribute="statecode" operator="eq" value="0" />'
    fetchXml += '</filter>'
    fetchXml += '</entity>'
    fetchXml += '</fetch>'
    xmlArray.push(fetchXml);

    var fetchXml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">';
    fetchXml += '<entity name="opportunity">'
    fetchXml += '<attribute name="name" />'
    fetchXml += '<attribute name="opportunityid" />'
    fetchXml += '<attribute name="parentaccountid" />'
    fetchXml += '<attribute name="statecode" />'
    fetchXml += '<attribute name="sosi_bdexecutive" />'
    fetchXml += '<attribute name="sosi_capturelead" />'
    fetchXml += '<attribute name="sosi_proposallead" />'
    fetchXml += '<attribute name="fedcap_opportunitystatus" />'
    fetchXml += '<attribute name="fedcap_contractstartdate" />'
    fetchXml += '<attribute name="fedcap_contractenddatecalculated" />'
    fetchXml += '<attribute name="fedcap_totalperiodofperformancemonths" />'
    fetchXml += '<attribute name="fedcap_rfpdate" />'
    fetchXml += '<attribute name="fedcap_proposalduedate" />'
    fetchXml += '<attribute name="fedcap_awarddate" />'
    fetchXml += '<attribute name="fedcap_pwin" />'
    fetchXml += '<attribute name="fedcap_primesub" />'
    fetchXml += '<attribute name="fedcap_opportunitytype" />'
    fetchXml += '<attribute name="sos_businessunit" />'
    fetchXml += '<attribute name="fedcap_basevalue" />'
    fetchXml += '<attribute name="fedcap_profit_new" />'
    fetchXml += '<order attribute="name" descending="false" />'
    fetchXml += '<filter type="and">'
    fetchXml += '<condition attribute="statecode" operator="eq" value="0" />'
    fetchXml += '</filter>'
    fetchXml += '<link-entity name="account" from="accountid" to="parentaccountid" alias="ac" link-type="inner">'
    fetchXml += '<filter type="and">'
    fetchXml += ' <condition attribute="accountid" operator="under" uiname="AIR FORCE" uitype="account" value="{' + AID + '}" />'
    //fetchXml += '<condition attribute="statuscode" operator="eq" value="1" />'
    fetchXml += '</filter>'
    fetchXml += '</link-entity>'
    fetchXml += '</entity>'
    fetchXml += '</fetch>'
    xmlArray.push(fetchXml);

    if (xmlArray.length == 0) {
        alert("Records not found.");
        return;
    }
    else {
        //var encodedFetchXml = encodeURI(fetchXml);
        var SOSiValue = 0.0;
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = month + "." + day + "." + year + "_" + hour + "." + mins;
        //creating a temporary HTML link element (they support setting file names)
        //getting data from our div that contains the HTML table
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('dvData');
        //var table_html = table_div.outerHTML.replace(/ /g, '%20');
        var today = new Date();
        var table = '<div id="dvtest">';
        table += '<table border="1" style="font-family:Segoe UI"><thead>';
        table += '<tr style="font-size:xx-small"><th style="background-color:#00B0F0;width: 500px">Opportunity Name</th>';
        table += '<th style="background-color:#00B0F0;width: 300px">Account Name</th>';
        table += '<th style="background-color:#00B0F0;">BD Executive</th>';
        table += '<th style="background-color:#00B0F0">Capture Lead</th>';
        table += '<th style="background-color:#00B0F0">Proposal Lead</th>';
        table += '<th style="background-color:#00B0F0">Opportunity Status</th>';
        table += '<th style="background-color:#00B0F0">Contract Start Date</th>';
        table += '<th style="background-color:#00B0F0">Contract End Date Calculated</th>';
        table += '<th style="background-color:#00B0F0">Total Period of Performance (Months)</th>';
        table += '<th style="background-color:#00B0F0">RFP Date</th>';
        table += '<th style="background-color:#00B0F0">Proposal Due Date</th>';
        table += '<th style="background-color:#00B0F0">Award Date</th>';
        table += '<th style="background-color:#00B0F0">Pwin</th>';
        table += '<th style="background-color:#00B0F0">SOSi Role</th>';
        table += '<th style="background-color:#00B0F0">Opportunity Type (FedCap)</th>';
        table += '<th style="background-color:#00B0F0">Business Unit</th>';
        table += '<th style="background-color:#00B0F0">SOSi Value</th>';
        table += '<th style="background-color:#00B0F0">Profit (Decimal)</th>';
        table += "</tr>";
        table += "<tbody>";

        for (var xml = 0; xml < xmlArray.length; xml++) {
            GetOppList();
            function GetOppList() {
                var encodedFetchXml = encodeURI(xmlArray[xml]);
                var URL = Xrm.Page.context.getClientUrl() + "/api/data/v9.0/opportunities?fetchXml=" + encodedFetchXml;
                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    url: Xrm.Page.context.getClientUrl() + "/api/data/v9.0/opportunities?fetchXml=" + encodedFetchXml,
                    beforeSend: function (XMLHttpRequest) {
                        XMLHttpRequest.setRequestHeader("OData-MaxVersion", "4.0");
                        XMLHttpRequest.setRequestHeader("OData-Version", "4.0");
                        XMLHttpRequest.setRequestHeader("Accept", "application/json");
                        XMLHttpRequest.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
                    },
                    async: false,
                    success: function (data, textStatus, xhr) {
                        var result = data;
                        //getting values of current time for generating the file name
                        for (var j = 0; j < result.value.length; j++) {
                            try {
                                var name = (result.value[j]["name"] == undefined) ? "" : result.value[j]["name"];
                                var parentaccountid_value = (result.value[j]["_parentaccountid_value@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["_parentaccountid_value@OData.Community.Display.V1.FormattedValue"];
                                //var statecode = (result.value[j]["statecode@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["statecode@OData.Community.Display.V1.FormattedValue"];
                                var sosi_bdexecutive = (result.value[j]["_sosi_bdexecutive_value@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["_sosi_bdexecutive_value@OData.Community.Display.V1.FormattedValue"];
                                var sosi_capturelead = (result.value[j]["_sosi_capturelead_value@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["_sosi_capturelead_value@OData.Community.Display.V1.FormattedValue"];
                                var sosi_proposallead = (result.value[j]["_sosi_proposallead_value@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["_sosi_proposallead_value@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_opportunitystatus = (result.value[j]["fedcap_opportunitystatus@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_opportunitystatus@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_contractstartdate = (result.value[j]["fedcap_contractstartdate@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_contractstartdate@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_contractenddatecalculated = (result.value[j]["fedcap_contractenddatecalculated@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_contractenddatecalculated@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_totalperiodofperformancemonths = (result.value[j]["fedcap_totalperiodofperformancemonths@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_totalperiodofperformancemonths@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_rfpdate = (result.value[j]["fedcap_rfpdate@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_rfpdate@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_proposalduedate = (result.value[j]["fedcap_proposalduedate@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_proposalduedate@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_awarddate = (result.value[j]["fedcap_awarddate@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_awarddate@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_pwin = (result.value[j]["fedcap_pwin@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_pwin@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_primesubSOSiRole = (result.value[j]["fedcap_primesub@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_primesub@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_opportunitytype = (result.value[j]["fedcap_opportunitytype@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_opportunitytype@OData.Community.Display.V1.FormattedValue"];
                                var sos_businessunit = (result.value[j]["sos_businessunit@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["sos_businessunit@OData.Community.Display.V1.FormattedValue"];
                                var fedcap_basevalueSOSiValue = (result.value[j]["fedcap_basevalue@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_basevalue@OData.Community.Display.V1.FormattedValue"];
                                var SOSi_Value = (result.value[j]["fedcap_basevalue"]);
                                if (result.value[j]["fedcap_basevalue@OData.Community.Display.V1.FormattedValue"] !== undefined) {
                                    SOSiValue += parseFloat(SOSi_Value);
                                }
                                var fedcap_profit_new = (result.value[j]["fedcap_profit_new@OData.Community.Display.V1.FormattedValue"] == undefined) ? "" : result.value[j]["fedcap_profit_new@OData.Community.Display.V1.FormattedValue"];
                                
                                table += "<tr>";
                                table += "<td style='font-size:xx-small;'>" + name + "</td>";
                                table += "<td style='font-size:xx-small;'>" + parentaccountid_value + "</td>";
                                table += "<td style='font-size:xx-small;'>" + sosi_bdexecutive + "</td>";
                                table += "<td style='font-size:xx-small;'>" + sosi_capturelead + "</td>";
                                table += "<td style='font-size:xx-small;'>" + sosi_proposallead + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_opportunitystatus + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_contractstartdate + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_contractenddatecalculated + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_totalperiodofperformancemonths + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_rfpdate + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_proposalduedate + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_awarddate + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_pwin + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_primesubSOSiRole + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_opportunitytype + "</td>";
                                table += "<td style='font-size:xx-small;'>" + sos_businessunit + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_basevalueSOSiValue + "</td>";
                                table += "<td style='font-size:xx-small;'>" + fedcap_profit_new + "</td>";
                                table += "</tr>";

                            } catch (e) {

                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                    }
                });
            }
        }
        debugger;
        var SOSiValueStr = '';
        try {
             SOSiValueStr = "$ " + (SOSiValue.toString().toLocaleString('en-US'));
        } catch (e) {
        }
        table += "<tr>";
        table += "<td><b>Total</b></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "<td><b>" + SOSiValueStr + "</b></td>";
        table += "<td style='font-size:xx-small;'></td>";
        table += "</tr>";

        table += "</tbody>";
        table += "</table>";
        table += "</div>";
        //Xrm.Page.ui.clearFormNotification();
        //var table_html = table_div.outerHTML.replace(/ /g, '%20');
        var table_html = table.replace(/ /g, '%20');
        if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) //IF IE > 10
        {
            var table1 = table;
            table1 = table1.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            table1 = table1.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            table1 = table1.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            if (document.execCommand) {
                try {
                    var oWin = window.open("about:blank", "_blank");
                    //var oWin = window.open("", "test", "resizable=no,scrollbars=yes,width=260,height=225");
                    oWin.document.write(table1);
                    oWin.document.close();
                    var success = oWin.document.execCommand('SaveAs', false, "Opportunity.xls")
                    oWin.close();
                }
                catch (err) {
                    alert(err.message());
                }
            }
            else {
                alert('Error while downloading!!!');
            }
        }
        else if ((navigator.userAgent.indexOf("Edge") != -1)) //IF IE > 10
        {
            var table1 = table;
            table1 = table1.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            table1 = table1.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            table1 = table1.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
            var blob = new Blob([table1], { type: 'data:application/vnd.ms-excel' });
            window.navigator.msSaveBlob(blob, 'AccountOpportunity_' + postfix + '.xls');
        }
        else if (navigator.userAgent.indexOf("Firefox") != -1) {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Opportunities</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>' + table + '</body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            var ctx = { worksheet: "test" || 'Worksheet', "dvtest": table }
            //window.location.href = uri + base64(format(template, ctx))
            let link = document.createElement('a');
            link.id = "AccountOpportunity";
            link.style.display = "none"; // because Firefox sux
            document.body.appendChild(link); // because Firefox sux
            link.href = uri + base64(format(template, ctx))
            link.download = 'AccountOpportunity_' + postfix + '.xls';
            link.click();
            document.body.removeChild(link); // because Firefox sux
        }
        else {
            //var a = document.createElement('a');
            //a.href = data_type + ', ' + table_html;
            ////setting the file name
            //a.download = 'AccountOpportunity_' + postfix + '.xls';
            ////triggering the function
            //document.body.appendChild(a);
            //a.click();
            ////just in case, prevent default behaviour
            //e.preventDefault();

            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Opportunities</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>' + table + '</body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            var ctx = { worksheet: "test" || 'Worksheet', "dvtest": table }
            //window.location.href = uri + base64(format(template, ctx))
            let link = document.createElement('a');
            link.id = "AccountOpportunity";
            link.style.display = "none"; // because Firefox sux
            document.body.appendChild(link); // because Firefox sux
            link.href = uri + base64(format(template, ctx))
            link.download = 'AccountOpportunity_' + postfix + '.xls';
            link.click();
            document.body.removeChild(link); // because Firefox sux
        }
    }
}

function FormatDate(passedDate) {
    var day = '';
    var month = '';
    var year = '';
    year = passedDate.getFullYear().toString();
    month = (passedDate.getMonth() + 1).toString();
    day = passedDate.getDate().toString();
    if (month.length == 1) {
        month = "0" + (passedDate.getMonth() + 1).toString();
    }
    if (day.length == 1) {
        day = "0" + passedDate.getDate().toString();
    }
    var convertedDate = year + "-" + month + "-" + day;
    return convertedDate;
}
function FormatDateUTC(passedDate) {
    var day = '';
    var month = '';
    var year = '';
    year = passedDate.getFullYear().toString();
    month = (passedDate.getMonth() + 1).toString();
    day = passedDate.getDate().toString();
    if (month.length == 1) {
        month = "0" + (passedDate.getMonth() + 1).toString();
    }
    if (day.length == 1) {
        day = "0" + passedDate.getDate().toString();
    }
    var convertedDate = new Date(passedDate.getUTCFullYear(), passedDate.getUTCMonth(), passedDate.getUTCDate());// year + "-" + month + "-" + day;
    return convertedDate;
}
function FormatCurrency(CurrencyField) {

    if (CurrencyField == null) {
        return "";
    }
    if (CurrencyField == '0.00') {
        return "";
    }
    return "$" + CurrencyField.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}



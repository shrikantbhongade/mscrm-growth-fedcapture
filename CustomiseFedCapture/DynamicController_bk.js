﻿(function () {
    "use strict";
    var app = angular.module('MyApp', []);
    console.log("Into MyAppDynamic");
    app.controller('DynamicController', DynamicController);
    function DynamicController($scope) {
        var Xrm = parent.Xrm;
        var oppguid = Xrm.Page.data.entity.getId();
        var OID = oppguid.substring(1, 37);
        UserId = Xrm.Page.context.getUserId();
        GetCurrency();
        GetTabData();
        function GetTabData() {
            console.log("Into GetTabData");
            var serverUrl = location.protocol + "//" + location.host;
            var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
            req.open("GET", serverUrl + "/api/data/v9.0/fedcap_customizetabs?$orderby=fedcap_order asc", false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("Prefer", "odata.include-annotations=*");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                if (this.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (this.status === 200) {
                        var data = JSON.parse(this.response);
                        $scope.tabs = data.value;
                        //for (var i = 0; i < data.value.length; i++) {
                        //    GetSectionData(data.value[i]["fedcap_customizetabid"], i);
                        //}
                        GetSectionData(data.value[0]["fedcap_customizetabid"], 0);
                        $scope.tabs[0].LoadTabData = true;
                        //$scope.tabs[0].fedcap_backgroundcoloractive = $scope.tabs[0]["fedcap_backgroundcolor"];
                        for (var tab = 0; tab < $scope.tabs.length; tab++) {
                            if (tab === 0) {
                                $scope.tabs[tab].fedcap_backgroundcoloractive = "tabLink ms-crm-PanelHeader-Color active";
                                $scope.tabs[tab].tabcolor = { "background-color": $scope.tabs[tab]["fedcap_backgroundcolor"] };
                            }
                            else {
                                $scope.tabs[tab].fedcap_backgroundcoloractive = 'tabLink ms-crm-PanelHeader-Color';
                                $scope.tabs[tab].tabcolor = { "background-color": '' };
                            }
                        }
                        //$scope.tabs[0].fedcap_backgroundcoloractive = "tabLink ms-crm-PanelHeader-Color active";
                        //$scope.tabs[0].tabcolor = { "background-color": $scope.tabs[0]["fedcap_backgroundcolor"] };
                        var height = (parseInt(data.value[0]["fedcap_height"]) + 5);
                        var height1 = parseInt(data.value[0]["fedcap_height"]);
                        $('#fedcap_div', window.parent.document).height(height);
                        $('#fedcap_div1', window.parent.document).height(height1);
                    }
                    else {
                        $('#fedcap_div', window.parent.document).height(550);
                        $('#fedcap_div1', window.parent.document).height(500);
                        //var error = JSON.parse(this.response).error;
                        //Xrm.Utility.alertDialog(error.message);
                    }
                }
            };
            req.send();

        }
        function GetSectionData(tabGUID, tab) {
            var serverUrl = location.protocol + "//" + location.host;
            var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
            req.open("GET", serverUrl + "/api/data/v9.0/fedcap_customizesections?$filter=_fedcap_tab_value eq " + tabGUID + " &$orderby=fedcap_order asc", false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("Prefer", "odata.include-annotations=*");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                if (this.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (this.status === 200) {
                        var data = JSON.parse(this.response);
                        $scope.tabs[tab].tabAllFields = '';
                        $scope.tabs[tab].sections = data.value;
                        debugger;
                        for (var i = 0; i < data.value.length; i++) {
                            $scope.tabs[tab].sections[i].controlwidth = { "width": $scope.tabs[tab].sections[i]["fedcap_controlwidth"] + "px" };

                            if (data.value[i]["fedcap_name"] === 'PWin') {
                                GetPWinOpportunityGivenQuestionAnswer(tab,i);
                                GetOptionSetLabelValue(tab,i,"fedcap_pwinquestion", "fedcap_stage");
                            }
                            if (data.value[i]["fedcap_name"] === 'PGo') {
                                GetPGoOpportunityGivenQuestionAnswer(tab, i);
                                GetPGoQuestion(tab, i);
                            }
                            GetFieldData(data.value[i]["fedcap_customizesectionid"], tab, i);
                            if (data.value[i]["fedcap_name"] === 'GovWin and FBO') {
                                //$scope.$apply();
                                //debugger;
                                //CallGovWin();
                                //var iFrameObj = $('#GovWinFBO').attr('id');
                                //var id = $('#GovWinFBO').attr('src');
                                //$('#GovWinFBO').attr('src', $('#GovWinFBO').attr('src'));

                            }
                        }
                    }
                    else {
                        //var error = JSON.parse(this.response).error;
                        //Xrm.Utility.alertDialog(error.message);
                    }
                }
            };
            req.send();
        }
        function GetFieldData(sectionGUID, tab, section) {
            console.log("Into GetFieldData");
            var serverUrl = location.protocol + "//" + location.host;
            var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
            req.open("GET", serverUrl + "/api/data/v9.0/fedcap_customizefields?$filter=_fedcap_section_value eq " + sectionGUID + " &$orderby=fedcap_order asc", false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("Prefer", "odata.include-annotations=*");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                if (this.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (this.status === 200) {
                        var data = JSON.parse(this.response);
                        $scope.tabs[tab].sections[section].fields = data.value;
                        LoadOptionSetAndGetTabFieldsInQuery(tab, section);
                        if (data.value.length > 0) {
                            //if (tab == 0) {
                            //    GetFieldOpportunityData(sectionGUID, tab, section);
                            //}
                            GetFieldOpportunityData(sectionGUID, tab, section);
                        }
                        else {
                            $scope.tabs[tab].sections[section].display = "none";// if fields not available
                        }
                    }
                    else {
                    }
                }
            };
            req.send();
        }
        function LoadOptionSetAndGetTabFieldsInQuery(tab, section) {
            //*******************************************************************************************
            var selectQry = '';
            var selectQryforMultiPickList = '';
            var isMultiPickListExist = false;
            var fieldIds = [];
            for (var i = 0; i < $scope.tabs[tab].sections[section].fields.length; i++) {
                if ($scope.tabs[tab].sections[section].fields[i]['fedcap_fieldtype'] === 'Picklist') {
                    GetOptionSetLabel(tab, section, i, "opportunity", $scope.tabs[tab].sections[section].fields[i]['fedcap_fieldapiname']);
                }
                if ($scope.tabs[tab].sections[section].fields[i]['fedcap_fieldtype'] === 'MultiSelectPicklistType') {
                    isMultiPickListExist = true;
                    fieldIds.push({ 'id': i });
                    if (selectQryforMultiPickList === '') {
                        selectQryforMultiPickList = $scope.tabs[tab].sections[section].fields[i]['fedcap_fieldapiname'];
                    }
                    else {
                        selectQryforMultiPickList = selectQryforMultiPickList + ',' + $scope.tabs[tab].sections[section].fields[i]['fedcap_fieldapiname'];
                    }

                    GetOptionSetLabelMulti(tab, section, i, "opportunity", $scope.tabs[tab].sections[section].fields[i]['fedcap_fieldapiname']);
                }
                if (selectQry === '') {
                    selectQry = $scope.tabs[tab].sections[section].fields[i]['fedcap_fieldapiname'];
                }
                else {
                    selectQry = selectQry + ',' + $scope.tabs[tab].sections[section].fields[i]['fedcap_fieldapiname'];
                }
            }
            if (selectQry !== '') {
                $scope.tabs[tab].tabAllFields = $scope.tabs[tab].tabAllFields + ',' + selectQry;
            }
            //************************************************************************************************
        }
        function setCharAt(str,index,chr) {
            if(index > str.length-1) return str;
            return str.substr(0,index) + chr + str.substr(index+1);
        }
        function FormatDate(passedDate) {
            var day = '';
            var month = '';
            var year = '';
            year = passedDate.getFullYear().toString();
            month = (passedDate.getMonth() + 1).toString();
            day = passedDate.getDate().toString();
            if (month.length === 1) {
                month = "0" + (passedDate.getMonth() + 1).toString();
            }
            if (day.length === 1) {
                day = "0" + passedDate.getDate().toString();
            }
            //var convertedDate = year + "-" + month + "-" + day;
            var convertedDate = month + "/" + day + "/" + year;
            return convertedDate;
        }
        function GetFieldOpportunityData(sectionGUID, tab, section) {
            var selectQry = $scope.tabs[tab].tabAllFields;
            var isMultiPickListExist = false;
            var fieldsArr = selectQry.split(',');
            if (fieldsArr[0] === '') {
                selectQry = setCharAt(selectQry, 0, "");
            }
            for (var i = 0; i < $scope.tabs[tab].sections[section].fields.length; i++) {
                if ($scope.tabs[tab].sections[section].fields[i]['fedcap_fieldtype'] === 'MultiSelectPicklistType') {
                    isMultiPickListExist = true;
                }
            }
            var serverUrl = location.protocol + "//" + location.host;
            var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
            req.open("GET", serverUrl + "/api/data/v9.0/opportunities(" + OID + ")?$select=" + selectQry, false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("Prefer", "odata.include-annotations=*");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                if (this.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (this.status === 200) {
                        var data = JSON.parse(this.response);
                        $scope.tabs[tab].sections[section].fieldValue = data;
                        for (var i = 0; i < $scope.tabs[tab].sections[section].fields.length; i++) {
                            if ($scope.tabs[tab].sections[section].fields[i]["fedcap_fieldtype"] == 'DateTime') {
                                var fieldApiName = $scope.tabs[tab].sections[section].fields[i]["fedcap_fieldapiname"];
                                if ($scope.tabs[tab].sections[section].fieldValue[fieldApiName] != null) {
                                    var dateFormat = (new Date($scope.tabs[tab].sections[section].fieldValue[fieldApiName]));
                                    var formatedValue = fieldApiName + "@OData.Community.Display.V1.FormattedValue";
                                    $scope.tabs[tab].sections[section].fieldValue[formatedValue] = localDateToUTC(dateFormat);
                                }
                            }
                        }
                        $scope.$apply();
                        if (isMultiPickListExist) {
                            MultiPickListBind(selectQryforMultiPickList, tab, section, fieldIds);
                        }
                    }
                    else {
                        //var error = JSON.parse(this.response).error;
                        //Xrm.Utility.alertDialog(error.message);
                    }
                }
            };
            req.send();
        }
        function BindCalenderIE(sectionId) {
            var datefield = document.createElement("input");
            datefield.id = sectionId;
            datefield.setAttribute("type", "date");
            if (datefield.type !== "date") { //if browser doesn't support input type="date", initialize date picker widget:
                jQuery(function ($) { //on
                    $('input[type=date]').datepicker({ dateFormat: 'yy-mm-dd' }).val();
                    $('input[type=date]').attr('placeholder', 'yyyy-mm-dd');
                })
            }
        }
        function localDateToUTC(localDate) {
            return new Date(localDate.getUTCFullYear(), localDate.getUTCMonth(), localDate.getUTCDate(),localDate.getUTCHours(), localDate.getUTCMinutes(), localDate.getUTCSeconds());
        }
        function MultiPickListBind(selectQry, tab, section, fieldIds) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/opportunities(" + OID + ")?$select=" + selectQry, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            var data = JSON.parse(this.response);
                            for (var field = 0; field < fieldIds.length; field++) {
                                var value = fieldIds[field];
                                $scope.tabs[tab].sections[section].fields[value.id].MultiPickList = [];
                                var retrivedFields = selectQry.split(',');
                                for (var retrivedField = 0; retrivedField < retrivedFields.length; retrivedField++) {
                                    var retrivedFieldName = retrivedFields[retrivedField];
                                    var dataValue = data[retrivedFieldName].split(',');
                                    var dataText = data[retrivedFieldName + "@OData.Community.Display.V1.FormattedValue"].split('; ');
                                    for (var i = 0; i < dataValue.length; i++) {
                                        $scope.tabs[tab].sections[section].fields[value.id].MultiPickList.push({ "id": dataValue[i], "value": dataText[i] })
                                        $scope.tabs[tab].sections[section].fields[value.id].PickList = removeByKey($scope.tabs[tab].sections[section].fields[value.id].PickList, { key: 'id', value: parseInt(dataValue[i]) });
                                        function removeByKey(array, params) {
                                            array.some(function (item, index) {
                                                if (array[index][params.key] === params.value) {
                                                    // found it!
                                                    array.splice(index, 1);
                                                }
                                            });
                                            return array;
                                        }
                                    }
                                    $scope.$apply();
                                }
                            }
                        }
                        else {
                            //var error = JSON.parse(this.response).error;
                            //Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();

            } catch (e) {

            }
        }
        function GetOptionSetLabel(tab, section, field, EntityLogicalName, AttributeLogicalName) {
            SDK.Metadata.RetrieveAttribute(EntityLogicalName, AttributeLogicalName, "00000000-0000-0000-0000-000000000000", true,
                function (result) {
                    $scope.tabs[tab].sections[section].fields[field].PickList = [];
                    var optionSet = result.OptionSet.Options;
                    for (var i = 0; i < result.OptionSet.Options.length; i++) {
                        $scope.tabs[tab].sections[section].fields[field].PickList.push({ "id": result.OptionSet.Options[i].Value, "value": result.OptionSet.Options[i].Label.LocalizedLabels[0].Label })
                    }
                    $scope.$apply();
                },
                function (error) { }
            );
        }
        function GetOptionSetLabelMulti(tab, section, field, EntityLogicalName, AttributeLogicalName) {
            try {
                var multiOptionSet = Xrm.Page.getAttribute(AttributeLogicalName).getOptions();
                $scope.tabs[tab].sections[section].fields[field].PickList = [];
                for (var i = 0; i < multiOptionSet.length; i++) {
                    if (multiOptionSet[i].value != "-1") {
                        $scope.tabs[tab].sections[section].fields[field].PickList.push({ "id": multiOptionSet[i].value, "value": multiOptionSet[i].text })
                    }
                }
                $scope.$apply();
            } catch (e) {

            }
        }
        $scope.LinkChanged = function (height) {
            var heightConverted = parseInt(height);
            $('#fedcap_div', window.parent.document).height(heightConverted);
            $('#fedcap_div1', window.parent.document).height(heightConverted);
        }
        $scope.OnOptionSetCallACT = function (fromAttribute) {
            var toAttribute = 'Chosen_' + fromAttribute;
            var fromAttribute = document.getElementById(fromAttribute);
            var selectedText = fromAttribute.options[fromAttribute.selectedIndex].text;
            //var selectedValue = document.getElementById(fromAttribute).value;
            var selectedValue = fromAttribute.options[fromAttribute.selectedIndex].value;

            var toAttribute = document.getElementById(toAttribute);
            var opt = document.createElement('option');
            opt.innerHTML = selectedText;
            opt.value = selectedValue;
            toAttribute.appendChild(opt);
            for (var i = 0; i < fromAttribute.length; i++) {
                if (fromAttribute.options[i].value === selectedValue) {
                    fromAttribute.remove(i);
                }
            }
        }
        $scope.OnOptionSetCallACTRev = function (toAttribute) {
            var fromAttribute = toAttribute;
            var toAttribute = 'Chosen_' + toAttribute;
            var toAttribute = document.getElementById(toAttribute);
            var selectedText = toAttribute.options[toAttribute.selectedIndex].text;
            //var selectedValue = document.getElementById(toAttribute).value;
            var selectedValue = toAttribute.options[toAttribute.selectedIndex].value;

            var fromAttribute = document.getElementById(fromAttribute);
            var opt = document.createElement('option');
            opt.innerHTML = selectedText;
            opt.value = selectedValue;
            fromAttribute.appendChild(opt);
            for (var i = 0; i < toAttribute.length; i++) {
                if (toAttribute.options[i].value === selectedValue) {
                    toAttribute.remove(i);
                }
            }
        }
        var controlId, control_Id;
        $scope.OpenLookup = function (entityName, cntrlId) {
            controlId = cntrlId + "_id";
            control_Id = cntrlId;
            var lookupObjectParams = {};
            lookupObjectParams.entityTypes = [entityName];
            lookupObjectParams.defaultEntityType = entityName;
            //lookupObjectParams.defaultViewId = "3B24E1B8-438C-4B49-8A68-8748D5291688";
            lookupObjectParams.allowMultiSelect = false;
            Xrm.Utility.lookupObjects(lookupObjectParams).then(CallbackFunctionLookup, LookupCancelCallback)
        }
        function CallbackFunctionLookup(selectedItems) {

            if (selectedItems != null && selectedItems.length > 0) {
                var lookup = [{ id: selectedItems[0].id, typename: selectedItems[0].typename, name: selectedItems[0].name }];
                $("#" + control_Id).val(lookup[0].name);
                $("#" + controlId).val(lookup[0].id.substring(1, 37));
            }
        }
        function LookupCancelCallback() { }
        var currencySymbol = '';
        $scope.saveForm = function (cntrlId, selectControls, tabName) {
            debugger;
            var currencySymbol = $scope.currencysymbol;
            var btnId = tabName;
            var entityData = {};
            var entityName = '';
            var inValid = false;
            var errormsg = "";
            debugger;
            if (OID === '') {
                $("#dvNotificationText").text("Please wait while Opportunity is creating..!!!");
            }
            else {
                $("#dvNotificationText").text("Please wait while " + tabName + " is updating..!!!");
            }
            $("#dvNotification").toggleClass('warning').removeClass('error').removeClass('info');
            try {
                if (tabName === 'PWin') {
                    var PhaseObj = new Object();
                    for (var i = 0; i < $scope.pwintabs.length; i++) {
                        for (var que = 0; que < $scope.pwintabs[i].questions.length; que++) {
                            var questionId = $scope.pwintabs[i].questions[que]["fedcap_pwinquestionid"];
                            var questionWt = $scope.pwintabs[i].questions[que]["fedcap_weighting"];
                            var _questionId = "ans_" + questionId;
                            var questionName = $scope.pwintabs[i].questions[que]["fedcap_name"]
                            var AnswerId = $('input[name="' + _questionId + '"]:checked').attr('id');
                            var AnswerWt = $('input[name="' + _questionId + '"]:checked').attr('value');
                            PhaseObj["fedcap_OpportunityID@odata.bind"] = "/fedcap_opportunitypwinanswers(" + OID + ")";
                            PhaseObj["fedcap_name"] = questionName;
                            PhaseObj["fedcap_pwinanswerweighting"] = AnswerWt;
                            
                            PhaseObj["fedcap_PWinGivenAnswer@odata.bind"] = "/fedcap_opportunitypwinanswers(" + AnswerId + ")";
                            PhaseObj["fedcap_PWinQuestion@odata.bind"] = "/fedcap_opportunitypwinanswers(" + questionId + ")";
                            entityName = 'fedcap_opportunitypwinanswers'
                            var serverUrl = location.protocol + "//" + location.host;
                            var req = new XMLHttpRequest();
                            req.open("PATCH", serverUrl + "/api/data/v9.0/fedcap_opportunitypwinanswers(" + questionId + ")", true);
                            req.setRequestHeader("Accept", "application/json");
                            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                            req.setRequestHeader("Prefer", "odata.include-annotations=*");
                            req.setRequestHeader("OData-MaxVersion", "4.0");
                            req.setRequestHeader("OData-Version", "4.0");
                            req.onreadystatechange = function () {
                                if (this.readyState === 4) {
                                    req.onreadystatechange = null;
                                    if (this.status === 204) {
                                    }
                                    else if (this.status === 201) {
                                    }
                                    else {
                                    }
                                }
                                else {

                                    var err = this.response;
                                    var err1 = JSON.stringify(err);
                                    if (err !== null || err || err !== "") {
                                        var error = JSON.parse(this.response).error;
                                    }
                                }
                            }
                            var body = JSON.stringify(PhaseObj);
                            req.send(body);
                        }
                    }
                }
                else if (tabName === 'PGo') {
                    debugger;
                    var PhaseObj = new Object();
                    for (var tab = 0; tab < $scope.tabs.length; tab++) {
                        for (var sec = 0; sec < $scope.tabs[tab].sections.length; sec++) {
                            if ($scope.tabs[tab].sections[sec]["fedcap_name"] === 'PGo') {
                                for (var que = 0; que < $scope.tabs[tab].sections[sec].pgoquestions.length; que++) {
                                    var questionId = $scope.tabs[tab].sections[sec].pgoquestions[que]["fedcap_pgoquestionid"];
                                    var questionWt = $scope.tabs[tab].sections[sec].pgoquestions[que]["fedcap_weighting"];
                                    var _questionId = "ans_" + questionId;
                                    var questionName = $scope.tabs[tab].sections[sec].pgoquestions[que]["fedcap_name"]
                                    var AnswerId = $('input[name="' + _questionId + '"]:checked').attr('id');
                                    var AnswerWt = $('input[name="' + _questionId + '"]:checked').attr('value');
                                    PhaseObj["fedcap_OpportunityID@odata.bind"] = "/fedcap_opportunitypgoanswers(" + OID + ")";
                                    PhaseObj["fedcap_name"] = questionName;
                                    PhaseObj["fedcap_pgoanswerweighting"] = AnswerWt;
                                    PhaseObj["fedcap_PGoGivenAnswer@odata.bind"] = "/fedcap_opportunitypgoanswers(" + AnswerId + ")";
                                    PhaseObj["fedcap_PGoQuestion@odata.bind"] = "/fedcap_opportunitypgoanswers(" + questionId + ")";
                                    var serverUrl = location.protocol + "//" + location.host;
                                    var req = new XMLHttpRequest();
                                    req.open("PATCH", serverUrl + "/api/data/v9.0/fedcap_opportunitypgoanswers(" + questionId + ")", true);
                                    req.setRequestHeader("Accept", "application/json");
                                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                                    req.setRequestHeader("Prefer", "odata.include-annotations=*");
                                    req.setRequestHeader("OData-MaxVersion", "4.0");
                                    req.setRequestHeader("OData-Version", "4.0");
                                    req.onreadystatechange = function () {
                                        if (this.readyState === 4) {
                                            req.onreadystatechange = null;
                                            if (this.status === 204) {
                                            }
                                            else if (this.status === 201) {
                                            }
                                            else {
                                            }
                                        }
                                        else {

                                            //var err = this.response;
                                            //var err1 = JSON.stringify(err);
                                            //if (err !== null || err || err !== "") {
                                            //    var error = JSON.parse(this.response).error;
                                            //}
                                        }
                                    }
                                    var body = JSON.stringify(PhaseObj);
                                    req.send(body);
                                }
                            }
                        }
                    }
                }
                else {
                }
                entityName = 'opportunity'
                var tabFields = selectControls.split(',');
                var isrequired = [];
                for (var i = 1; i <= tabFields.length; i++) {
                    var isReadOnly = $("#" + tabFields[i] + "").attr('ng-readonly');
                    if (isReadOnly === "true") {
                        
                    }
                    else {
                        var fieldtype = $("#" + tabFields[i] + "").attr('data-fieldtype');
                        var _isrequired = $("#" + tabFields[i] + "").data('isrequired');
                        if (_isrequired) {
                            if (fieldtype === 'Boolean') {
                                if ($("#" + tabFields[i] + "").prop("checked") !== true) {
                                    $("#" + tabFields[i] + "").css({ "border": "1px solid #DC1616" });
                                    isrequired.push({ "id": tabFields[i], "ValidationMsg": $("#" + tabFields[i] + "").data('label') });
                                }
                                else {
                                    $("#" + tabFields[i] + "").css({ "border": "1px solid #f8f8f8" });
                                }
                            }
                            if ($("#" + tabFields[i] + "").val() === '') {
                                isrequired.push({ "id": tabFields[i], "ValidationMsg": $("#" + tabFields[i] + "").data('label') });
                                $("#" + tabFields[i] + "").css({ "border": "1px solid #DC1616" });
                            }
                            else {
                                $("#" + tabFields[i] + "").css({ "border": "1px solid #f8f8f8" });
                            }
                        }
                        var value = $("#" + tabFields[i] + "").val();
                        var id = $("#" + tabFields[i] + "").attr('id');
                        if (fieldtype === 'String' || fieldtype === 'Memo') {
                            entityData[id] = value;
                        }
                        if (fieldtype === 'DateTime') {
                            if (value != '') {
                                entityData[id] = value;
                            }
                            else {
                                entityData[id] = null;
                            }
                        }
                        if (fieldtype === 'Integer' || fieldtype === 'BigInt') {
                            value = parseInt(value.replace('$', '').replace(/,/g, '').replace(currencySymbol,''));
                            entityData[id] = value;
                        }
                        if (fieldtype === 'Money' || fieldtype === 'Decimal') {
                            value = parseFloat(value.replace('$', '').replace(/,/g, '').replace(currencySymbol, ''));
                            entityData[id] = value;
                        }
                        if (fieldtype === 'Picklist') {
                            var pickListId = value.replace('number:', '').replace('string:', '').replace(/,/g, '');
                            if (value === "") {
                                entityData[id] = null;
                            }
                            else {
                                entityData[id] = pickListId;
                            }
                        }
                        if (fieldtype === 'MultiSelectPicklistType') {
                            var multiSelectPicklistId = "Chosen_" + tabFields[i];
                            var multiSelectPicklist = document.getElementById(multiSelectPicklistId);
                            var multiSelectPicklistSelectedValues = '';
                            for (var selected = 0; selected < multiSelectPicklist.length; selected++) {
                                var selectedCheck = selected + 1;
                                if (selectedCheck == multiSelectPicklist.length) {
                                    multiSelectPicklistSelectedValues += multiSelectPicklist[selected].value.replace('number:', '').replace('string:', '').replace(/,/g, '');
                                }
                                else {
                                    multiSelectPicklistSelectedValues += multiSelectPicklist[selected].value.replace('number:', '').replace('string:', '').replace(/,/g, '') + ",";
                                }
                            }
                            if (multiSelectPicklistSelectedValues == '') {
                                entityData[id] = null;
                            }
                            else {
                                entityData[id] = multiSelectPicklistSelectedValues;
                            }
                        }
                        if (fieldtype === 'Boolean') {
                            var check = false;
                            if ($("#" + tabFields[i] + "").prop("checked") === true) {
                                var check = true;
                            }
                            entityData[id] = check;
                        }
                        if (fieldtype === 'Lookup' || fieldtype === 'Customer') {
                            var idApi = $("#" + tabFields[i] + "").attr('data-webapi');
                            var lookupValue = $("#" + tabFields[i] + "_id").val();
                            if (lookupValue != '') {
                                if (fieldtype === 'Customer') {
                                    idApi = idApi + '_account';
                                }
                                entityData["" + idApi + "@odata.bind"] = "/opportunities(" + lookupValue + ")";
                            }
                        }
                    }
                }
                if (isrequired.length > 0) {
                    var validationMsg = '';
                    if (isrequired.length > 1) {
                        for (var i = 0; i < isrequired.length; i++) {
                            validationMsg = validationMsg + isrequired[i]['ValidationMsg'] + ",";
                        }
                        $("#dvNotificationText").text(validationMsg + " are required.");
                    }
                    else {
                        $("#dvNotificationText").text(isrequired[0]['ValidationMsg'] + " is required.");
                    }
                    
                    //$("#dvNotification").toggleClass('warning').removeClass('error').removeClass('info');
                    setTimeout(function () { $("#dvNotificationText").text(""); $("#dvNotification").removeClass('warning'); }, 5000);
                    return;
                }
                if (!Validation()) {
                    return;
                }
                if (OID === '') {
                    Xrm.WebApi.createRecord(entityName, entityData).then(function (result) {
                        var recordId = result.id;
                        $("#dvNotificationText").text("Opportunity is created successfully.");
                        $("#dvNotification").addClass('info').removeClass('warning').removeClass('error');

                        setTimeout(function () { $("#dvNotificationText").text(""); $("#dvNotification").removeClass('info'); }, 3000);
                        var windowOptions = {
                            openInNewWindow: false
                        };
                        if (Xrm.Utility != null) {
                            Xrm.Utility.openEntityForm("opportunity", recordId, null, windowOptions);
                        }
                    })
                          .fail(function (error) {
                              $("#dvNotificationText").text("Error while Opportunity is creating.");
                              $("#dvNotification").addClass('error').removeClass('warning').removeClass('info');
                              setTimeout(function () { $("#dvNotificationText").text(""); $("#dvNotification").removeClass('error'); }, 3000);
                          });
                }
                else {
                    Xrm.WebApi.updateRecord(entityName, OID, entityData).then(
                        function success(result) {
                            //GetTabData();
                            for (var tab = 0; tab < $scope.tabs.length; tab++) {
                                if ($scope.tabs[tab]["fedcap_customizetabid"] === cntrlId) {
                                    for (var section = 0; section < $scope.tabs[tab].sections.length; section++) {
                                        GetFieldOpportunityData($scope.tabs[tab].sections[section]["fedcap_customizesectionid"], tab, section);
                                        if (tabName === 'Intelligence') {
                                            CallGovWin();
                                            var iFrameObj = $('#GovWinFBO').attr('id');
                                            var id = $('#GovWinFBO').attr('src');
                                            $('#GovWinFBO').attr('src', $('#GovWinFBO').attr('src'));
                                        }
                                    }
                                }
                            }
                            $("#dvNotificationText").text("" + tabName + " is updated successfully.");
                            $("#dvNotification").toggleClass('info').removeClass('warning').removeClass('error');
                            setTimeout(function () { $("#dvNotificationText").text(""); $("#dvNotification").removeClass('info'); }, 3000);
                        },
                        function (error) {
                            $("#dvNotificationText").text("Error while " + tabName + " is updating.");
                            $("#dvNotification").toggleClass('error').removeClass('warning').removeClass('info');
                            setTimeout(function () { $("#dvNotificationText").text(""); $("#dvNotification").removeClass('error'); }, 3000);
                        }
                    );
                }
            } catch (e) {
                $("#dvNotificationText").text("Error while " +tabName + " is updating.");
                $("#dvNotification").toggleClass('error').removeClass('warning').removeClass('info');
                setTimeout(function () { $("#dvNotificationText").text(""); $("#dvNotification").removeClass('error'); }, 3000);
            }
        }
        $scope.cancelDetails = function (cntrlId, selectControls, tabName) {
            for (var tab = 0; tab < $scope.tabs.length; tab++) {
                if ($scope.tabs[tab]["fedcap_customizetabid"] === cntrlId) {
                    for (var section = 0; section < $scope.tabs[tab].sections.length; section++) {
                        GetFieldOpportunityData($scope.tabs[tab].sections[section]["fedcap_customizesectionid"], tab, section);
                    }
                    return;
                }
            }
        }
        $scope.LoadTabData = function (cntrlId, selectControls, tabName) {
            for (var tab = 0; tab < $scope.tabs.length; tab++) {
                if ($scope.tabs[tab]["fedcap_customizetabid"] === cntrlId) {
                    //$scope.tabs[tab].fedcap_backgroundcoloractive = $scope.tabs[tab]["fedcap_backgroundcolor"];
                    $scope.tabs[tab].fedcap_backgroundcoloractive = "tabLink ms-crm-PanelHeader-Color active";
                    $scope.tabs[tab].tabcolor = { "background-color": $scope.tabs[tab]["fedcap_backgroundcolor"] };
                }
                else {
                    $scope.tabs[tab].fedcap_backgroundcoloractive = 'tabLink ms-crm-PanelHeader-Color';
                    $scope.tabs[tab].tabcolor = { "background-color": '' };
                }
            }
            for (var tab = 0; tab < $scope.tabs.length; tab++) {
                if ($scope.tabs[tab].LoadTabData !== true) {
                    if ($scope.tabs[tab]["fedcap_customizetabid"] === cntrlId) {
                        $("#dvNotificationText").text("Please wait while " + tabName + " is retriving..!!!");
                        $("#dvNotification").toggleClass('warning').removeClass('error').removeClass('info');
                        setTimeout(function () {
                            GetSectionData($scope.tabs[tab]["fedcap_customizetabid"], tab); $scope.tabs[tab].LoadTabData = true; BindCalenderIE($scope.tabs[tab]["fedcap_customizetabid"]);
                        }, 100);
                        setTimeout(function () {
                            SecurityRole(); $("#dvNotificationText").text(""); $("#dvNotification").removeClass('warning');
                        }, 500);
                        return;
                    }
                }
            }
            //$scope.$apply();
        }
        $scope.LoadSecurityRole = function (cntrlId, selectControls, tabName) {
            SecurityRole();
        }
        
        //***************************PWin*************************************************
        $scope.pwintabs = [];
        function GetOptionSetLabelValue(tab,section,EntityLogicalName, AttributeLogicalName) {
            try {
                
                SDK.Metadata.RetrieveAttribute(EntityLogicalName, AttributeLogicalName, "00000000-0000-0000-0000-000000000000", true,
                    function (result) {
                        $scope.pwintabs = [];
                        for (var i = 0; i < result.OptionSet.Options.length; i++) {
                            var sum = 0.00;
                            var value = result.OptionSet.Options[i].Label.LocalizedLabels[0].Label;
                            $scope.pwintabs.push({ "id": result.OptionSet.Options[i].Value, "value": result.OptionSet.Options[i].Label.LocalizedLabels[0].Label,"score": 0.00 })
                            GetQuestion(tab, section, i, sum);
                        }
                        $scope.$apply();
                    },
                    function (error) {
                    }
                );
            }
            catch (e) {

            }
        }
        function GetPWinOpportunityGivenQuestionAnswer(tab,section) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_opportunitypwinanswers?$filter=_fedcap_opportunityid_value eq " + OID + "", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            var data = JSON.parse(this.response);
                            $scope.tabs[tab].sections[section].pwingivenans = data.value;
                        }
                        else {
                            var error = JSON.parse(this.response).error;
                            //Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
        function GetPGoOpportunityGivenQuestionAnswer(tab, section) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_opportunitypgoanswers?$filter=_fedcap_opportunityid_value eq " + OID + "", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            var data = JSON.parse(this.response);
                            $scope.tabs[tab].sections[section].pgogivenans = data.value;
                        }
                        else {
                            var error = JSON.parse(this.response).error;
                            //Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
        function GetQuestion(tab, section, StageIndex, sum) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_pwinquestions?$filter=fedcap_stage eq " + StageIndex + "&$orderby=fedcap_sequence asc", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            var data = JSON.parse(this.response);
                            $scope.pwintabs[StageIndex].questions = data.value;
                            for (var i = 0; i < data.value.length; i++) {
                                if ($scope.tabs[tab].sections[section].pwingivenans.length > 0) {
                                    var filteredData = $scope.tabs[tab].sections[section].pwingivenans.filter(
                                        function (response)
                                        {
                                            //var sum = 0.00;
                                            var id = response["_fedcap_pwinquestion_value"];
                                            if (id === data.value[i]["fedcap_pwinquestionid"]) {
                                                sum += (parseFloat(response["fedcap_pwinanswerweighting"]) / 5) * parseFloat($scope.pwintabs[StageIndex].questions[i]["fedcap_weighting"]);
                                                $scope.pwintabs[StageIndex]["score"] = sum;
                                                $scope.pwintabs[StageIndex].questions[i]["fedcap_answerid"] = response["_fedcap_pwingivenanswer_value"];
                                            }
                                            return id === data.value[i]["fedcap_pwinquestionid"];
                                        })
                                }
                                GetAnswer(StageIndex, i, data.value[i]["fedcap_pwinquestionid"]);
                            }
                        }
                        else {
                            //var error = JSON.parse(this.response).error;
                            //Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
        function GetAnswer(StageIndex, QueIndex, QuestionId) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_pwinanswers?$filter=_fedcap_question_value eq " + QuestionId + "", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            var data = JSON.parse(this.response);
                            $scope.pwintabs[StageIndex].questions[QueIndex].answers = data.value;
                        }
                        else {
                            //var error = JSON.parse(this.response).error;
                            //Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
        //**********************PWin End**************************************************
        //****************************PGo*************************************************
        //$scope.pgoquestions = [];
        //$scope.onSelectChange = function () {
        //    $timeout(function () {
        //        var filteredData = $scope.productsList.filter(function (response) {return response.Id === $scope.data.ProductId; })
        //        console.log(filteredData[0].ProductColor);
        //    }, 100);
        //};
        $scope.pgo_questions = [];

        function GetPGoQuestion(tab, section) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_pgoquestions?$orderby=fedcap_sequence asc", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            debugger;
                            var data = JSON.parse(this.response);
                            $scope.pgo_questions = data.value;
                            $scope.pgo_questions.score = 12;
                            //$scope.tabs[tab].pgoquestions = data.value;
                            //$scope.tabs[tab].sections[section].pgoquestions = data.value;
                            //var sum = 0.00;
                            //for (var i = 0; i < data.value.length; i++) {
                            //    if ($scope.tabs[tab].sections[section].pgogivenans.length > 0) {
                            //        var filteredData = $scope.tabs[tab].sections[section].pgogivenans.filter(
                            //            function (response) {
                            //                var id = response["_fedcap_pgoquestion_value"];
                            //                if (id === data.value[i]["fedcap_pgoquestionid"]) {
                            //                    sum += (parseFloat(response["fedcap_pgoanswerweighting"]) / 5) * parseFloat($scope.tabs[tab].sections[section].pgoquestions[i]["fedcap_weighting"]);
                            //                    $scope.tabs[tab].sections[section].pgoquestions.score = sum;
                            //                    $scope.tabs[tab].sections[section].pgoquestions[i]["fedcap_answerid"] = response["_fedcap_pgogivenanswer_value"];
                            //                }
                            //                return id === data.value[i]["fedcap_pgoquestionid"];
                            //            })
                            //    }
                            //    GetPGoAnswer(tab, section,i, data.value[i]["fedcap_pgoquestionid"]);
                            //}
                        }
                        else {
                            //var error = JSON.parse(this.response).error;
                            //Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
        function GetPGoAnswer(tab, section,QueIndex, QuestionId) {
            try {
                var serverUrl = location.protocol + "//" + location.host;
                var req = new XMLHttpRequest(); // the name of custom entity is = new_trialentity
                req.open("GET", serverUrl + "/api/data/v9.0/fedcap_pgoanswers?$filter=_fedcap_pgo_question_value eq " + QuestionId + "", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("Prefer", "odata.include-annotations=*");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (this.status === 200) {
                            var data = JSON.parse(this.response);
                            //$scope.tabs[tab].pgoquestions[QueIndex].answers = data.value;
                            $scope.tabs[tab].sections[section].pgoquestions[QueIndex].pgoanswers = data.value;
                        }
                        else {
                            //var error = JSON.parse(this.response).error;
                            //Xrm.Utility.alertDialog(error.message);
                        }
                    }
                };
                req.send();
            } catch (e) {

            }
        }
        //********************************************************************************
       
        function GetCurrency() {
            debugger;
            var currencysymbol = '';
            var currencyid = '';
            try {
                Xrm.WebApi.retrieveRecord("opportunity", OID, "$select=_transactioncurrencyid_value")
                                .then(function (data) {
                                    debugger;
                                    currencyid = data["_transactioncurrencyid_value"];
                                    Xrm.WebApi.retrieveRecord("transactioncurrency", currencyid, "$select=currencysymbol")
                                                       .then(function (dataCurr) {
                                                           debugger;
                                                           currencysymbol = dataCurr["currencysymbol"];
                                                           $scope.currencysymbol = currencysymbol;
                                                       })
                                                       .fail(function (error) {
                                                           //Xrm.Utility.alertDialog(error.message);
                                                       });
                                })
                                .fail(function (error) {
                                });
               
            } catch (e) {
                //Xrm.Utility.alertDialog(e.message);
            }
            return currencysymbol;
        }
    }
})();
function Validation(tabName, currencySymbol) {
    try {
        //alert("Validation file called!!!");
        if (tabName === "Details") {
            //*****************Out Of Range Validation Start*************************************************
            if ($('#fedcap_ourworkshare').length > 0) {
                if ($("#fedcap_ourworkshare").val() !== "") {
                    var fedcap_ourworkshareValue = $("#fedcap_ourworkshare").val();
                    var fedcap_ourworkshare = parseInt(fedcap_ourworkshareValue.replace('number:', '').replace('string:', '').replace(/,/g, ''));
                    if (fedcap_ourworkshare > 100) {
                        Xrm.Page.ui.setFormNotification("Our Workshare % must be between 0 to 100.", "WARNING", "1");
                        setTimeout(function () { Xrm.Page.ui.clearFormNotification("1"); }, 6000);
                        return false;
                    }
                }
            }
            //*****************Out Of Range Validation End*************************************************

        }
        if (tabName === "Budget") {
            debugger;
            //*****************Out Of Range Validation Start*************************************************
            if ($('#fedcap_estimated_bid_fee').length > 0) {
                if ($("#fedcap_estimated_bid_fee").val() !== "") {
                    var fedcap_estimatedbidfeeValue = $("#fedcap_estimated_bid_fee").val();
                    var fedcap_estimatedbidfee = parseInt(fedcap_estimatedbidfeeValue.replace('$', '').replace(/,/g, '').replace(currencySymbol, ''));
                    if (fedcap_estimatedbidfee > 100) {
                        Xrm.Page.ui.setFormNotification("Estimated Bid Fee % should be between 0 to 100.", "WARNING", "1");
                        setTimeout(function () { Xrm.Page.ui.clearFormNotification("1"); }, 6000);
                        return false;
                    }
                }
            }
            //*****************Out Of Range Validation End*************************************************

        }
    } catch (e) {
    }
    return true;

}
function UpdateOpptortunityStage(stageId,traversedPath,StageIndex) {
    debugger;
    //traversedPath = Previous stage ids + current stage id, current stage id must be append on last
    var objArry = {};
    objArry["activestageid@odata.bind"] = "/processstages(" + stageId + ")";
    objArry["bpf_opportunityid@odata.bind"] = "/opportunities(" + OID + ")";
    objArry["traversedpath"] = traversedPath;
    
    Xrm.WebApi.updateRecord("new_bpf_0dc682429ede4e3292295e38ebec935f", "0DC68242-9EDE-4E32-9229-5E38EBEC935F", objArry).then(
                    function success(result) {
                        //stage_0
                        //processStageContainer layer0 hasNext activeStage selectedStage - Active
                        //processStageContainer layer1 hasPrevious hasNext unselectedStage - Not Active
                        //processStageContainer layer0 hasNext unselectedStage completedStage - completedStage
                        //processStageContainer layer5 hasPrevious unselectedStage - finish
                        debugger;
                        for (var index = 0; index <= 5; index++) {
                            if (StageIndex === index) {
                                //$("#stage_0").attr('class', 'processStageContainer layer0 hasNext activeStage selectedStage');
                            }
                            else {
                                var classActived = 'processStageContainer layer' + StageIndex.toString() + ' hasPrevious hasNext unselectedStage';
                                $("#stage_" + StageIndex.toString()).removeClass('processStageContainer').removeClass('layer' + StageIndex.toString()).removeClass('hasNext').removeClass('activeStage').removeClass('selectedStage').removeClass('completedStage').removeClass('unselectedStage');
                                $("#stage_" + StageIndex.toString()).attr('class', classActived);
                            }
                        }
                        if (StageIndex === 0) {
                            $("#stage_0").removeClass('processStageContainer').removeClass('layer0').removeClass('hasNext').removeClass('activeStage').removeClass('selectedStage');
                            $("#stage_0").toggleClass('processStageContainer layer0 hasNext activeStage selectedStage');
                        }
                        return true;
                    },
                    function (error) {
                        return false;
                    }
                );
}
function ActiveStages(stageid)
{
    debugger;
    //var activeStage = Xrm.Page.data.process.getActiveStage();
    //if (activeStage != null && activeStage != undefined)
    //{
    //}
    //Xrm.Page.data.process.setActiveStage(stageid, function (reason)
    //{
    //    debugger;
    //    if (reason == "success")
    //    {
    //        debugger;
    //    }
    //    else {
    //    }
    //});
}
function StageChange(opportunitystatus) {
    debugger;
    //var StageValue = Xrm.Page.getAttribute('stageid').getValue();
    var returnValue = false;
    try {
        //var opportunitystatus = Xrm.Page.getAttribute("fedcap_opportunitystatus").getValue();
        switch (opportunitystatus) {
            case "0"://ID/Track                             
                returnValue = UpdateOpptortunityStage('460a0795-743f-4ebe-92cd-b838fac9e8a8', '460a0795-743f-4ebe-92cd-b838fac9e8a8',0);
                //Xrm.Page.getAttribute('stageid').setValue('460a0795-743f-4ebe-92cd-b838fac9e8a8');
                break;
            case "1": //ID/Track
                returnValue = UpdateOpptortunityStage('460a0795-743f-4ebe-92cd-b838fac9e8a8', '460a0795-743f-4ebe-92cd-b838fac9e8a8',0);
                //Xrm.Page.getAttribute('stageid').setValue('460a0795-743f-4ebe-92cd-b838fac9e8a8');
                break;
            case "15": //Cancel
                returnValue = UpdateOpptortunityStage('460a0795-743f-4ebe-92cd-b838fac9e8a8', '460a0795-743f-4ebe-92cd-b838fac9e8a8',0);
                //Xrm.Page.getAttribute('stageid').setValue('460a0795-743f-4ebe-92cd-b838fac9e8a8');
                break;
            case "13"://Dead
                returnValue = UpdateOpptortunityStage('460a0795-743f-4ebe-92cd-b838fac9e8a8', '460a0795-743f-4ebe-92cd-b838fac9e8a8',0);
                //Xrm.Page.getAttribute('stageid').setValue('460a0795-743f-4ebe-92cd-b838fac9e8a8');
                break;
            case "16"://No Pursue
                Xrm.Page.getAttribute('stageid').setValue('460a0795-743f-4ebe-92cd-b838fac9e8a8', '460a0795-743f-4ebe-92cd-b838fac9e8a8', 0);
                break;
            case "2"://Qualification
                returnValue = UpdateOpptortunityStage('c9b3149d-e9ca-4885-9aba-a2b72e690019',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019',1);
                //Xrm.Page.getAttribute('stageid').setValue('c9b3149d-e9ca-4885-9aba-a2b72e690019');
                break;
            case "3"://Pre-Proposal
                returnValue = UpdateOpptortunityStage('552eabfe-c03d-402b-91c4-ff1d3f2924fc',
                                                       '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc',2);
                //Xrm.Page.getAttribute('stageid').setValue('552eabfe-c03d-402b-91c4-ff1d3f2924fc');
                break;
            case "14"://Pre-Proposal-NoBid
                returnValue = UpdateOpptortunityStage('552eabfe-c03d-402b-91c4-ff1d3f2924fc',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc',2);
                //Xrm.Page.getAttribute('stageid').setValue('552eabfe-c03d-402b-91c4-ff1d3f2924fc');
                break;
            case "4"://Proposal
                returnValue = UpdateOpptortunityStage('2918512f-8a41-490a-9702-ba6e291acac3',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc,2918512f-8a41-490a-9702-ba6e291acac3',3);
                //Xrm.Page.getAttribute('stageid').setValue('2918512f-8a41-490a-9702-ba6e291acac3');
                break;
            case "5"://Post Submition
                returnValue = UpdateOpptortunityStage('49734e9b-de26-4c63-a822-33150394f113',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc,2918512f-8a41-490a-9702-ba6e291acac3,49734e9b-de26-4c63-a822-33150394f113', 4);
                //Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "6"://Post Submition-EN
                returnValue = UpdateOpptortunityStage('49734e9b-de26-4c63-a822-33150394f113',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc,2918512f-8a41-490a-9702-ba6e291acac3,49734e9b-de26-4c63-a822-33150394f113', 4);
                //Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "7"://Post Submition-FPR
                returnValue = UpdateOpptortunityStage('49734e9b-de26-4c63-a822-33150394f113',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc,2918512f-8a41-490a-9702-ba6e291acac3,49734e9b-de26-4c63-a822-33150394f113', 4);
                //Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "8"://Post Submition-In Protest
                returnValue = UpdateOpptortunityStage('49734e9b-de26-4c63-a822-33150394f113',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc,2918512f-8a41-490a-9702-ba6e291acac3,49734e9b-de26-4c63-a822-33150394f113', 4);
                //Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "11"://Post Submition-Dead
                returnValue = UpdateOpptortunityStage('49734e9b-de26-4c63-a822-33150394f113',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc,2918512f-8a41-490a-9702-ba6e291acac3,49734e9b-de26-4c63-a822-33150394f113', 4);
                //Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "9"://Post Award-Won
                returnValue = UpdateOpptortunityStage('1b3982ea-2d1e-4a59-b6e9-d39b1717b646',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc,2918512f-8a41-490a-9702-ba6e291acac3,49734e9b-de26-4c63-a822-33150394f113,1b3982ea-2d1e-4a59-b6e9-d39b1717b646', 5);
                //Xrm.Page.getAttribute('stageid').setValue('1b3982ea-2d1e-4a59-b6e9-d39b1717b646');
                break;
            case "10"://Post Award-Lost
                returnValue = UpdateOpptortunityStage('1b3982ea-2d1e-4a59-b6e9-d39b1717b646',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc,2918512f-8a41-490a-9702-ba6e291acac3,49734e9b-de26-4c63-a822-33150394f113,1b3982ea-2d1e-4a59-b6e9-d39b1717b646', 5);
                //Xrm.Page.getAttribute('stageid').setValue('1b3982ea-2d1e-4a59-b6e9-d39b1717b646');
                break;
            case "17"://Award-Lost
                returnValue = UpdateOpptortunityStage('1b3982ea-2d1e-4a59-b6e9-d39b1717b646',
                                                      '460a0795-743f-4ebe-92cd-b838fac9e8a8,c9b3149d-e9ca-4885-9aba-a2b72e690019,552eabfe-c03d-402b-91c4-ff1d3f2924fc,2918512f-8a41-490a-9702-ba6e291acac3,49734e9b-de26-4c63-a822-33150394f113,1b3982ea-2d1e-4a59-b6e9-d39b1717b646', 5);
                //Xrm.Page.getAttribute('stageid').setValue('1b3982ea-2d1e-4a59-b6e9-d39b1717b646');
                break;
            default:
                returnValue = true;
                break;
        }
        return returnValue;
        //var save = true;
        //Xrm.Page.data.refresh(save).then(successCallback, errorCallback);

    } catch (e) {
        //alert("Please select stage step by step.\nOR \nPlease fill required field(s) from the page(Header Or Section).");
        return false;
    }
}
function StageChange_1(opportunitystatus) {
    debugger;
    //var StageValue = Xrm.Page.getAttribute('stageid').getValue();
    try {
        //var opportunitystatus = Xrm.Page.getAttribute("fedcap_opportunitystatus").getValue();
        switch (opportunitystatus) {
            case "0"://ID/Track                             
                Xrm.Page.getAttribute('stageid').setValue('460a0795-743f-4ebe-92cd-b838fac9e8a8');
                break;
            case "1": //ID/Track
                Xrm.Page.getAttribute('stageid').setValue('460a0795-743f-4ebe-92cd-b838fac9e8a8');
                break;
            case "2"://Qualification
                Xrm.Page.getAttribute('stageid').setValue('c9b3149d-e9ca-4885-9aba-a2b72e690019');
                break;
            case "15": //Cancel
                Xrm.Page.getAttribute('stageid').setValue('460a0795-743f-4ebe-92cd-b838fac9e8a8');
                break;
            case "13"://No Pursue
                Xrm.Page.getAttribute('stageid').setValue('460a0795-743f-4ebe-92cd-b838fac9e8a8');
                break;
            case "3"://Pre-Proposal
                Xrm.Page.getAttribute('stageid').setValue('552eabfe-c03d-402b-91c4-ff1d3f2924fc');
                break;
            case "14"://Pre-Proposal-NoBid
                Xrm.Page.getAttribute('stageid').setValue('552eabfe-c03d-402b-91c4-ff1d3f2924fc');
                break;
            case "4"://Proposal
                Xrm.Page.getAttribute('stageid').setValue('2918512f-8a41-490a-9702-ba6e291acac3');
                break;
            case "5"://Post Submition
                Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "6"://Post Submition-EN
                Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "7"://Post Submition-FPR
                Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "8"://Post Submition-In Protest
                Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "11"://Post Submition-Dead
                Xrm.Page.getAttribute('stageid').setValue('49734e9b-de26-4c63-a822-33150394f113');
                break;
            case "9"://Post Award-Won
                Xrm.Page.getAttribute('stageid').setValue('1b3982ea-2d1e-4a59-b6e9-d39b1717b646');
                break;
            case "10"://Post Award-Lost
                Xrm.Page.getAttribute('stageid').setValue('1b3982ea-2d1e-4a59-b6e9-d39b1717b646');
                break;
            case "17"://Award-Lost
                Xrm.Page.getAttribute('stageid').setValue('1b3982ea-2d1e-4a59-b6e9-d39b1717b646');
                break;
            default:
                break;
        }
        var save = true;
        Xrm.Page.data.refresh(save).then(successCallback, errorCallback);

    } catch (e) {
        //alert("Please select stage step by step.\nOR \nPlease fill required field(s) from the page(Header Or Section).");
        return false;
    }
}
function successCallback() {

    //var Xrm = parent.Xrm;
    //var windowOptions = { openInNewWindow: false };
    //if (Xrm.Utility != null) {
    //    Xrm.Utility.openEntityForm("opportunity", OID, null, windowOptions);
    //}
}
function errorCallback() {
}
